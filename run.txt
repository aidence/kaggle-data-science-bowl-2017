# Copyright (C) 2017  Aidence B.V.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA



# !!
# !! Put Kaggle dicoms (train & test) in the original-datasets/kaggle/dicom/ folder.
# !! Put all available labels (so stage 1 and stage 2) in original-datasets/kaggle/labels.csv
# !!
# !! Annotation pickle files are from annotations done by us.
# !!

# !!
# !! Put LIDC/IDIR dicoms (including annotation XML files in the series dir) in the original-datasets/lidc/dicom folder.
# !!
# !! Annotations and series uid files are from the LUNA challenge.
# !!



# Read and scale both datasets:

python3 src/writelidcdataset.py original-datasets/lidc/annotations.csv original-datasets/lidc/annotations_excluded.csv original-datasets/lidc/seriesuids.csv original-datasets/lidc/dicom/ scaled-datasets/lidc/ --scale "2.5x0.512x0.512"
python3 src/writekaggledataset.py original-datasets/kaggle/annotations.pkl original-datasets/kaggle/dicom/ original-datasets/kaggle/labels.csv scaled-datasets/kaggle/ --scale "2.5x0.512x0.512"



# Write random LIDC patches for normal model and large nodule model:

python3 src/writerandompatches.py --output-file-name patches/lidc-random-patches.npz config/random-patches.yml
python3 src/writerandompatches.py --output-file-name patches/lidc-kaggle-random-patches-large.npz config/random-patches-large.yml



# Train localization networks:

python3 src/train.py config/localization.yml models/localization
python3 src/train.py config/localization-large.yml models/localization-large



# Write network logits (commands can be done in parallel on different machines):

python3 src/writelogits.py config/lidc-logits.yml logits/lidc-logits
python3 src/writelogits.py config/kaggle-logits.yml logits/kaggle-logits

python3 src/writelogits.py config/lidc-logits-large.yml logits/lidc-logits-large
python3 src/writelogits.py config/kaggle-logits-large.yml logits/kaggle-logits-large



# Write candidates (commands can be done in parallel on different machines):

# !!
# !! Fill the arguments of the writecandidates script with the values provided by the last determinethresholds script call.
# !!
# !!
# !! This is needed because Tensorflow is not deterministic :-(, and logit distributions vary quite a bit.
# !!


# Normal model:
# 2.0: 0.9991424509 (99.91424509%), 231740015/231938914.
# 2.5: 0.9993550026 (99.93550026%), 231789314/231938914.
# 3.0: 0.9995098278 (99.95098278%), 231825224/231938914.
# 3.5: 0.9996201672 (99.96201672%), 231850816/231938914.

# Large model:
# 2.0: 0.9994163646 (99.94163646%), 21132711/21145052.
# 2.5: 0.9996525428 (99.96525428%), 21137705/21145052.
# 3.0: 0.9998107359 (99.98107359%), 21141050/21145052.
# 3.5: 0.9998958149 (99.98958149%), 21142849/21145052.


# !! Our model found threshold=2.0, size-threshold=2.0

python3 src/determinethresholds.py --threshold-percentile 0.9991424509 --size-percentile 0.9991424509 logits/lidc-logits/
python3 src/writecandidates.py --threshold [fill in threshold] --size-threshold [fill in size threshold] candidates/kaggle-candidates/ logits/kaggle-logits/

# !! Our model found threshold=2.5, size threshold=2.0

python3 src/determinethresholds.py --threshold-percentile 0.9993550026 --size-percentile 0.9991424509 logits/lidc-logits/
python3 src/writecandidates.py --threshold [fill in threshold] --size-threshold [fill in size threshold] candidates/lidc-candidates/ logits/lidc-logits/
python3 src/writecandidates.py --threshold [fill in threshold] --size-threshold [fill in size threshold] candidates/kaggle-candidates-tim/ logits/kaggle-logits/

# !! Our model found threshold=3.5, size-threshold=3.0

python3 src/determinethresholds.py --threshold-percentile 0.9998958149 --size-percentile 0.9998107359 logits/lidc-logits-large/
python3 src/writecandidates.py --threshold [fill in threshold] --size-threshold [fill in size threshold] candidates/kaggle-candidates-large/ logits/kaggle-logits-large/



# Merge Kaggle candidates.

python3 src/mergecandidates.py candidates/kaggle-candidates-merged/ candidates/kaggle-candidates/ candidates/kaggle-candidates-large/ --max-distance -1000



# Read and scale both datasets in lower resolution:

rm -r scaled-datasets/kaggle/
rm -r scaled-datasets/lidc/

python3 src/writelidcdataset.py original-datasets/lidc/annotations.csv original-datasets/lidc/annotations_excluded.csv original-datasets/lidc/seriesuids.csv original-datasets/lidc/dicom/ scaled-datasets/lidc/ --scale "1.25x0.5x0.5"
python3 src/writekaggledataset.py original-datasets/kaggle/annotations.pkl original-datasets/kaggle/dicom/ original-datasets/kaggle/labels.csv scaled-datasets/kaggle/ --scale "1.25x0.5x0.5"



# Generate patches.

python3 src/writepatchesfinal.py "14x36x36" candidates/lidc-candidates/ --output-file-name patches/lidc-candidate-patches.pkl.gz
python3 src/writepatchesfinal.py "14x36x36" candidates/kaggle-candidates-merged/ --output-file-name patches/kaggle-candidate-patches-inc-masses.pkl.gz



# Train final model using different random seeds. This requires Tensorflow 1.1, an up-to-date Anaconda3 install, and 8 GPUs. Run until we have saved at least 20 models, preferably a lot more.
# Check models-final dir to see how many models were trained.

python3 finalsrc/trainfinal.py --seed=1
[ python3 finalsrc/trainfinal.py --seed=2 ]
[ python3 finalsrc/trainfinal.py --seed=n ]



# Run ensemble script which reads in the model predictions generated in previous step and outputs the averaged prediction for submission.

python3 finalsrc/ensemblefinal.py



# Model checkpoints and predictions included in the 'models-final' folder.



