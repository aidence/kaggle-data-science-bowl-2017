# Copyright (C) 2017  Aidence B.V.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA

import numpy as np
import tensorflow as tf
from tensorflow.python.client import device_lib

class GPUAssigner:
    def __init__(self):
        local_device_protos = device_lib.list_local_devices()
        self.available_gpus = [x.name for x in local_device_protos if x.device_type == 'GPU']
        self.gpu_last_assigned_to = -1
    def get_device(self):
        gpu_nr = (self.gpu_last_assigned_to + 1) % len(self.available_gpus)
        self.gpu_last_assigned_to = gpu_nr
        return self.available_gpus[gpu_nr]
gpu_assigner = GPUAssigner()

def get_init(num_in):
    s = np.sqrt(6./num_in)
    return tf.random_uniform_initializer(minval=-s, maxval=s, seed=np.random.randint(99999999999), dtype=tf.float32)

counters = {}
def get_name(layer_name):
    ''' utlity for keeping track of layer names '''
    if not layer_name in counters:
        counters[layer_name] = 0
    name = layer_name + '_' + str(counters[layer_name])
    counters[layer_name] += 1
    return name

def add_checkpoint(x):
    ''' hint that this input should be remembered by our memory efficient gradient implementation '''
    if isinstance(x, tuple) or isinstance(x, list):
        for xi in x:
            add_checkpoint(xi)
    else:
        tf.add_to_collection('checkpoints', x)

def maybe_concat(x):
    if isinstance(x, tuple):
        return tf.concat(x, len(x[0].get_shape()) - 1)
    else:
        return x

class Layer:
    def __init__(self):
        pass

    def __call__(self, x, **kwargs):
        if isinstance(x,list):
            if len(x) % len(gpu_assigner.available_gpus) == 0:
                out = []
                for i in range(len(x)):
                    with tf.device(gpu_assigner.get_device()):
                        out.append(self.apply(maybe_concat(x[i]), **kwargs))
                return out
            else:
                return [self.apply(maybe_concat(xi), **kwargs) for xi in x]
        else:
            return self.apply(maybe_concat(x), **kwargs)

    def __add__(self, other): # not commutative!
        return AddLayer(self, other)

    def __iadd__(self, other): # just for notational convenience, we don't do anything in place!
        return self.__add__(other)

    def apply(self, x, **kwargs):
        return x

class AddLayer(Layer):
    def __init__(self, layer1, layer2):
        self.layer1 = layer1
        self.layer2 = layer2

    def __call__(self, x, **kwargs):
        l1 = self.layer1(x, **kwargs)
        if isinstance(self.layer2, Layer):
            out = self.layer2(l1, **kwargs)
        elif isinstance(self.layer2, list):
            assert(isinstance(l1,list))
            out = [la + lb for la,lb in zip(l1,self.layer2)]
        elif isinstance(self.layer2, float) or isinstance(self.layer2, int) or isinstance(self.layer2, tf.Tensor) or isinstance(self.layer2, tf.Variable):
            if isinstance(l1, list):
                out = [l+self.layer2 for l in l1]
            else:
                out = l1 + self.layer2
        else:
            raise ('unsupported Layer addition type ' + str(type(self.layer2)))
        return out

class Dense(Layer):
    def __init__(self, num_in, num_out, use_bias=True, name=None):
        self.use_bias = use_bias
        if name is None:
            name = get_name('dense')
        self.name = name
        with tf.variable_scope(name):
            self.W = tf.get_variable('W', [num_in,num_out], tf.float32, get_init(num_in), trainable=True)
            if use_bias:
                self.b = tf.get_variable('b', initializer=tf.zeros(num_out,dtype=tf.float32), trainable=True)

    def apply(self, x, **kwargs):
        y = tf.matmul(x, self.W)
        if self.use_bias:
            y = tf.nn.bias_add(y,self.b)
        return y

class Conv2d(Layer):
    def __init__(self, num_filters_in, num_filters_out, filter_size=[3,3], stride=[1,1], pad='SAME', use_bias=True, name=None):
        self.filter_size = filter_size
        self.stride = stride
        self.pad = pad
        self.use_bias = use_bias
        if name is None:
            name = get_name('conv2d')
        self.name = name
        with tf.variable_scope(name):
            self.W = tf.get_variable('W', filter_size+[num_filters_in,num_filters_out], tf.float32, get_init(num_filters_in*np.prod(filter_size)), trainable=True)
            if use_bias:
                self.b = tf.get_variable('b', initializer=tf.zeros(num_filters_out,dtype=tf.float32), trainable=True)

    def apply(self, x, **kwargs):
        y = tf.nn.conv2d(x, self.W, [1]+self.stride+[1], self.pad)
        if self.use_bias:
            y = tf.nn.bias_add(y,self.b)
        return y

class Conv3d(Layer):
    def __init__(self, num_filters_in, num_filters_out, filter_size=[3,3,3], stride=[1,1,1], pad='SAME', use_bias=True, name=None):
        self.filter_size = filter_size
        self.stride = stride
        self.pad = pad
        self.use_bias = use_bias
        self.num_filters_out = num_filters_out
        if name is None:
            name = get_name('conv3d')
        self.name = name
        with tf.variable_scope(name):
            self.W = tf.get_variable('W', filter_size+[num_filters_in,num_filters_out], tf.float32, get_init(num_filters_in*np.prod(filter_size)), trainable=True)
            if use_bias:
                self.b = tf.get_variable('b', initializer=tf.zeros(num_filters_out,dtype=tf.float32), trainable=True)

    def apply(self, x, **kwargs):
        y = tf.nn.conv3d(x, self.W, [1]+self.stride+[1], self.pad)
        if self.use_bias:
            y += tf.reshape(self.b, [1,1,1,1,self.num_filters_out])
        return y

class Scale(Layer):
    def __init__(self, num_features, name=None):
        self.num_features = num_features
        if name is None:
            name = get_name('scale')
        self.name = name
        with tf.variable_scope(name):
            self.scale = tf.get_variable('scale', initializer=tf.ones(num_features,dtype=tf.float32), trainable=True)

    def apply(self, x, **kwargs):
        return x * tf.reshape(self.scale,[1]*(len(x.get_shape())-1)+[self.num_features])

class ScaleAndShift(Layer):
    def __init__(self, num_features, name=None):
        self.num_features = num_features
        if name is None:
            name = get_name('scale_and_shift')
        self.name = name
        with tf.variable_scope(name):
            self.scale = tf.get_variable('scale', initializer=tf.ones(num_features,dtype=tf.float32), trainable=True)
            self.shift = tf.get_variable('shift', initializer=tf.zeros(num_features, dtype=tf.float32), trainable=True)

    def apply(self, x, **kwargs):
        return x * tf.reshape(self.scale,[1]*(len(x.get_shape())-1)+[self.num_features]) + tf.reshape(self.shift,[1]*(len(x.get_shape())-1)+[self.num_features])

class Reshape(Layer):
    def __init__(self, shape):
        self.shape = tuple(shape)
    def apply(self, x, **kwargs):
        return tf.reshape(x, self.shape)

class Transpose(Layer):
    def __init__(self, perm):
        self.perm = tuple(perm)
    def apply(self, x, **kwargs):
        return tf.transpose(x, perm)

class BatchNorm(Layer):
    def __init__(self, num_features, rho=0.9, use_offset=True, use_scale=True, name=None):
        self.num_features = num_features
        self.rho = rho
        self.use_offset = use_offset
        self.use_scale = use_scale
        if name is None:
            name = get_name('batch_norm')
        self.name = name
        with tf.variable_scope(name):
            self.avg_batch_mean = tf.Variable(tf.zeros(num_features, dtype=tf.float32),trainable=False)
            self.avg_batch_var = tf.Variable(tf.ones(num_features, dtype=tf.float32), trainable=False)
            if self.use_offset:
                self.offset = tf.Variable(tf.zeros(num_features, dtype=tf.float32), trainable=True)
            else:
                self.offset = None
            if self.use_scale:
                self.scale = tf.Variable(tf.ones(num_features, dtype=tf.float32), trainable=True)
            else:
                self.scale = None

    def __call__(self, x, is_training=True, update_bn_stats=True, bn_block_mode='all_blocks', bn_nr_blocks='all', **kwargs):
        # calc statistics
        if is_training:
            if isinstance(x,list):
                if bn_nr_blocks == 'all':
                    bn_nr_blocks = len(x)
                moms = [tf.nn.moments(xi,tuple([i for i in range(len(xi.get_shape())-1)])) for xi in x[:bn_nr_blocks]]
                mt = sum([x[0] for x in moms]) / len(moms)
                vt = (sum([x[1] for x in moms]) + sum([tf.square(x[0] - mt) for x in moms])) / len(moms)

                if update_bn_stats:
                    bn_stats_ops = [self.avg_batch_mean.assign(self.rho * self.avg_batch_mean + (1. - self.rho) * mt),
                                    self.avg_batch_var.assign(self.rho * self.avg_batch_var + (1. - self.rho) * vt)]
                else:
                    bn_stats_ops = None

                with tf.control_dependencies(bn_stats_ops):
                    if bn_block_mode == 'per_block':
                        return [self.apply(x[i], moms[i][0], moms[i][1], **kwargs) for i in range(len(x))]
                    else:
                        return [self.apply(x[i], mt, vt, **kwargs) for i in range(len(x))]

            else:
                mt, vt = tf.nn.moments(x,tuple([i for i in range(len(x.get_shape())-1)]))
                if update_bn_stats:
                    bn_stats_ops = [self.avg_batch_mean.assign(self.rho * self.avg_batch_mean + (1. - self.rho) * mt),
                                    self.avg_batch_var.assign(self.rho * self.avg_batch_var + (1. - self.rho) * vt)]
                else:
                    bn_stats_ops = None

                with tf.control_dependencies(bn_stats_ops):
                    return self.apply(x, mt, vt, **kwargs)

        else:
            if isinstance(x,list):
                return [self.apply(xi, self.avg_batch_mean, self.avg_batch_var, **kwargs) for xi in x]
            else:
                return self.apply(x, self.avg_batch_mean, self.avg_batch_var, **kwargs)

    def apply(self, x, m, v, **kwargs):
        return tf.nn.batch_normalization(x,m,v,self.offset,self.scale,1e-5)

class ReLU(Layer):
    def apply(self, x, **kwargs):
        return tf.nn.relu(x)

class AvgPool(Layer):
    def __init__(self, ksize=[2,2], padding='VALID'):
        self.ksize = ksize
        self.padding = padding
    def apply(self, x, **kwargs):
        return tf.nn.avg_pool(x,[1]+self.ksize+[1],[1]+self.ksize+[1],self.padding)

class AvgPool3d(Layer):
    def __init__(self, ksize=[2,2,2], padding='VALID'):
        self.ksize = ksize
        self.padding = padding
    def apply(self, x, **kwargs):
        return tf.nn.avg_pool3d(x,[1]+self.ksize+[1],[1]+self.ksize+[1],self.padding)

class GlobalAvgPool(Layer):
    def apply(self, x, **kwargs):
        return tf.reduce_mean(x,[i for i in range(1,len(x.get_shape())-1)])

class Dropout(Layer):
    def __init__(self, drop_prob=0.5):
        self.drop_prob = drop_prob
    def apply(self, x, is_training=True, **kwargs):
        if is_training and self.drop_prob>0:
            x = tf.nn.dropout(x, 1. - self.drop_prob, seed=np.random.randint(99999999999))
        return x

class Resnet(Layer):
    def __init__(self, num_features_in, num_features_hidden, nonlinearity=ReLU):
        self.num_features_in = num_features_in
        self.num_features_hidden = num_features_hidden
        self.nonlinearity = nonlinearity
        self.residual_layer = BatchNorm(num_features_in) + nonlinearity() + Conv2d(num_features_in, num_features_hidden, use_bias=False) + \
                              BatchNorm(num_features_hidden) + nonlinearity() + Conv2d(num_features_hidden, num_features_in, use_bias=False)

    def __call__(self, x, **kwargs):
        r = self.residual_layer(x, **kwargs)
        out = x+r
        add_checkpoint(out)
        return out

class Resnet3d(Resnet):
    def __init__(self, num_features_in, num_features_hidden, nonlinearity=ReLU):
        self.num_features_in = num_features_in
        self.num_features_hidden = num_features_hidden
        self.nonlinearity = nonlinearity
        self.residual_layer = BatchNorm(num_features_in) + nonlinearity() + Conv3d(num_features_in, num_features_hidden, use_bias=False) + \
                              BatchNorm(num_features_hidden) + nonlinearity() + Conv3d(num_features_hidden, num_features_in, use_bias=False)

class Densenet(Layer):
    def __init__(self, num_features_in, num_features_out, drop_prob=0.):
        self.num_features_in = num_features_in
        self.num_features_out = num_features_out
        self.drop_prob = drop_prob
        self.sub_layer = ScaleAndShift(num_features_in) + ReLU() + Conv2d(num_features_in, num_features_out, use_bias=(drop_prob>0)) \
                         + Dropout(drop_prob) + BatchNorm(num_features_out,use_offset=False,use_scale=False)

    def __call__(self, x, **kwargs):  # Densenet layers return tuples!
        l = self.sub_layer(x, **kwargs)
        add_checkpoint(l)
        if isinstance(x,list):
            out = []
            for xi,li in zip(x,l):
                if isinstance(xi, tuple):
                    out.append(xi + (li,))
                else:
                    out.append((xi, li))
            return out
        else:
            if isinstance(x, tuple):
                return x + (l,)
            else:
                return (x,l)

class DensenetB(Densenet):
    def __init__(self, num_features_in, num_features_out, num_features_hidden=None, drop_prob=0.):
        self.num_features_in = num_features_in
        self.num_features_out = num_features_out
        if num_features_hidden is None:
            num_features_hidden = num_features_out*4
        self.drop_prob = drop_prob
        self.sub_layer = ScaleAndShift(num_features_in) + ReLU() + Conv2d(num_features_in, num_features_hidden, [1,1], use_bias=False) + BatchNorm(num_features_hidden) + \
                         ReLU() + Conv2d(num_features_hidden, num_features_out, use_bias=(drop_prob>0)) + Dropout(drop_prob) + BatchNorm(num_features_out,use_offset=False,use_scale=False)

class Densenet3d(Densenet):
    def __init__(self, num_features_in, num_features_out, drop_prob=0.):
        self.num_features_in = num_features_in
        self.num_features_out = num_features_out
        self.drop_prob = drop_prob
        self.sub_layer = ScaleAndShift(num_features_in) + ReLU() + Conv3d(num_features_in, num_features_out, use_bias=(drop_prob>0)) \
                         + Dropout(drop_prob) + BatchNorm(num_features_out,use_offset=False,use_scale=False)

class DensenetB3d(Densenet):
    def __init__(self, num_features_in, num_features_out, num_features_hidden=None, drop_prob=0.):
        self.num_features_in = num_features_in
        self.num_features_out = num_features_out
        if num_features_hidden is None:
            num_features_hidden = num_features_out * 4
        self.drop_prob = drop_prob
        self.sub_layer = ScaleAndShift(num_features_in) + ReLU() + Conv3d(num_features_in, num_features_hidden, [1,1,1], use_bias=False) + BatchNorm(num_features_hidden) + \
                         ReLU() + Conv3d(num_features_hidden, num_features_out, use_bias=(drop_prob>0)) + Dropout(drop_prob) + BatchNorm(num_features_out,use_offset=False,use_scale=False)

class Densenet3dFactored(Densenet):
    def __init__(self, num_features_in, num_features_out, zyx_dim, drop_prob=0.):
        self.num_features_in = num_features_in
        self.num_features_out = num_features_out
        self.drop_prob = drop_prob
        self.sub_layer = ScaleAndShift(num_features_in) + ReLU()
        if num_features_in >= 8*num_features_out:
            num_features_hidden = 4*num_features_out
            self.sub_layer += Conv3d(num_features_in, num_features_hidden, [1,1,1], use_bias=False) + BatchNorm(num_features_hidden) + ReLU()
        else:
            num_features_hidden = num_features_in
        self.sub_layer += Reshape([-1,zyx_dim[1],zyx_dim[2],num_features_hidden]) + Conv2d(num_features_hidden, num_features_out, use_bias=False) \
                          + BatchNorm(num_features_out) + ReLU() + Reshape([-1,zyx_dim[0],zyx_dim[1],zyx_dim[2],num_features_out]) \
                          + Conv3d(num_features_out, num_features_out, [3,1,1], use_bias=(drop_prob>0)) + Dropout(drop_prob) + BatchNorm(num_features_out,use_offset=False,use_scale=False)

class Transition(Layer):
    def __init__(self, num_features_in, num_features_out=None, drop_prob=0.):
        if num_features_out is None:
            num_features_out = num_features_in // 2
        self.num_features_in = num_features_in
        self.num_features_out = num_features_out
        self.drop_prob = drop_prob
        self.sub_layer = ScaleAndShift(num_features_in) + ReLU() + Conv2d(num_features_in, num_features_out, [1,1], use_bias=(drop_prob>0)) \
                         + Dropout(drop_prob) + AvgPool([2,2]) + BatchNorm(num_features_out,use_offset=False,use_scale=False)

    def __call__(self, x, **kwargs):
        out = self.sub_layer(x, **kwargs)
        add_checkpoint(out)
        return out

class Transition3d(Transition):
    def __init__(self, num_features_in, num_features_out=None, drop_prob=0.):
        if num_features_out is None:
            num_features_out = num_features_in // 2
        self.num_features_in = num_features_in
        self.num_features_out = num_features_out
        self.drop_prob = drop_prob
        self.sub_layer = ScaleAndShift(num_features_in) + ReLU() + Conv3d(num_features_in, num_features_out, [1,1,1], use_bias=(drop_prob>0)) \
                         + Dropout(drop_prob) + AvgPool3d([2,2,2]) + BatchNorm(num_features_out,use_offset=False,use_scale=False)
