# Copyright (C) 2017  Aidence B.V.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA

import os
import time
import argparse
import pandas as pd
import gzip
import pickle
import numpy as np
import tensorflow as tf
import nn
import data

parser = argparse.ArgumentParser()
parser.add_argument('-s', '--seed', type=int, default=1, help='Random seed to use')
parser.add_argument('-o', '--save_dir', type=str, default='models-final', help='Location for parameter checkpoints and samples')
parser.add_argument('-l', '--learning_rate', type=float, default=0.2, help='Base learning rate')
parser.add_argument('-v', '--holdout_fraction', type=float, default=0.33, help='Which part of the data to use for validation / bagging')
parser.add_argument('-w', '--weight_decay', type=float, default=1e-4, help='Weight decay coefficient')
parser.add_argument('-d', '--drop_prob', type=float, default=0.2, help='Dropout probability')
parser.add_argument('-m', '--meta_drop_prob', type=float, default=0.2, help='Dropout probability on meta features')
parser.add_argument('-t', '--nr_epochs', type=int, default=100, help='Total number of parameter updates')
parser.add_argument('-p', '--frac_low_mom_epochs', type=float, default=0.33, help='fraction of epochs to run with lower momentum value')
parser.add_argument('-f', '--nr_filters', type=int, default=8, help='Nr of filters to use')
parser.add_argument('-b', '--batch_size', type=int, default=32, help='batch size for training and testing')
parser.add_argument('-k', '--nr_kaggle_scans', type=int, default=16, help='nr of kaggle scans per batch')
parser.add_argument('-n', '--nr_densenet_layers', type=int, default=4, help='Nr of densenet layer to use')
args = parser.parse_args()
print(args)

np.random.seed(args.seed)
if not os.path.exists(args.save_dir):
    os.makedirs(args.save_dir)


# /////// define tensorflow model ////////
print('defining the model')

# shared base, for input patches of size N,12,32,32
base_model = nn.Reshape([-1,12,32,32,1]) + nn.Conv3d(1, 2*args.nr_filters, use_bias=False) + nn.BatchNorm(2*args.nr_filters, use_offset=False, use_scale=False)
for i in range(args.nr_densenet_layers):
    base_model += nn.Densenet3d((2+i)*args.nr_filters, args.nr_filters, args.drop_prob)

n_in = (2+args.nr_densenet_layers)*args.nr_filters
n_out = n_in // 2
base_model += nn.Transition3d(n_in, n_out, args.drop_prob)

for i in range(args.nr_densenet_layers):
    base_model += nn.Densenet3d(n_out + i * args.nr_filters, args.nr_filters, args.drop_prob)

n_in = n_out + args.nr_densenet_layers*args.nr_filters
n_out = n_in // 2
base_model += nn.Transition3d(n_in, n_out, args.drop_prob)

for i in range(args.nr_densenet_layers):
    base_model += nn.Densenet3d(n_out + i * args.nr_filters, args.nr_filters, args.drop_prob)

n_in = n_out + args.nr_densenet_layers*args.nr_filters
base_model += nn.ScaleAndShift(n_in) + nn.ReLU() + nn.GlobalAvgPool()

# some dropout on the meta features
meta_prepro = nn.Dropout(drop_prob=args.meta_drop_prob)

# output heads for different applications, take conv features from base model + 5 additional features
lidc_nodule_head = nn.Dense(n_in, 1)
lidc_meta_head = nn.Dense(n_in, 6)
kaggle_prior_logit = tf.placeholder(tf.float32, shape=[])
kaggle_head = nn.Dense(n_in+5, 1) + kaggle_prior_logit

# data placeholders
x_lidc = [tf.placeholder(tf.float32, shape=(args.batch_size,12,32,32), name='x_lidc_' + str(i)) for i in range(2)] # input patches
y_lidc = tf.placeholder(tf.float32, shape=(args.batch_size,6), name='y_lidc') # for holding meta targets
x_kaggle = tf.placeholder(tf.float32, shape=(None,12,32,32), name='x_kaggle')
m_kaggle = tf.placeholder(tf.float32, shape=(None,5), name='m_kaggle') # meta features
masks = tf.placeholder(tf.float32, shape=(None,args.nr_kaggle_scans))

# extract features for training
all_x = x_lidc + tf.split(x_kaggle, 6, 0)
conv_features = base_model(all_x, is_training=True, update_bn_stats=True, bn_nr_blocks=2) # do batch norm based on first 2 (LIDC) inputs
m_features = meta_prepro(tf.split(m_kaggle, 6, 0), is_training=True)
lidc_conv_features = conv_features[:2]
kaggle_conv_features = conv_features[2:]
kaggle_all_features = [tf.concat([c,m],1) for c,m in zip(kaggle_conv_features,m_features)]

# LIDC nodule / no nodule classification
lidc_nodule_logits = lidc_nodule_head(lidc_conv_features)
ce_lidc_nod = (tf.reduce_mean(tf.nn.softplus(lidc_nodule_logits[0])-lidc_nodule_logits[0])
               + tf.reduce_mean(tf.nn.softplus(lidc_nodule_logits[1]))) / 2.

# LIDC meta targets
lidc_meta_pred = lidc_meta_head(lidc_conv_features[0])
ce_lidc_meta = tf.reduce_mean(abs(y_lidc-lidc_meta_pred))

# Kaggle cancer classification, share parameters with other two heads
pred_kaggle_head = kaggle_head(kaggle_all_features)
nod_coeff = tf.Variable(np.cast[np.float32](0.5),trainable=True)
meta_coeff = tf.Variable(np.array([0.5] + [0.]*5, dtype=np.float32), trainable=True)
pred_other_heads = [nod_coeff*lidc_nodule_head(v) + tf.matmul(lidc_meta_head(v),tf.reshape(meta_coeff,[-1,1])) for v in kaggle_conv_features]
kaggle_logits = tf.concat([0.33*a + 0.66*b for a,b in zip(pred_kaggle_head,pred_other_heads)], 0)

masked_logits = tf.reshape(tf.reduce_max(tf.reshape(kaggle_logits, [-1,1]) * masks - (1.-masks)*9999999., 0),[-1])
kaggle_pred = tf.nn.sigmoid(masked_logits)
pos_logits, neg_logits = tf.split(masked_logits, 2, 0) # training assumes 50/50 split of positives/negatives
kaggle_c_prob = tf.nn.sigmoid(kaggle_prior_logit)
ce_kaggle = kaggle_c_prob * tf.reduce_mean(tf.nn.softplus(pos_logits)-pos_logits) + (1.-kaggle_c_prob)*tf.reduce_mean(tf.nn.softplus(neg_logits))

# optimization
params = tf.trainable_variables()
sumloss = (ce_lidc_nod + ce_lidc_meta + ce_kaggle) / 3.
for p in params:
    sumloss += args.weight_decay * tf.reduce_sum(tf.square(p))

tf_lr = tf.placeholder(tf.float32, shape=[])
optim = tf.train.MomentumOptimizer(tf_lr, 0.9, use_nesterov=True)
optim_op = optim.minimize(sumloss, colocate_gradients_with_ops=True)

# count number of params
nparam = 0
for v in tf.trainable_variables():
    nparam += np.prod(v.get_shape().as_list())
print("This model has %d parameters." % nparam)

# init params and finalize graph
init = tf.global_variables_initializer()
saver = tf.train.Saver()
tf.get_default_graph().finalize()

# filling the placeholders
def make_feed_dict(numpy_dict):
    d = {}
    if 'x_lidc_0' in numpy_dict:
        d[x_lidc[0]] = numpy_dict['x_lidc_0']
        d[x_lidc[1]] = numpy_dict['x_lidc_1']
        d[y_lidc] = numpy_dict['y_lidc']
    d[x_kaggle] = numpy_dict['x_kaggle']
    d[m_kaggle] = numpy_dict['m_kaggle']
    if 'masks' in numpy_dict:
        d[masks] = numpy_dict['masks']
    if 'pred_label' in numpy_dict:
        d[pred_label] = numpy_dict['pred_label']
    d[kaggle_prior_logit] = numpy_dict['kaggle_prior_logit']
    return d

# //////////// perform training //////////////
model_nr = 0
with tf.Session() as sess:
    while True: # keep running the model again and again for ensembling
        print('start training model nr %d' % model_nr, flush=True)
        sess.run(init)

        # read data
        dseed=np.random.randint(9999999)
        train_data = data.DataIterator('train', args.nr_kaggle_scans, args.holdout_fraction, args.batch_size, seed=dseed)
        if args.holdout_fraction > 0:
            validation_data = data.DataIterator('validation', args.nr_kaggle_scans, args.holdout_fraction, seed=dseed)

        for epoch in range(args.nr_epochs):
            begin = time.time()

            # lower momentum for last few epochs
            if epoch > (args.nr_epochs * (1. - args.frac_low_mom_epochs)):
                optim._momentum = 0.5

            # one pass over the data
            train_losses = [[], [], []]
            lr = 0.5 * args.learning_rate * (1. + np.cos(np.pi * epoch / args.nr_epochs))
            for npd in train_data:
                feed_dict = make_feed_dict(npd)
                feed_dict.update({ tf_lr: lr })
                loss_step = sess.run([ce_lidc_nod, ce_lidc_meta, ce_kaggle, optim_op], feed_dict)
                for i in range(len(train_losses)):
                    train_losses[i].append(loss_step[i])

            # log progress to console
            mean_train_losses = [np.mean(l) for l in train_losses]
            print("Iteration %d, steps = %d, time = %ds, ce_lidc_nod = %.4f, ce_lidc_meta = %.4f, ce_kaggle = %.4f" %
                  (epoch, len(train_losses[0]), time.time()-begin, mean_train_losses[0], mean_train_losses[1], mean_train_losses[2]), flush=True)
            np_nod_coeff, np_meta_coeff = sess.run([nod_coeff,meta_coeff])
            print('nodule coefficient: %.4f, other coefficients:' % np_nod_coeff)
            print(np_meta_coeff)

            # validate every 10 epochs
            if False: #args.holdout_fraction>0 and (epoch+1)%10 == 0:
                begin = time.time()

                # predict using same noise as during training, average over multiple runs to reduce variance
                sum_prob = np.zeros(validation_data.n)
                sum_count = np.zeros(validation_data.n)

                for repeat in range(50):
                    for npd,inds in validation_data:
                        feed_dict = make_feed_dict(npd)
                        preds = sess.run(kaggle_pred, feed_dict)
                        sum_prob[inds] += preds
                        sum_count[inds] += 1

                preds = sum_prob/sum_count
                val_acc = np.mean(validation_data.labels*np.cast[np.float64](preds>0.5) + (1.-validation_data.labels)*np.cast[np.float64](preds<=0.5))
                val_loss = -np.mean(validation_data.labels*np.log(preds) + (1.-validation_data.labels)*np.log(1.-preds))
                print("//// Test iteration %d, time = %ds, ce = %.4f, acc = %.4f" % (epoch, time.time() - begin, val_loss, val_acc), flush=True)

        # save model output
        saver.save(sess, args.save_dir + '/model_seed_' + str(args.seed), global_step=model_nr)

        # predict using same noise as during training, average over multiple runs to reduce variance
        submit_data = data.DataIterator('submission', args.nr_kaggle_scans, 0.)
        sum_prob = np.zeros(submit_data.n)
        sum_count = np.zeros(submit_data.n)

        # predict submission set
        for repeat in range(100):
            for npd, inds in submit_data:
                feed_dict = make_feed_dict(npd)
                preds = sess.run(kaggle_pred, feed_dict)
                sum_prob[inds] += preds
                sum_count[inds] += 1

        preds = sum_prob / sum_count
        np.savez(args.save_dir + '/prediction_seed_' + str(args.seed) + '_' + str(model_nr) + '.npz', preds=preds)

        model_nr += 1

