# Copyright (C) 2017  Aidence B.V.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA

import pandas as pd
import numpy as np
import gzip
import pickle
import argparse
from os import listdir
from os.path import isfile, join

minimum_pred = 0.01 # where to cap predictions

parser = argparse.ArgumentParser()
parser.add_argument('-o', '--save_dir', type=str, default='models-final', help='Location for parameter checkpoints and samples')
args = parser.parse_args()
print(args)

# Kaggle data
submission = pd.read_csv('original-datasets/kaggle/stage2_sample_submission.csv')
kaggle_labels = pd.read_csv('original-datasets/kaggle/stage1_labels.csv')
kaggle_labels_lb1 = pd.read_csv('original-datasets/kaggle/stage1_solution.csv')
kaggle_labels = pd.concat([kaggle_labels, kaggle_labels_lb1])

kaggle_cancer_ids = []
kaggle_nocancer_ids = []
for i in range(len(kaggle_labels)):
    if kaggle_labels.cancer.iloc[i] == 1:
        kaggle_cancer_ids.append(kaggle_labels.id.iloc[i])
    else:
        kaggle_nocancer_ids.append(kaggle_labels.id.iloc[i])
kaggle_submit_ids = list(submission.id)

with gzip.open('patches/kaggle-candidate-patches-inc-masses.pkl.gz', 'rb') as f:
    kaggle_data = pickle.load(f)

kaggle_meta = kaggle_data['negative-metadata']
kaggle_x = kaggle_data['negative-inputs']

kaggle_cancer_d = {}
kaggle_nocancer_d = {}
kaggle_submit_d = {}

def add_to_dict(d, x, m, kid):
    if kid in d:
        d[kid][0].append(x)
        d[kid][1].append(m)
    else:
        d[kid] = [[x],[m]]

def array_from_meta(meta):
    return np.array([meta['radius'],
                     meta['zoom_mod'],
                     meta['z-fraction'],
                     meta['y-fraction'],
                     abs(meta['x-fraction']-0.5)
                     ])

# get all meta data and normalize
all_m = np.stack([array_from_meta(m) for m in kaggle_meta], 0)
meta_mean = np.mean(all_m,axis=0,keepdims=True)
meta_stdv = np.std(all_m,axis=0,keepdims=True)
all_m = (all_m - meta_mean) / meta_stdv

for i in range(all_m.shape[0]):
    m = all_m[i]
    x = kaggle_x[i]
    kid = kaggle_meta[i]['id']
    if kid in kaggle_cancer_ids:
        add_to_dict(kaggle_cancer_d, x, m, kid)
    elif kid in kaggle_nocancer_ids:
        add_to_dict(kaggle_nocancer_d, x, m, kid)
    elif kid in kaggle_submit_ids:
        add_to_dict(kaggle_submit_d, x, m, kid)
    else:
        raise ('id ' + kid + ' not in data')

kaggle_cancer = []
kaggle_no_cancer = []
for kid in kaggle_cancer_ids:
    if kid in kaggle_cancer_d:
        kaggle_cancer.append((np.stack(kaggle_cancer_d[kid][0], 0), np.stack(kaggle_cancer_d[kid][1], 0)))

for kid in kaggle_nocancer_ids:
    if kid in kaggle_nocancer_d:
        kaggle_no_cancer.append((np.stack(kaggle_nocancer_d[kid][0], 0),np.stack(kaggle_nocancer_d[kid][1], 0)))

kaggle_c_prior = len(kaggle_cancer)/(len(kaggle_cancer) + len(kaggle_no_cancer))
np_kaggle_prior_logit = np.log(kaggle_c_prior/(1.-kaggle_c_prior))

# for submission
kaggle_submit = []
kaggle_candidates_ids = []
kaggle_nocandidates_ids = []
for kid in kaggle_submit_ids:
    if kid in kaggle_submit_d:
        kaggle_submit.append((np.stack(kaggle_submit_d[kid][0], 0), np.stack(kaggle_submit_d[kid][1], 0)))
        kaggle_candidates_ids.append(kid)
    else:
        kaggle_nocandidates_ids.append(kid)


# read in all predictions
files = [f for f in listdir(args.save_dir) if isfile(join(args.save_dir, f))]
preds = 0.
count = 0.
for f in files:
    if '.npz' in f:
        dat = np.load(join(args.save_dir, f))
        print('loaded ' + f)
        preds += dat['preds']
        count += 1.

preds /= count
preds = np.maximum(preds, minimum_pred)

# put in pandas dataframe
preds_final = np.zeros(len(submission))
counter1 = 0
counter2 = 0
for kid in kaggle_submit_ids:
    if kid in kaggle_candidates_ids:
        assert (kaggle_submit_ids[counter2] == kaggle_candidates_ids[counter1])
        preds_final[counter2] = preds[counter1]
        counter1 += 1
    else: # fix probability of this submission
        assert (kaggle_submit_ids[counter2] == kaggle_nocandidates_ids[counter2-counter1])
        preds_final[counter2] = minimum_pred
    counter2 += 1

assert(counter2 == len(preds_final))

print('expected loss %.4f' % (-np.mean(preds_final*np.log(preds_final) + (1.-preds_final)*np.log(1.-preds_final))))

submission.cancer = preds_final
submission.to_csv('sub.csv', index=False)
print('saved submission')

