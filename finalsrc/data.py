# Copyright (C) 2017  Aidence B.V.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA

import pandas as pd
import gzip
import pickle
import numpy as np

# LIDC
with gzip.open('patches/lidc-candidate-patches.pkl.gz', 'rb') as f:
    lidc_data = pickle.load(f)

# positives
lidc_nodule_inc = [[], []]
lidc_nodule_not_inc = []
pos_meta = lidc_data['positive-metadata']
pos_x = lidc_data['positive-inputs']
for i in range(lidc_data['positive-inputs'].shape[0]):
    if pos_meta[i]['nodule-included']:
        lidc_nodule_inc[0].append(pos_x[i])

        targets = []
        for t in ['malignancy', 'spiculation', 'margin', 'texture', 'calcification', 'sphericity']:
            targets.append(np.cast[np.float32](np.median(pos_meta[i]['nodule-metadata'][t])-3.))

        lidc_nodule_inc[1].append(targets)
    else:
        lidc_nodule_not_inc.append(pos_x[i])

lidc_pos = [np.stack(x, 0) for x in lidc_nodule_inc]
lidc_nodule_not_inc = np.stack(lidc_nodule_not_inc, 0)
lidc_neg = np.concatenate([lidc_data['negative-inputs'],lidc_nodule_not_inc],0)

# Kaggle
kaggle_labels = pd.read_csv('original-datasets/kaggle/stage1_labels.csv')
kaggle_labels_lb1 = pd.read_csv('original-datasets/kaggle/stage1_solution.csv')
kaggle_labels = pd.concat([kaggle_labels, kaggle_labels_lb1])

kaggle_cancer_ids = []
kaggle_nocancer_ids = []
for i in range(len(kaggle_labels)):
    if kaggle_labels.cancer.iloc[i] == 1:
        kaggle_cancer_ids.append(kaggle_labels.id.iloc[i])
    else:
        kaggle_nocancer_ids.append(kaggle_labels.id.iloc[i])
kaggle_submit_ids = list(pd.read_csv('original-datasets/kaggle/stage2_sample_submission.csv').id)

with gzip.open('patches/kaggle-candidate-patches-inc-masses.pkl.gz', 'rb') as f:
    kaggle_data = pickle.load(f)

kaggle_meta = kaggle_data['negative-metadata']
kaggle_x = kaggle_data['negative-inputs']

kaggle_cancer_d = {}
kaggle_nocancer_d = {}
kaggle_submit_d = {}

def add_to_dict(d, x, m, kid):
    if kid in d:
        d[kid][0].append(x)
        d[kid][1].append(m)
    else:
        d[kid] = [[x],[m]]

def array_from_meta(meta):
    return np.array([meta['radius'],
                     meta['zoom_mod'],
                     meta['z-fraction'],
                     meta['y-fraction'],
                     abs(meta['x-fraction']-0.5)
                     ])

# get all meta data and normalize
all_m = np.stack([array_from_meta(m) for m in kaggle_meta], 0)
meta_mean = np.mean(all_m,axis=0,keepdims=True)
meta_stdv = np.std(all_m,axis=0,keepdims=True)
all_m = (all_m - meta_mean) / meta_stdv

for i in range(all_m.shape[0]):
    m = all_m[i]
    x = kaggle_x[i]
    kid = kaggle_meta[i]['id']
    if kid in kaggle_cancer_ids:
        add_to_dict(kaggle_cancer_d, x, m, kid)
    elif kid in kaggle_nocancer_ids:
        add_to_dict(kaggle_nocancer_d, x, m, kid)
    elif kid in kaggle_submit_ids:
        add_to_dict(kaggle_submit_d, x, m, kid)
    else:
        raise ('id ' + kid + ' not in data')

kaggle_cancer = []
kaggle_no_cancer = []
for kid in kaggle_cancer_ids:
    if kid in kaggle_cancer_d:
        kaggle_cancer.append((np.stack(kaggle_cancer_d[kid][0], 0), np.stack(kaggle_cancer_d[kid][1], 0)))

for kid in kaggle_nocancer_ids:
    if kid in kaggle_nocancer_d:
        kaggle_no_cancer.append((np.stack(kaggle_nocancer_d[kid][0], 0),np.stack(kaggle_nocancer_d[kid][1], 0)))

kaggle_c_prior = len(kaggle_cancer)/(len(kaggle_cancer) + len(kaggle_no_cancer))
np_kaggle_prior_logit = np.log(kaggle_c_prior/(1.-kaggle_c_prior))

# for submission
kaggle_submit = []
kaggle_candidates_ids = []
kaggle_nocandidates_ids = []
for kid in kaggle_submit_ids:
    if kid in kaggle_submit_d:
        kaggle_submit.append((np.stack(kaggle_submit_d[kid][0], 0), np.stack(kaggle_submit_d[kid][1], 0)))
        kaggle_candidates_ids.append(kid)
    else:
        kaggle_nocandidates_ids.append(kid)

class DataIterator(object):
    def __init__(self, subset, nr_kaggle_scans, holdout_fraction=0., lidc_batch_size=32, seed=1):
        self.rng = np.random.RandomState(seed)

        # permute Kaggle data, so we can split into train and test later
        kaggle_cancer_perm = [kaggle_cancer[i] for i in self.rng.permutation(len(kaggle_cancer))]
        kaggle_no_cancer_perm = [kaggle_no_cancer[i] for i in self.rng.permutation(len(kaggle_no_cancer))]

        self.subset = subset
        self.nr_kaggle_scans = nr_kaggle_scans
        self.holdout_fraction = holdout_fraction
        self.lidc_batch_size = lidc_batch_size
        self.p = 0  # pointer to where we are in iteration

        # gather the data
        self.lidc_pos = lidc_pos
        self.lidc_neg = lidc_neg

        if holdout_fraction>0:
            nvc = int(holdout_fraction * len(kaggle_cancer_perm))
            nvnc = int(holdout_fraction * len(kaggle_no_cancer_perm))
            self.kaggle_cancer = kaggle_cancer_perm[nvc:]
            self.kaggle_no_cancer = kaggle_no_cancer_perm[nvnc:]
        else:
            self.kaggle_cancer = kaggle_cancer_perm
            self.kaggle_no_cancer = kaggle_no_cancer_perm

        self.nkc = len(self.kaggle_cancer)
        self.nknc = len(self.kaggle_no_cancer)
        self.nr_batches = int(np.ceil(np.maximum(self.nkc,self.nknc) / (0.5*nr_kaggle_scans)))

        if subset == 'validation':
            assert(holdout_fraction>0)
            nvc = int(holdout_fraction * len(kaggle_cancer_perm))
            nvnc = int(holdout_fraction * len(kaggle_no_cancer_perm))
            self.kaggle_joint = kaggle_cancer_perm[:nvc] + kaggle_no_cancer_perm[:nvnc]
            self.labels = np.concatenate([np.ones(nvc,dtype=np.float32), np.zeros(nvnc,dtype=np.float32)])
            self.n = nvc+nvnc
            self.nr_batches = int(np.ceil(self.n/nr_kaggle_scans))

        elif subset == 'submission':
            self.kaggle_joint = kaggle_submit
            self.n = len(self.kaggle_joint)
            self.nr_batches = int(np.ceil(self.n / nr_kaggle_scans))

    def reset(self):
        self.p = 0

    def __iter__(self):
        return self

    def __next__(self):

        # on first iteration lazily permute all training data
        if self.p == 0:
            lidc_pos_perm = self.rng.permutation(self.lidc_pos[0].shape[0])
            self.lidc_pos = [self.lidc_pos[0][lidc_pos_perm], self.lidc_pos[1][lidc_pos_perm]]
            lidc_neg_perm = self.rng.permutation(self.lidc_neg.shape[0])
            self.lidc_neg = self.lidc_neg[lidc_neg_perm]
            k_pos_perm = self.rng.permutation(len(self.kaggle_cancer))
            self.kaggle_cancer = [self.kaggle_cancer[i] for i in k_pos_perm]
            k_neg_perm = self.rng.permutation(len(self.kaggle_no_cancer))
            self.kaggle_no_cancer = [self.kaggle_no_cancer[i] for i in k_neg_perm]

        # on last iteration reset the counter and raise StopIteration
        if self.p == self.nr_batches:
            self.reset()  # reset for next time we get called
            raise StopIteration

        # on intermediate iterations fetch the next batch
        npd = {'kaggle_prior_logit': np_kaggle_prior_logit}
        lidc_ind = np.arange(self.p * self.lidc_batch_size, (self.p + 1) * self.lidc_batch_size)
        npd['x_lidc_0'] = self.augment(np.take(self.lidc_pos[0], lidc_ind, axis=0, mode='wrap'))
        npd['x_lidc_1'] = self.augment(np.take(self.lidc_neg, lidc_ind, axis=0, mode='wrap'))
        npd['y_lidc'] = np.take(self.lidc_pos[1], lidc_ind, axis=0, mode='wrap')

        # kaggle data
        if self.subset == 'train':
            # same number of positives and negatives
            kxp = []
            kmp = []
            kxn = []
            kmn = []
            p_length = []
            n_length = []
            for i in range(self.p * self.nr_kaggle_scans // 2, (self.p + 1) * self.nr_kaggle_scans // 2):
                ind_p = i % self.nkc
                ind_n = i % self.nknc

                kxp.append(self.augment(self.kaggle_cancer[ind_p][0]))
                kmp.append(self.kaggle_cancer[ind_p][1])
                p_length.append(kxp[-1].shape[0])

                kxn.append(self.augment(self.kaggle_no_cancer[ind_n][0]))
                kmn.append(self.kaggle_no_cancer[ind_n][1])
                n_length.append(kxn[-1].shape[0])

            kx_all = kxp + kxn
            km_all = kmp + kmn
            all_length = p_length+n_length

        else:
            kx_all = []
            km_all = []
            all_length = []
            inds = []
            for i in range(self.p * self.nr_kaggle_scans, (self.p + 1) * self.nr_kaggle_scans):
                j = i%self.n
                kx_all.append(self.augment(self.kaggle_joint[j][0]))
                km_all.append(self.kaggle_joint[j][1])
                all_length.append(self.kaggle_joint[j][0].shape[0])
                inds.append(j)

        # expand to be devisible by 6
        ntot = sum(all_length)
        addn = 6 - (ntot % 6)
        kx_tot = np.concatenate(kx_all + [np.zeros((addn,12,32,32),dtype=np.float32)],0)
        km_tot = np.concatenate(km_all + [np.zeros((addn, km_all[-1].shape[1]), dtype=np.float32)],0)
        ntot += addn

        masks = np.zeros((ntot, self.nr_kaggle_scans), dtype=np.float32)
        counter = 0
        for i in range(self.nr_kaggle_scans):
            n = all_length[i]
            masks[counter:(counter+n),i] = 1.
            counter += n

        npd['x_kaggle'] = kx_tot
        npd['m_kaggle'] = km_tot
        npd['masks'] = masks

        self.p += 1

        if self.subset == 'train':
            return npd
        else:
            return npd, np.array(inds)

    # data augmentation function
    def augment(self, patch, shift=None):
        if shift is None:
            shift = (self.subset == 'train')
        n = patch.shape[0]
        patch_out = np.zeros((n, 12, 32, 32), dtype=np.float32)

        for i in range(n):
            p = patch[i, :, :, :]
            if shift:
                zc = self.rng.randint(3)
                yc = self.rng.randint(5)
                xc = self.rng.randint(5)
            else:
                zc = 1
                yc = 2
                xc = 2
            p = p[zc:zc + 12, yc:yc + 32, xc:xc + 32].astype(np.float32) / 255.

            if self.rng.rand() > 0.5:
                p = p[:, :, ::-1]
            if self.rng.rand() > 0.5:
                p = p[:, ::-1, :]
            if self.rng.rand() > 0.5:
                p = p[::-1, :, :]
            if self.rng.rand() > 0.5:
                p = np.transpose(p, (0, 2, 1))

            patch_out[i, :, :, :] = p

        return patch_out
