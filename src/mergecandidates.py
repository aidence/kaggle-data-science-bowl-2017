# Copyright (C) 2017  Aidence B.V.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA

import argparse
import glob
import pickle
import os

from util.util import Util

import inference


def main():
    # Parse command line options.
    parser = argparse.ArgumentParser(description='Merges candidates.',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('output_directory', metavar='output-directory', type=str, help='Directory to store candidates.')
    parser.add_argument('candidate_directories', metavar='candidate-directories', type=str, nargs='+',
                        help='Directories where candidates are stored.')
    parser.add_argument('--max-distance', metavar='max-distance', type=float, default=3, help='Maximum merging distance.')

    args = parser.parse_args()

    # Create directory.
    os.makedirs(args.output_directory, exist_ok=True)

    # Get pickle files.
    file_names = []

    for candidate_directory in args.candidate_directories:
        file_names += glob.glob(os.path.join(candidate_directory, 'candidates-*.pkl'))

    print('Found {} candidate files.'.format(len(file_names)))

    # Read them.
    entries = []

    for file_name in file_names:
        with open(file_name, 'rb') as f:
            entries += pickle.load(f)

    print('Read {} scan entries.'.format(len(entries)))

    # Merge them based on scan id.
    scan_entries = {}
    for entry in entries:
        id = entry['id']
        if id not in scan_entries:
            # Set.
            entry['mode'] = None
            entry['threshold'] = None
            entry['size-threshold'] = None
            entry['in-mask'] = None
            entry['large'] = None
            entry['positives'] = None
            entry['negatives'] = None

            scan_entries[id] = entry
        else:
            # Append.
            scan_entries[id]['candidates'] += entry['candidates']

    # Process them per scan.
    data_sets = {}
    for id, entry in scan_entries.items():
        # Get data-set.
        data_set_name = entry['data-set']
        if data_set_name in data_sets:
            data_set = data_sets[data_set_name]
        else:
            data_set = Util.load_data_set(data_set_name)

            data_sets[data_set_name] = data_set

        # Get scan.
        scan = data_set.get_scan(id)

        candidates = entry['candidates']

        inference.CandidateCalculator.merge_candidates(candidates, args.max_distance)

        # Write entries.
        entries = [entry]

        file_name = os.path.join(args.output_directory, 'candidates-{}.pkl'.format(id))

        with open(file_name, 'wb') as f:
            pickle.dump(entries, f)


if __name__ == '__main__':
    main()
