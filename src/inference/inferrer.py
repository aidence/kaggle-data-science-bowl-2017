# Copyright (C) 2017  Aidence B.V.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA

import numpy as np


class Inferrer:
    @staticmethod
    def get_patch(volume, center, size, pad_value=0):
        # Calculate start and end indices.
        start_index = np.array((
            int(round(center.z - 0.5 * size.z + 0.5)),
            int(round(center.y - 0.5 * size.y + 0.5)),
            int(round(center.x - 0.5 * size.x + 0.5))
        ))

        end_index = start_index + np.ceil(size.get_as_zyx_tuple()).astype(np.uint32)

        full_size = volume.shape[0:3]

        if np.any(start_index < 0) or np.any(end_index > full_size):
            # We need to pad.
            pad_before = np.maximum(-start_index, 0)
            pad_after = np.maximum(end_index - full_size, 0)

            start_index = np.maximum(start_index, 0)

            # Fetch patch.
            patch = volume[start_index[0]:end_index[0],start_index[1]:end_index[1],start_index[2]:end_index[2]]

            # Pad with 0.
            paddings = (
                (pad_before[0], pad_after[0]),
                (pad_before[1], pad_after[1]),
                (pad_before[2], pad_after[2])
            )

            paddings += ((0, 0),) * (volume.ndim - 3)

            patch = np.pad(patch, paddings, 'constant', constant_values=pad_value)

            return patch
        else:
            return volume[start_index[0]:end_index[0], start_index[1]:end_index[1], start_index[2]:end_index[2]]
