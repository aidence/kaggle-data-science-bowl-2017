# Copyright (C) 2017  Aidence B.V.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA

import numpy as np

from util.point import Point3D


class Evaluator:
    def __init__(self, model, maximum_block_size=Point3D(64, 256, 256), pad_value=-1):
        self.model = model
        self.maximum_block_size = maximum_block_size.get_as_zyx_array()
        self.filter_size = model.get_filter_size().get_as_zyx_array()
        self.margin_left = (self.filter_size - 1) // 2
        self.margin_right = self.filter_size - 1 - self.margin_left
        self.num_gpus = model.get_context().get_num_gpus()
        self.pad_value = pad_value

    def evaluate(self, volume, start, size, captures):
        """
        Evaluates model over volume using given bounding box.
        """

        # Walk over volume.
        full_size = volume.shape[:-1]

        start = start.get_as_zyx_array()
        size = size.get_as_zyx_array()
        end = start + size

        z_outputs = []
        for z in range(start[0], end[0], self.maximum_block_size[0]):
            y_outputs = []
            for y in range(start[1], end[1], self.maximum_block_size[1]):
                x_outputs = []
                for x in range(start[2], end[2], self.maximum_block_size[2]):
                    # Get block dimensions.
                    point = np.array((z, y, x))
                    block_start = point - self.margin_left
                    block_end = np.minimum(point + self.maximum_block_size, end) + self.margin_right

                    if np.any(block_start < 0) or np.any(block_end > full_size):
                        # We need to pad.
                        pad_before = np.maximum(-block_start, 0)
                        pad_after = np.maximum(block_end - full_size, 0)

                        block_start = np.maximum(block_start, 0)

                        # Fetch block.
                        block = volume[
                            block_start[0]:block_end[0],
                            block_start[1]:block_end[1],
                            block_start[2]:block_end[2],
                            :
                        ]

                        # Pad.
                        block = np.pad(block, (
                                (pad_before[0], pad_after[0]),
                                (pad_before[1], pad_after[1]),
                                (pad_before[2], pad_after[2]),
                                (0, 0)
                            ), 'constant', constant_values=self.pad_value)
                    else:
                        # Fetch block.
                        block = volume[
                            block_start[0]:block_end[0],
                            block_start[1]:block_end[1],
                            block_start[2]:block_end[2],
                            :
                        ]

                    # Reshape to add batch.
                    block = block.reshape((1,) + block.shape)

                    # Evaluate block.
                    outputs = self.model.evaluate(captures, inputs=block)

                    # Reshape to remove batch.
                    reshaped_outputs = [output.reshape(output.shape[1:]) for output in outputs]

                    x_outputs.append(reshaped_outputs)

                # Concatenate in x.
                outputs = [np.concatenate([x_output[i] for x_output in x_outputs], axis=2) for i in range(len(captures))]

                y_outputs.append(outputs)

            # Concatenate in y.
            outputs = [np.concatenate([y_output[i] for y_output in y_outputs], axis=1) for i in range(len(captures))]

            z_outputs.append(outputs)

        # Concatenate in z.
        outputs = [np.concatenate([z_output[i] for z_output in z_outputs], axis=0) for i in range(len(captures))]

        # Create dictionary.
        outputs = {name: output for name, output in zip(captures, outputs)}

        return outputs
