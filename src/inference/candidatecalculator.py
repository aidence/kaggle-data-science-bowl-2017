# Copyright (C) 2017  Aidence B.V.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA

from util.point import Point3D

import numpy as np

import scipy
import scipy.spatial.distance
import scipy.ndimage

import skimage
import skimage.measure


class CandidateCalculator:
    def __init__(self, selection_threshold=None, size_threshold=None, max_candidates=100):
        self.selection_threshold = selection_threshold if selection_threshold is not None else 3.5
        self.size_threshold = size_threshold if size_threshold is not None else 2
        self.max_candidates = max_candidates

        if self.size_threshold > self.selection_threshold:
            raise Exception('Size threshold must be smaller than or equal to selection threshold.')

    def calculate(self, logits, start, world_offset, world_scale, mask=None, large=False, in_mask=True, get_radius=True, get_distance=True):
        # Get world scale and offset.
        world_scale = world_scale.get_as_zyx_array()
        world_offset = world_offset.get_as_zyx_array()

        # Adjust world scale in large mode.
        if large:
            world_scale *= (2, 4, 4)

        # Reshape and copy logits. Make sure there are no nans anymore.
        logits = np.copy(logits[:,:,:,0])

        logits[np.isnan(logits)] = self.size_threshold - 100

        # Lower threshold.
        size_mask = logits > self.size_threshold

        # Erode to remove small artifacts.
        size_mask = scipy.ndimage.morphology.binary_erosion(size_mask)

        # Higher threshold.
        selection_mask = logits > self.selection_threshold

        # Erode to remove small artifacts.
        selection_mask = scipy.ndimage.morphology.binary_erosion(selection_mask)

        # Label.
        labeled, num_labels = scipy.ndimage.label(size_mask, structure=np.ones((3, 3, 3), dtype=np.uint8))
        if not num_labels:
            return []

        # Select labels of which small mask occurs within large mask.
        label_numbers = list(range(1, num_labels + 1))

        counts = scipy.ndimage.measurements.sum(selection_mask, labeled, label_numbers)

        label_numbers = np.array([number for number, count in zip(label_numbers, counts) if count >= 1])
        if not len(label_numbers):
            return []

        # Calculate maximum probability per label.
        maxima = scipy.ndimage.measurements.maximum(logits, labeled, label_numbers)
        maxima = 1 / (1 + np.exp(-maxima))

        # Rescale from 0 to 1.
        threshold_probability = 1 / (1 + np.exp(-self.size_threshold))

        maxima = (maxima - threshold_probability) / (1 - threshold_probability)

        # Sort candidates and cut them off.
        #indices = np.argsort(maxima)[::-1][0:self.max_candidates]

        #maxima = maxima[indices]
        #label_numbers = label_numbers[indices]

        # Calculate center of mass for each label.
        centers = scipy.ndimage.measurements.center_of_mass(labeled, labeled, label_numbers)

        centers += start.get_as_zyx_array()

        # Convert centers to world coordinates.
        world_centers = centers * world_scale + world_offset

        # Create candidates.
        candidates = [{'center': Point3D.from_zyx_list(world_center),
                       'center-in-pixels': Point3D.from_zyx_list(center),
                       'candidate-probability': maximum,
                       'label': label,
                       'large': large}
                      for world_center, center, maximum, label in zip(world_centers, centers, maxima, label_numbers)]

        # Filter based on mask.
        if mask is not None:
            candidates = self.filter_with_mask(candidates, mask, world_scale, large, in_mask, get_distance)
            if not candidates:
                return []

        # Get radii.
        if get_radius:
            self.augment_with_radii(candidates, labeled, world_scale)

        return candidates

    def filter_with_mask(self, candidates, mask, world_scale, large, in_mask, get_distance):
        # In large mode we do not use masking.
        if large:
            in_mask = False

        # Get mask.
        mask_array = mask.get_mask()

        mask_start = mask.get_start().get_as_zyx_array()
        mask_end = mask.get_end().get_as_zyx_array()
        mask_size = (mask_end - mask_start).astype(np.float32)

        # Scale mask in large mode.
        if large:
            mask_array = mask_array[::2, ::4, ::4]

            mask_start //= (2, 4, 4)
            mask_end //= (2, 4, 4)
            mask_size //= (2, 4, 4)

        # Get distances for each pixel in mask to nearest non-lung tissue.
        distances = None
        mask_subset_start = None
        if get_distance:
            if not in_mask:
                # Do whole volume.
                mask_subset_start = (0, 0, 0)
                mask_array_subset = mask_array
            else:
                # Select mask subvolume.
                mask_subset_start = np.maximum(mask_start - 1, 0)
                mask_subset_end = np.minimum(mask_end + 1, mask_array.shape)

                mask_array_subset = mask_array[
                    mask_subset_start[0]:mask_subset_end[0],
                    mask_subset_start[1]:mask_subset_end[1],
                    mask_subset_start[2]:mask_subset_end[2]
                ]

            distances = scipy.ndimage.morphology.distance_transform_edt(mask_array_subset, sampling=world_scale)

        # Get candidates that are within mask.
        new_candidates = []
        for candidate in candidates:
            center = candidate['center-in-pixels'].get_as_zyx_array()
            index = np.round(center).astype(np.int32)

            candidate_in_mask = not in_mask or mask_array[index[0], index[1], index[2]]

            if candidate_in_mask:
                # Get distance to nearest non-lung tissue.
                if get_distance:
                    distance_index = index - mask_subset_start

                    distance = distances[distance_index[0], distance_index[1], distance_index[2]]
                else:
                    distance = None

                # Calculate at which fraction center occurs.
                fraction = (center - mask_start) / mask_size

                # Augment candidate.
                candidate['distance'] = distance
                candidate['z-fraction'] = fraction[0]
                candidate['y-fraction'] = fraction[1]
                candidate['x-fraction'] = fraction[2]

                # Append candidate.
                new_candidates.append(candidate)

        return new_candidates

    def augment_with_radii(self, candidates, labeled, world_scale):
        label_numbers = [candidate['label'] for candidate in candidates]
        objects = scipy.ndimage.find_objects(labeled, max_label=np.max(label_numbers))

        for candidate in candidates:
            # Get label.
            label_number = candidate['label']
            object = objects[label_number - 1]
            label_part = (labeled[object] == label_number)
            label_part_shape = np.array(label_part.shape)

            # Surround label with zeros.
            surrounded_label_part = np.zeros(label_part_shape + 2, dtype=np.bool)

            surrounded_label_part[1:-1, 1:-1, 1:-1] = label_part

            # Get mesh.
            verts_3d, _ = skimage.measure.marching_cubes(surrounded_label_part, 0, spacing=world_scale)

            verts_2d = verts_3d[:, 1:3]

            # Get statistics.
            centroid_2d = np.mean(verts_2d, axis=0)

            centroid_distances_2d = scipy.spatial.distance.cdist(verts_2d, centroid_2d.reshape(1, 2))
            pairwise_distances_2d = scipy.spatial.distance.pdist(verts_2d)

            mean_centroid_distance = np.mean(centroid_distances_2d)
            min_centroid_distance = np.min(centroid_distances_2d)
            max_centroid_distance = np.max(centroid_distances_2d)

            mean_pairwise_distance = np.mean(pairwise_distances_2d)
            min_pairwise_distance = np.min(pairwise_distances_2d)
            max_pairwise_distance = np.max(pairwise_distances_2d)

            # Determine radius.
            radius = CandidateCalculator.get_radius(mean_centroid_distance)

            # Set it on candidate.
            candidate['mean-centroid-distance'] = mean_centroid_distance
            candidate['min-centroid-distance'] = min_centroid_distance
            candidate['max-centroid-distance'] = max_centroid_distance

            candidate['mean-pairwise-distance'] = mean_pairwise_distance
            candidate['min-pairwise-distance'] = min_pairwise_distance
            candidate['max-pairwise-distance'] = max_pairwise_distance

            candidate['radius'] = radius
            candidate['diameter'] = 2 * radius
            candidate['volume'] = 4/3 * np.pi * radius ** 3

    @staticmethod
    def merge_candidates(candidates, maximum_distance=3):
        # Check for empty case.
        if not candidates:
            return []

        # Sort by radius.
        candidates.sort(key=lambda x: x['radius'], reverse=True)

        # Get centers and radii.
        centers = [candidate['center'].get_as_zyx_array() for candidate in candidates]
        radii = [candidate['radius'] for candidate in candidates]

        # Get distances.
        distances = scipy.spatial.distance.squareform(scipy.spatial.distance.pdist(centers))

        removed = np.zeros(len(candidates), dtype=np.bool)

        for i in range(len(candidates)):
            # Skip if removed.
            if removed[i]:
                continue

            j = i + 1
            while j < len(candidates):
                # Skip if removed.
                if removed[j]:
                    j += 1
                    continue

                # Check whether to merge.
                distance = distances[i, j] - radii[i] - radii[j]
                if distance < maximum_distance:
                    if distance < 0:
                        # Candidate is contained in other.
                        pass
                    else:
                        # Find enclosing sphere.
                        center_distance = distances[i, j]
                        theta = 0.5 + (radii[i] - radii[j]) / (2 * center_distance)

                        new_radius = 0.5 * (center_distance + radii[i] + radii[j])
                        new_center = theta * centers[i] + (1 - theta) * centers[j]

                        # Update candidate.
                        candidates[i]['center'] = Point3D.from_zyx_list(new_center)
                        candidates[i]['radius'] = new_radius

                        # Update centers and radii.
                        centers[i] = new_center
                        radii[i] = new_radius

                        # Update distances.
                        new_distances = scipy.spatial.distance.cdist(centers, new_center.reshape(1, 3))

                        distances[i] = new_distances.flatten()
                        distances[:,i] = new_distances.flatten()

                    # Remove other.
                    removed[j] = True

                    # Reset.
                    j = i + 1

                # Advance.
                j += 1

        # Remove candidates.
        indices = np.nonzero(~removed)[0]

        candidates = [candidates[i] for i in indices]

        return candidates

    @staticmethod
    def get_radius(x):
        if x <= 1.81147:
            return 1.320298 * x
        if x >= 7:
            return 1.147 + 1.319 * x

        return -0.01634235 * x ** 3 + 0.21587463 * x ** 2 + 0.69907582 * x + 0.51409029
