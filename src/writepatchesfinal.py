# Copyright (C) 2017  Aidence B.V.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA

import glob
import argparse
import os
import pickle
import time

import numpy as np
from scipy.ndimage import zoom

from util.util import Util
from util.point import Point3D
from inference.inferrer import Inferrer


def worker_process_main(entry_queue, patch_queue, size):
    import queue
    import traceback

    size_tuple = size.get_as_zyx_tuple()
    extract_size = Point3D.from_zyx_list([3*x for x in size_tuple])

    # Process entries.
    data_sets = {}
    while True:
        # Try to get an entry.
        try:
            entry = entry_queue.get_nowait()
        except queue.Empty:
            break

        try:
            # Get data-set.
            data_set_name = entry['data-set']
            if data_set_name in data_sets:
                data_set = data_sets[data_set_name]
            else:
                data_set = Util.load_data_set(data_set_name)

                data_sets[data_set_name] = data_set

            # Get volume.
            scan = data_set.get_scan(entry['id'])

            volume = scan.get_volume()

            offset = scan.get_offset().get_as_zyx_array()
            scale = scan.get_scale().get_as_zyx_array()

            # Get patches.
            positives = []
            negatives = []
            pos_metadata = []
            neg_metadata = []

            def get_patch(candidate):
                center = Point3D.from_zyx_list((candidate['center'].get_as_zyx_array() - offset) / scale)
                patch = Inferrer.get_patch(volume, center, extract_size)
                zoom_scale = np.minimum(np.maximum(5. / candidate['radius'], 0.4), 1.)
                if zoom_scale < 1.:
                    patch = zoom(patch, zoom_scale)
                o = np.round((np.array(patch.shape)-np.array(size_tuple))/2.).astype(np.int32)
                patch = patch[o[0]:o[0]+size_tuple[0],o[1]:o[1]+size_tuple[1],o[2]:o[2]+size_tuple[2]]
                candidate.update({'id': entry['id'], 'zoom_mod': 1./zoom_scale - 1.})
                return patch, candidate

            if entry['positives'] is not None:
                for candidate in entry['positives']:
                    patch, candidate = get_patch(candidate)
                    positives.append(patch)
                    pos_metadata.append(candidate)
            if entry['negatives'] is not None:
                for candidate in entry['negatives']:
                    patch, candidate = get_patch(candidate)
                    negatives.append(patch)
                    neg_metadata.append(candidate)
            elif entry['candidates'] is not None:
                for candidate in entry['candidates']:
                    patch, candidate = get_patch(candidate)
                    negatives.append(patch)
                    neg_metadata.append(candidate)

        except KeyboardInterrupt:
            raise
        except:
            traceback.print_exc()

            print('Processing id {} failed.'.format(entry['id']))
            continue

        # Put them in queue.
        patch_queue.put((positives, pos_metadata, negatives, neg_metadata))


def accumulator_process_main(patch_queue, num_entries, output_file_name, seed):
    import gzip

    # Accumulate.
    num_seen = 0
    positives = []
    pos_metadata = []
    negatives = []
    neg_metadata = []

    while True:
        # Get an entry.
        entry = patch_queue.get()

        # Check whether we are done.
        if entry is None:
            break

        positives += entry[0]
        pos_metadata += entry[1]
        negatives += entry[2]
        neg_metadata += entry[3]

        num_seen += 1

        if num_seen % 10 == 0:
            print('{}/{} ({:.1%}), positives: {}, negatives: {}.'.format(
                num_seen,
                num_entries,
                num_seen / num_entries,
                len(positives),
                len(negatives)))

    if num_seen % 10 != 0:
        print('{}/{} ({:.1%}), positives: {}, negatives: {}.'.format(
            num_seen,
            num_entries,
            num_seen / num_entries,
            len(positives),
            len(negatives)))

    # Turn them into numpy arrays.
    positives = np.array(positives)
    negatives = np.array(negatives)

    # get rid of Point3D class
    for d in pos_metadata+neg_metadata:
        for k,v in d.items():
            if isinstance(v, Point3D):
                d[k] = v.get_as_zyx_tuple()
            elif isinstance(v, dict):
                for k2, v2 in v.items():
                    if isinstance(v2, Point3D):
                        v[k2] = v2.get_as_zyx_tuple()

    # Write patches.
    data = {
        'positive-inputs': positives,
        'positive-metadata': pos_metadata,
        'negative-inputs': negatives,
        'negative-metadata': neg_metadata,
    }

    with gzip.open(output_file_name, 'wb', compresslevel=3) as f:
        pickle.dump(data, f)


def main():
    # Parse command line options.
    parser = argparse.ArgumentParser(description='Write patches from candidates.',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('size', metavar='size', type=str, help='Size of patch (z, y, x).')
    parser.add_argument('candidate_directories', metavar='candidate-directories', type=str, nargs='+',
                        help='Directories where candidates are stored.')
    parser.add_argument('--output-file-name', metavar='output-file-name', type=str, default='candidate-patches.pkl.gz',
                        help='Output file name.')
    parser.add_argument('--num-processes', metavar='num-processes', type=int, help='Number of processes to use.')
    parser.add_argument('--seed', metavar='seed', type=int, default=42, help='Seed to use.')


    args = parser.parse_args()

    # Get size.
    size = Util.parse_3d_point(args.size)

    print('Using patch size: {}x{}x{}.'.format(size.z, size.y, size.x))

    # Get pickle files.
    file_names = []

    for candidate_directory in args.candidate_directories:
        file_names += glob.glob(os.path.join(candidate_directory, 'candidates-*.pkl'))

    print('Found {} candidate files.'.format(len(file_names)))

    # Read them.
    entries = []

    for file_name in file_names:
        with open(file_name, 'rb') as f:
            entries += pickle.load(f)

    print('Read {} scan entries.'.format(len(entries)))

    # Distribute it.
    Util.distribute_cpu_work(entries, worker_process_main, accumulator_process_main,
                             num_processes=args.num_processes,
                             worker_extra_args=(size,),
                             accumulator_extra_args=(args.output_file_name, args.seed))


if __name__ == '__main__':
    main()
