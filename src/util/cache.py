# Copyright (C) 2017  Aidence B.V.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA

import os


class Cache:
    def __init__(self, directory, the_class, extensions='pkl'):
        self.directory = directory
        self.the_class = the_class
        self.extension = extensions

        os.makedirs(self.directory, exist_ok=True)

    def get(self, id, *args, **kwargs):
        file_name = self.get_file_name(id)

        if os.path.exists(file_name):
            # Load existing.
            return self.the_class.load(file_name)
        else:
            # Create new.
            instance = self.the_class(*args, **kwargs)

            instance.save(file_name)

            return instance

    def invalidate(self, id):
        file_name = self.get_file_name(id)

        if os.path.exists(file_name):
            os.remove(file_name)

    def get_file_name(self, id):
        return os.path.join(self.directory, str(id) + '.' + self.extension)
