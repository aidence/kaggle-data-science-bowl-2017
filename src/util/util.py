# Copyright (C) 2017  Aidence B.V.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA

import importlib
import sys
import multiprocessing
import re
import os

from .point import Point3D

import numpy as np


# Worker target must be a global in order to be pickled.
def worker_target(num_done, num_total, result_queue, worker, *args, **kwargs):
    try:
        worker(*args, **kwargs)
    finally:
        with num_done.get_lock():
            num_done.value += 1

            if num_done.value == num_total:
                result_queue.put(None)


class Util:
    @staticmethod
    def get_scan_ids(config):
        # Get data-set.
        data_set = Util.load_data_set(config['data-set'])

        # Create random state.
        random_state = np.random.RandomState(config.get('seed', 42))

        # Get positive scans.
        all_positive_ids = data_set.get_positive_scan_ids()
        num_nodules = [len(data_set.get_scan(id).get_included_nodules()) for id in all_positive_ids]

        # Sort by number of nodules ascending.
        if config.get('sort-positives', True):
            sorting_indices = np.argsort(num_nodules)[::-1]

            all_positive_ids = np.array(all_positive_ids)[sorting_indices]
            num_nodules = np.array(num_nodules)[sorting_indices]
        elif config.get('shuffle-positives', True):
            all_positive_ids = all_positive_ids.copy()
            random_state.shuffle(all_positive_ids)

        # Take first.
        if 'positive-scans' in config:
            all_positive_ids = all_positive_ids[0:config['positive-scans']]

        # Check nodule requirements.
        positive_ids = []
        num_total_nodules = 0
        for id, num in zip(all_positive_ids, num_nodules):
            if 'max-nodules' in config and num_total_nodules + num > config['max-nodules']:
                continue

            if config.get('min-scan-nodules', 0) <= num <= config.get('max-scan-nodules', np.inf):
                positive_ids.append(id)
                num_total_nodules += num

        # Get negative scans.
        negative_ids = data_set.get_negative_scan_ids()

        if config.get('shuffle-negatives', True):
            negative_ids = negative_ids.copy()
            random_state.shuffle(negative_ids)

        if 'negative-scans' in config:
            negative_ids = negative_ids[0:config['negative-scans']]

        return positive_ids + negative_ids, len(positive_ids), len(negative_ids), num_total_nodules

    @staticmethod
    def load_data_set(name):
        import data

        # Load data set.
        if name == 'lidc':
            return data.LIDCDataSet()
        elif name == 'kaggle':
            return data.KaggleDataSet()
        else:
            raise Exception('Unknown data-set.')

    @staticmethod
    def create_model(config, input_shape, output_shape, num_cores=None, use_gpus=None):
        import model

        # Create context with given info.
        context = model.Context(num_cores, use_gpus)

        # Get model parameters.
        parameters = {}
        for name, value in config.items():
            if name in ('class', 'directory', 'step'):
                continue

            parameters[Util.process_config_name(name, underscore=False)] = Util.process_config_value(value)

        # Create model.
        the_class = Util.get_class(config['class'])

        return the_class(context=context, input_shape=input_shape, output_shape=output_shape, parameters=parameters)

    @staticmethod
    def load_model(config, num_cores=None, use_gpus=None, input_shape=None, output_shape=None):
        import model

        # Create context with given info.
        context = model.Context(num_cores, use_gpus)

        # Get model parameters.
        parameters = {}
        for name, value in config.items():
            if name in ('class', 'directory', 'step'):
                continue

            parameters[Util.process_config_name(name, underscore=False)] = Util.process_config_value(value)

        # Load model.
        the_class = Util.get_class(config['class'])

        directory = os.path.join(config['directory'], 'models')

        the_model = the_class.load(context, directory, config['step'],
                                   input_shape=input_shape,
                                   output_shape=output_shape,
                                   parameters=parameters)

        return the_model

    @staticmethod
    def get_class(name):
        parts = name.split('.')

        if len(parts) != 2:
            raise Exception('Class name must have two components.')

        module_name = '.'.join(parts[:-1])

        if module_name not in sys.modules:
            try:
                importlib.import_module(module_name)
            except ImportError:
                raise Exception('Class module not found.')

        module = sys.modules[parts[0]]
        if not hasattr(module, parts[1]):
            raise Exception('Class name not found.')

        return getattr(module, parts[1])

    @staticmethod
    def process_config_name(name, underscore=True):
        if underscore:
            return name.replace('-', '_')
        else:
            return name.replace('_', '-')

    @staticmethod
    def process_config_value(value, extra_arguments={}):
        # Check whether it is a simple type.
        if not isinstance(value, dict) or 'class' not in value:
            return value

        # Get class.
        class_name = value['class']

        the_class = Util.get_class(class_name)

        # Start with extra arguments that exist.
        function_code = the_class.__init__.__code__

        argument_names = set(function_code.co_varnames[1:function_code.co_argcount])

        arguments = {name: argument for name, argument in extra_arguments.items() if name in argument_names}

        # Add given arguments.
        for name, argument in value.items():
            if name == 'class':
                continue

            arguments[Util.process_config_name(name)] = Util.process_config_value(argument, extra_arguments)

        return the_class(**arguments)

    @staticmethod
    def parse_3d_point(point, dtype=int):
        return Point3D.from_zyx_list(list(map(dtype, re.split('[\sxX_,/\\\\|]+', point))))

    @staticmethod
    def parse_list(value):
        return list(filter(None, map(lambda x: x.strip(), re.split('[\s,/\\\\|]+', value))))

    @staticmethod
    def distribute_cpu_work(work, worker, accumulator, shuffle=True, num_processes=None,
                            worker_extra_args=(), worker_extra_kwargs={},
                            accumulator_extra_args=(), accumulator_extra_kwargs={}):
        # Shuffle work.
        if shuffle:
            random_state = np.random.RandomState(42)
            random_state.shuffle(work)

        # Put work in a queue.
        work_queue = multiprocessing.Queue()

        for entry in work:
            work_queue.put(entry)

        # Create result queue.
        result_queue = multiprocessing.Queue()

        # Start processes.
        if num_processes is None:
            num_processes = multiprocessing.cpu_count()

        num_done = multiprocessing.Value('i', 0)

        processes = []
        for i in range(num_processes):
            worker_args = (work_queue, result_queue) + worker_extra_args
            process_args = (num_done, num_processes, result_queue, worker) + worker_args

            process = multiprocessing.Process(target=worker_target, args=process_args, kwargs=worker_extra_kwargs)
            process.start()

            processes.append(process)

        process_args = (result_queue, len(work)) + accumulator_extra_args

        process = multiprocessing.Process(target=accumulator, args=process_args, kwargs=accumulator_extra_kwargs)
        process.start()

        processes.append(process)

        # Join them.
        for process in processes:
            process.join()

    @staticmethod
    def distribute_gpu_work(work, worker, accumulator, shuffle=True, gpus=None,
                            worker_extra_args=(), worker_extra_kwargs={},
                            accumulator_extra_args=(), accumulator_extra_kwargs={}):
        import model

        # Must spawn because cuda is not thread safe.
        # Forked processes will think cuda is already initialized otherwise.
        multiprocessing.set_start_method('spawn')

        # Shuffle work.
        if shuffle:
            random_state = np.random.RandomState(42)
            random_state.shuffle(work)

        # Put work in a queue.
        work_queue = multiprocessing.Queue()

        for entry in work:
            work_queue.put(entry)

        # Create result queue.
        result_queue = multiprocessing.Queue()

        # Determine GPUs to use.
        if gpus is None:
            # Use all GPUs.
            gpu_indices = list(range(model.Context.get_total_num_gpus()))
        elif isinstance(gpus, str):
            # Comma separated string of GPU numbers (one-based) to use.
            gpu_indices = list(map(lambda x: int(x) - 1, gpus.split(',')))
        elif isinstance(gpus, list):
            # List of GPU numbers (one-based) to use.
            gpu_indices = list(map(lambda x: int(x) - 1, gpus))
        else:
            # Single GPU number to use.
            gpu_indices = [int(gpus) - 1]

        num_done = multiprocessing.Value('i', 0)
        num_gpus = len(gpu_indices)

        processes = []
        for gpu_index in gpu_indices:
            worker_args = (gpu_index, work_queue, result_queue) + worker_extra_args
            process_args = (num_done, num_gpus, result_queue, worker) + worker_args

            process = multiprocessing.Process(target=worker_target, args=process_args, kwargs=worker_extra_kwargs)
            process.start()

            processes.append(process)

        process_args = (result_queue, len(work)) + accumulator_extra_args

        process = multiprocessing.Process(target=accumulator, args=process_args, kwargs=accumulator_extra_kwargs)
        process.start()

        processes.append(process)

        # Join them.
        for process in processes:
            process.join()
