# Copyright (C) 2017  Aidence B.V.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA

import numpy as np


class Point3D:
    def __init__(self, z, y, x, dtype=None):
        self.z = z
        self.y = y
        self.x = x
        self.dtype = dtype

    def get_as_xyz_tuple(self):
        return (self.x, self.y, self.z)

    def get_as_zyx_tuple(self):
        return (self.z, self.y, self.x)

    def get_as_xyz_array(self):
        return np.array((self.x, self.y, self.z), dtype=self.dtype)

    def get_as_zyx_array(self):
        return np.array((self.z, self.y, self.x), dtype=self.dtype)

    def __repr__(self):
        return '{}(z={}, y={}, x={})'.format(self.__class__.__name__, self.z, self.y, self.x)

    @staticmethod
    def from_xyz_list(l, dtype=None):
        return Point3D(l[2], l[1], l[0], dtype=dtype)

    @staticmethod
    def from_zyx_list(l, dtype=None):
        return Point3D(l[0], l[1], l[2], dtype=dtype)


class Point2D:
    def __init__(self, y, x, dtype=None):
        self.y = y
        self.x = x
        self.dtype = dtype

    def get_as_xy_tuple(self):
        return (self.x, self.y)

    def get_as_yx_tuple(self):
        return (self.y, self.x)

    def get_as_xy_array(self):
        return np.array((self.x, self.y), dtype=self.dtype)

    def get_as_yx_array(self):
        return np.array((self.y, self.x), dtype=self.dtype)

    @staticmethod
    def from_xy_list(l, dtype=None):
        return Point2D(l[1], l[0], dtype=dtype)

    @staticmethod
    def from_yx_list(l, dtype=None):
        return Point2D(l[0], l[1], dtype=dtype)
