# Copyright (C) 2017  Aidence B.V.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA

import argparse
import pickle
import glob
import os

import numpy as np

from util.util import Util


def main():
    # Parse command line options.
    parser = argparse.ArgumentParser(description='Writes candidate parameters.',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--threshold-percentile', metavar='threshold-percentile', type=float, help='Seed to use for model.')
    parser.add_argument('--size-percentile', metavar='size-percentile', type=float, help='Seed to use for model.')
    parser.add_argument('logit_directories', metavar='logit-directories', type=str, nargs='+',
                        help='Directories where logits are stored.')

    args = parser.parse_args()

    # Get pickle files.
    file_names = []

    for logits_directory in args.logit_directories:
        file_names += glob.glob(os.path.join(logits_directory, 'logits-*.pkl'))

    print('Found {} logit files.'.format(len(file_names)))

    # Determine ids.
    file_names_per_id = {}
    for file_name in file_names:
        id = os.path.basename(file_name)[len('logits-'):-len('.pkl')]

        file_names_per_id[id] = file_name

    # Get 10 scans with the most nodules.
    scan_ids, _, _, _ = Util.get_scan_ids({'data-set': 'lidc', 'positive-scans': 10, 'negative-scans': 0})

    all_logits = []
    for scan_id in scan_ids:
        with open(file_names_per_id[scan_id], 'rb') as f:
            data = pickle.load(f)

        all_logits.append(data['logits'].flatten())

    all_logits = np.concatenate(all_logits)

    all_logits.sort()

    # Show statistics.
    print('Mean: {:.6f}, std-dev: {:.6f}.'.format(np.mean(all_logits), np.std(all_logits)))

    print('Min: {:.6f}, max: {:.6f}.'.format(all_logits[0], all_logits[-1]))
    print('Median: {:.6f}.'.format(all_logits[len(all_logits) // 2]))
    print()
    print('10%: {:.6f}, 20%: {:.6f}, 30%: {:.6f}, 40%: {:.6f}, 50%: {:.6f}, 60%: {:.6f}, 70%: {:.6f}, 80%: {:.6f}, 90%: {:.6f}.'.format(
        all_logits[int(len(all_logits) * 0.1)],
        all_logits[int(len(all_logits) * 0.2)],
        all_logits[int(len(all_logits) * 0.3)],
        all_logits[int(len(all_logits) * 0.4)],
        all_logits[int(len(all_logits) * 0.5)],
        all_logits[int(len(all_logits) * 0.6)],
        all_logits[int(len(all_logits) * 0.7)],
        all_logits[int(len(all_logits) * 0.8)],
        all_logits[int(len(all_logits) * 0.9)],
    ))

    print()

    index = np.searchsorted(all_logits, 2.0)
    print('2.0: {:.10f} ({:.8%}), {}/{}.'.format(index / len(all_logits), index / len(all_logits), index, len(all_logits)))

    index = np.searchsorted(all_logits, 2.5)
    print('2.5: {:.10f} ({:.8%}), {}/{}.'.format(index / len(all_logits), index / len(all_logits), index, len(all_logits)))

    index = np.searchsorted(all_logits, 3.0)
    print('3.0: {:.10f} ({:.8%}), {}/{}.'.format(index / len(all_logits), index / len(all_logits), index, len(all_logits)))

    index = np.searchsorted(all_logits, 3.5)
    print('3.5: {:.10f} ({:.8%}), {}/{}.'.format(index / len(all_logits), index / len(all_logits), index, len(all_logits)))

    print()
    print()

    # Show percentiles if provided.
    if args.threshold_percentile is not None:
        print('Threshold: {:.10f}.'.format(all_logits[int(len(all_logits) * args.threshold_percentile)]))

    if args.size_percentile is not None:
        print('Size threshold: {:.10f}.'.format(all_logits[int(len(all_logits) * args.size_percentile)]))


if __name__ == '__main__':
    main()
