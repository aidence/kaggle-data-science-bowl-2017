# Copyright (C) 2017  Aidence B.V.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA

import model

import tensorflow as tf


class JoinDepthAndBatch(model.Node):
    """
    Joins depth and batch dimensions.
    """
    def __init__(self, input, name=None):
        self.input = input
        self.depth_size = None

        super().__init__(name, [input])

    def get_name_prefix(self):
        return 'join-depth-and-batch'

    def get_attributes(self):
        return []

    def build(self):
        # Check.
        if self.input.get_num_dimensions() != 5:
            raise Exception('Input dimensionality must be 5.')

        # Get input and shape.
        input = self.input.get_output()
        shape = self.input.get_dynamic_shape()

        # Set shape size for split.
        self.depth_size = shape[1]

        # Join.
        output = tf.reshape(input, (-1, shape[2], shape[3], shape[4]))

        return output

    def get_depth_size(self):
        return self.depth_size


class SplitDepthAndBatch(model.Node):
    """
    Splits depth and batch dimensions.
    """
    def __init__(self, input, join_depth_and_batch, name=None):
        self.input = input
        self.join_depth_and_batch = join_depth_and_batch

        super().__init__(name, [input])

    def get_name_prefix(self):
        return 'split-depth-and-batch'

    def get_attributes(self):
        return []

    def build(self):
        # Check.
        if self.input.get_num_dimensions() != 4:
            raise Exception('Input dimensionality must be 4.')

        # Get input, depth size and shape.
        input = self.input.get_output()
        shape = self.input.get_dynamic_shape()
        depth_size = self.join_depth_and_batch.get_depth_size()

        # Split.
        output = tf.reshape(input, (-1, depth_size, shape[1], shape[2], shape[3]))

        return output
