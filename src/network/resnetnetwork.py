# Copyright (C) 2017  Aidence B.V.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA

import model
from util.point import Point3D
from .nodes import *


class ResnetNetwork(model.Graph):
    """
    Residual network.
    """
    def add_graph(self, input):
        # Parameters.
        num_filters = self.get_parameter('num-filters', 16)
        activation = self.get_parameter('activation', 'crelu')
        negative_weight = self.get_parameter('negative-weight', 2)
        extra_residual_network = self.get_parameter('extra-residual-network', False)
        activation_before_3d = self.get_parameter('activation-before-3d', False)
        first_activation = self.get_parameter('first-activation', True)

        # Get input.
        output = input

        # Go to 2D view.
        output = join_depth_and_batch = JoinDepthAndBatch(output)

        # First convolution.
        if first_activation:
            output = model.Activation(output, activation)

        output = model.Convolution(output, num_filters, (3, 3))

        # Add resnet block.
        output = ResnetNetwork.add_resnet_block(output, num_filters, activation, dilate=1)

        # Apply 3D convolution.
        output = SplitDepthAndBatch(output, join_depth_and_batch)

        if activation_before_3d:
            output = model.Activation(output, activation)

        output = model.Convolution(output, num_filters, (3, 3, 3))
        output = join_depth_and_batch = JoinDepthAndBatch(output)

        # Add resnet block.
        output = ResnetNetwork.add_resnet_block(output, num_filters, activation, dilate=2)

        # Apply 3D convolution.
        output = SplitDepthAndBatch(output, join_depth_and_batch)

        if activation_before_3d:
            output = model.Activation(output, activation)

        output = model.Convolution(output, num_filters, (3, 3, 3))
        output = join_depth_and_batch = JoinDepthAndBatch(output)

        # Add resnet block.
        output = ResnetNetwork.add_resnet_block(output, num_filters, activation, dilate=3)

        # Apply 3D convolution.
        output = SplitDepthAndBatch(output, join_depth_and_batch)

        if activation_before_3d:
            output = model.Activation(output, activation)

        output = model.Convolution(output, num_filters, (3, 3, 3))
        output = join_depth_and_batch = JoinDepthAndBatch(output)

        # Add resnet block.
        output = ResnetNetwork.add_resnet_block(output, num_filters, activation, dilate=4)

        if extra_residual_network:
            # Add resnet block.
            output = ResnetNetwork.add_resnet_block(output, num_filters, activation, dilate=5)

        # Final convolution.
        output = model.Activation(output, activation)
        output = model.Convolution(output, 1, (3, 3))

        # Split depth and batch again.
        output = SplitDepthAndBatch(output, join_depth_and_batch)

        output = model.Sigmoid(output, negative_weight=negative_weight)

        return output

    @staticmethod
    def add_resnet_block(output, num_filters, activation, filter_size=3, length=3, dilate=1):
        for i in range(length):
            save_output = output

            output = model.Activation(output, activation)
            output = model.Convolution(output, num_filters, (filter_size, filter_size), dilate=dilate)

            output = model.Activation(output, activation)
            output = model.Convolution(output, num_filters, (1, 1))

            gap = (filter_size - 1) * dilate
            before = gap // 2
            after = gap - before

            save_output = model.Crop(save_output, [(before, after), (before, after)])

            output = model.Merge([save_output, output])

        return output

    def get_filter_size(self):
        extra_residual_network = self.get_parameter('extra-residual-network', False)
        global_pool = self.get_parameter('global-pool', True)

        if global_pool:
            return Point3D(7, 128, 128)
        else:
            if extra_residual_network:
                return Point3D(7, 128 - 28 + 1, 128 - 28 + 1)
            else:
                return Point3D(7, 128 - 58 + 1, 128 - 58 + 1)
