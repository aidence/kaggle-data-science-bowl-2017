# Copyright (C) 2017  Aidence B.V.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA

from util.util import Util

import multiprocessing

import argparse
import os
import datetime
import shutil

import numpy as np
import yaml


def worker_process_main(gpu_index, id_queue, result_queue, config):
    # Imports.
    import inference
    from util.point import Point3D

    import pickle
    import time
    import queue
    import traceback

    # Get large.
    large = config.get('large', False)

    # Load data set.
    data_set = Util.load_data_set(config['data-set'])

    # Load model.
    the_model = Util.load_model(config['model'], use_gpus=[gpu_index + 1],
                                input_shape=(None, None, None, None, 1), output_shape=(None, None, None, None, 1))

    # Create evaluator.
    evaluator = inference.Evaluator(the_model)

    while True:
        # Try to get an id.
        try:
            id = id_queue.get_nowait()
        except queue.Empty:
            break

        try:
            # Get start time.
            start = time.time()

            # Get scan info.
            scan = data_set.get_scan(id)

            volume = scan.get_volume()

            # Prepare volume for evaluation.
            volume = volume.reshape(volume.shape + (1,))
            volume = (volume - np.float32(127.5)) * np.float32(1 / 127.5)

            # Downsample volume in large mode.
            if large:
                volume = volume[::2, ::4, ::4]

            # Get capture name and mode.
            capture_name = config['model'].get('output-layer-name', 'sigmoid-1') + '/logits'

            mode = config.get('mode', 'mask')

            # Use volume mode for large mode.
            if large and mode == 'mask':
                mode = 'volume'

            # Evaluate.
            the_data = {
                'type': 'logits',
                'data-set': config['data-set'],
                'id': id,
                'mode': mode,
                'large': large,
            }

            if mode == 'mask':
                # Evaluate inside mask.
                mask_start = scan.get_mask().get_start()
                mask_size = scan.get_mask().get_size()

                outputs = evaluator.evaluate(volume, mask_start, mask_size, [capture_name])

                logits = outputs[capture_name]

                the_data['logits'] = logits
                the_data['start'] = mask_start
            elif mode == 'volume':
                # Evaluate whole volume.
                outputs = evaluator.evaluate(volume, Point3D(0, 0, 0), Point3D(volume.shape[0], volume.shape[1], volume.shape[2]), [capture_name])

                logits = outputs[capture_name]

                the_data['logits'] = logits
                the_data['start'] = Point3D(0, 0, 0)
            elif mode == 'nodules':
                # Evaluate all nodules. Set rest to nan.
                logits = np.full(volume.shape, np.nan, dtype=np.float32)

                min_start_index = volume.shape[:-1]
                max_end_index = np.zeros(3, dtype=np.int)
                for nodule in scan.get_nodules():
                    # Get nodule dimensions.
                    cz, cy, cx = nodule.get_center(in_world_space=False).get_as_zyx_tuple()
                    depth, height, width = nodule.get_size(in_world_space=False).get_as_zyx_tuple()

                    # Scale in large mode.
                    if large:
                        cz *= 0.5
                        cy *= 0.25
                        cx *= 0.25

                        depth *= 0.5
                        height *= 0.25
                        width *= 0.25

                    # Take 3 times as much context.
                    start_index = Point3D(
                        int(round(cz - depth * 1.5 + 0.5)),
                        int(round(cy - height * 1.5 + 0.5)),
                        int(round(cx - width * 1.5 + 0.5))
                    )
                    end_index = Point3D(
                        start_index.z + int(round(3 * depth)),
                        start_index.y + int(round(3 * height)),
                        start_index.x + int(round(3 * width))
                    )

                    start_index = np.maximum(start_index.get_as_zyx_array(), 0)
                    end_index = np.minimum(end_index.get_as_zyx_array(), volume.shape[:-1])

                    min_start_index = np.minimum(min_start_index, start_index)
                    max_end_index = np.maximum(max_end_index, end_index)

                    start_index = Point3D.from_zyx_list(start_index)
                    end_index = Point3D.from_zyx_list(end_index)

                    size = Point3D.from_zyx_list(end_index.get_as_zyx_array() - start_index.get_as_zyx_array())

                    # Evaluate nodule.
                    outputs = evaluator.evaluate(volume, start_index, size, [capture_name])

                    # Set output.
                    logits[start_index.z:end_index.z, start_index.y:end_index.y, start_index.x:end_index.x, :] = outputs[capture_name]

                # Crop logits and set start accordingly.
                logits = logits[min_start_index[0]:max_end_index[0],min_start_index[1]:max_end_index[1],min_start_index[2]:max_end_index[2]]

                the_data['logits'] = logits
                the_data['start'] = Point3D(min_start_index[0], min_start_index[1], min_start_index[2])
            else:
                raise Exception('Unknown mode.')
        except KeyboardInterrupt:
            raise
        except:
            traceback.print_exc()

            print('Processing id {} failed.'.format(id))
            continue

        # Write it.
        file_name = os.path.join(config['output-directory'], 'logits-{}.pkl'.format(id))

        with open(file_name, 'wb') as f:
            pickle.dump(the_data, f)

        # Show time.
        result_queue.put('Evaluation of scan took {:.3f}s.'.format(time.time() - start))


def accumulator_process_main(result_queue, num_entries):
    num_results = 0
    while True:
        # Get an entry.
        result = result_queue.get()

        # Check whether we are done.
        if result is None:
            break

        num_results += 1

        # Show progress.
        print('{}/{} ({:.1%}). {}'.format(num_results, num_entries, num_results / num_entries, result))


def main():
    # Parse command line options.
    parser = argparse.ArgumentParser(description='Write model output logits of scans.',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('config_file_name', metavar='config-file-name', type=str, help='Configuration file name.')
    parser.add_argument('output_directory', metavar='output-directory', type=str, help='Directory to store logits.')
    parser.add_argument('--gpus', metavar='gpus', type=str, help='GPU numbers to use (comma separated).')

    args = parser.parse_args()

    # Load config yaml.
    with open(args.config_file_name, 'r') as f:
        config = yaml.load(f)

    config['output-directory'] = args.output_directory

    # Create directory.
    os.makedirs(args.output_directory, exist_ok=True)

    # Copy config.
    date = 'UTC-{:%Y-%m-%d@%H:%M:%S}'.format(datetime.datetime.utcnow())

    config_file_name = os.path.join(args.output_directory, 'config-{}.yml'.format(date))

    shutil.copy(args.config_file_name, config_file_name)

    # Get scans.
    if config.get('mode', 'mask') == 'nodules':
        config['negative-scans'] = 0

    scan_ids, num_positive_scans, num_negative_scans, num_nodules = Util.get_scan_ids(config)

    print('Found {} matching scans ({} positive, {} negative) with {} nodules.'.format(
        len(scan_ids), num_positive_scans, num_negative_scans, num_nodules))

    # Remove scans that already exists.
    scan_ids = [id for id in scan_ids
                if not os.path.isfile(os.path.join(config['output-directory'], 'logits-{}.pkl'.format(id)))]

    # Distribute it.
    Util.distribute_gpu_work(scan_ids, worker_process_main, accumulator_process_main,
                             gpus=args.gpus,
                             worker_extra_args=(config,),
                             accumulator_extra_args=())


if __name__ == '__main__':
    main()
