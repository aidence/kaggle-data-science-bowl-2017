# Copyright (C) 2017  Aidence B.V.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA

import argparse

import numpy as np
import yaml

from util.util import Util


def worker_process_main(entry_queue, patch_queue, patch_size, patches_per_scan, seed, large):
    import queue
    import traceback

    import inference
    from util.point import Point3D

    # Create random state.
    random_state = np.random.RandomState(seed)

    # Process ids.
    data_sets = {}
    while True:
        # Try to get an id.
        try:
            data_set_name, scan_id, is_positive = entry_queue.get_nowait()
        except queue.Empty:
            break

        try:
            # Get data-set.
            if data_set_name in data_sets:
                data_set = data_sets[data_set_name]
            else:
                data_set = Util.load_data_set(data_set_name)

                data_sets[data_set_name] = data_set

            # Get volume and mask.
            scan = data_set.get_scan(scan_id)

            volume = scan.get_volume()

            # Downsample volume in large mode.
            if large:
                volume = volume[::2, ::4, ::4]

            if is_positive:
                # Positive scan.
                positives = []
                dimensions = []
                for nodule in scan.get_included_nodules():
                    # Skip small nodules in large mode.
                    if large and nodule.get_radius() < 10:
                        continue

                    center = nodule.get_center(in_world_space=False)
                    size = nodule.get_size(in_world_space=False)

                    if large:
                        dimension = (int(round(size.z * 0.5)), int(round(size.y * 0.25)), int(round(size.x * 0.25)))
                    else:
                        dimension = (int(round(size.z)), int(round(size.y)), int(round(size.x)))

                    for z in range(nodule.get_start_index().z, nodule.get_end_index().z):
                        if large:
                            # Skip uneven z slices.
                            if z & 1 == 1:
                                continue

                            # Scale according to subsampling.
                            patch_center = Point3D(z // 2, center.y * 0.25, center.x * 0.25)
                        else:
                            patch_center = Point3D(z, center.y, center.x)

                        patch = inference.Inferrer.get_patch(volume, patch_center, patch_size)

                        positives.append(patch)
                        dimensions.append(dimension)

                # Put them in queue.
                patch_queue.put(dict(positives=positives, dimensions=dimensions))
            else:
                # Use full unmasked volume in large mode.
                if large:
                    mask = np.ones(volume.shape, dtype=np.bool)
                else:
                    # Get mask.
                    mask = scan.get_mask().get_mask()

                # Remove nodules from mask.
                for nodule in scan.get_nodules():
                    # Get nodule dimensions.
                    cz, cy, cx = nodule.get_center(in_world_space=False).get_as_zyx_tuple()
                    depth, height, width = nodule.get_size(in_world_space=False).get_as_zyx_tuple()

                    # Scale in large mode.
                    if large:
                        cz *= 0.5
                        cy *= 0.25
                        cx *= 0.25

                        depth *= 0.5
                        height *= 0.25
                        width *= 0.25

                    # Take 3 times as much context.
                    start_index = np.array([
                        int(round(cz - depth * 1.5 + 0.5)),
                        int(round(cy - height * 1.5 + 0.5)),
                        int(round(cx - width * 1.5 + 0.5))
                    ])
                    end_index = np.array([
                        start_index[0] + int(round(3 * depth)),
                        start_index[1] + int(round(3 * height)),
                        start_index[2] + int(round(3 * width))
                    ])

                    start_index = np.maximum(start_index, 0)
                    end_index = np.minimum(end_index, mask.shape)

                    # Remove nodule from mask.
                    mask[start_index[0]:end_index[0], start_index[1]:end_index[1], start_index[2]:end_index[2]] = False

                # Negative scan. Select random patches.
                z_indices, y_indices, x_indices = np.nonzero(mask)

                index_indices = random_state.choice(len(z_indices), size=patches_per_scan, replace=False)

                z_indices = z_indices[index_indices]
                y_indices = y_indices[index_indices]
                x_indices = x_indices[index_indices]

                negatives = []
                weights = []
                for z, y, x in zip(z_indices, y_indices, x_indices):
                    patch = inference.Inferrer.get_patch(volume, Point3D(z, y, x), patch_size)

                    negatives.append(patch)
                    weights.append(1)

                # Put them in queue.
                patch_queue.put(dict(negatives=negatives, weights=weights))
        except KeyboardInterrupt:
            raise
        except:
            traceback.print_exc()

            print('Processing id {} failed.'.format(scan_id))
            continue


def accumulator_process_main(patch_queue, num_entries, output_file_name, test_fraction, seed):
    # Accumulate.
    positives = []
    dimensions = []
    negatives = []
    weights = []

    num_seen = 0
    num_positives = 0
    num_negatives = 0

    while True:
        # Get an entry.
        entry = patch_queue.get()

        # Check whether we are done.
        if entry is None:
            break

        if 'positives' in entry:
            positives.append(entry['positives'])
            dimensions.append(entry['dimensions'])

            num_positives += len(entry['positives'])
        else:
            negatives.append(entry['negatives'])
            weights.append(entry['weights'])

            num_negatives += len(entry['negatives'])

        num_seen += 1

        if num_seen % 10 == 0:
            print('{}/{} ({:.1%}), positives: {}, negatives: {}.'.format(
                num_seen,
                num_entries,
                num_seen / num_entries,
                num_positives,
                num_negatives))

    if num_seen % 10 != 0:
        print('{}/{} ({:.1%}), positives: {}, negatives: {}.'.format(
            num_seen,
            num_entries,
            num_seen / num_entries,
            num_positives,
            num_negatives))

    # Shuffle them.
    random_state = np.random.RandomState(seed)

    positive_indices = random_state.permutation(len(positives))
    negative_indices = random_state.permutation(len(negatives))

    positives = [positives[i] for i in positive_indices]
    dimensions = [dimensions[i] for i in positive_indices]

    negatives = [negatives[i] for i in negative_indices]
    weights = [weights[i] for i in negative_indices]

    # Get arrays.
    test_positive_offset = int(len(positives) * test_fraction)
    test_negative_offset = int(len(negatives) * test_fraction)

    # Create arrays.
    train_positives = positives[test_positive_offset:]
    train_negatives = negatives[test_negative_offset:]
    train_dimensions = dimensions[test_positive_offset:]
    train_weights = weights[test_negative_offset:]

    test_positives = positives[0:test_positive_offset]
    test_negatives = negatives[0:test_negative_offset]
    test_dimensions = dimensions[0:test_positive_offset]
    test_weights = weights[0:test_negative_offset]

    # Sum and create numpy arrays.
    train_positives = np.array(sum(train_positives, []))
    train_negatives = np.array(sum(train_negatives, []))
    train_dimensions = np.array(sum(train_dimensions, []), dtype=np.int32)
    train_weights = np.array(sum(train_weights, []), dtype=np.float32)

    test_positives = np.array(sum(test_positives, []))
    test_negatives = np.array(sum(test_negatives, []))
    test_dimensions = np.array(sum(test_dimensions, []), dtype=np.int32)
    test_weights = np.array(sum(test_weights, []), dtype=np.float32)

    # Write patches.
    data = {
        'test-positive-inputs': test_positives,
        'test-positive-dimensions': test_dimensions,
        'test-negative-inputs': test_negatives,
        'test-negative-weights': test_weights,
        'train-positive-inputs': train_positives,
        'train-positive-dimensions': train_dimensions,
        'train-negative-inputs': train_negatives,
        'train-negative-weights': train_weights,
    }

    np.savez_compressed(output_file_name, **data)


def main():
    # Parse command line options.
    parser = argparse.ArgumentParser(description='Write patches from candidates.',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('config_file_name', metavar='config-file-name', type=str, help='Configuration file name.')
    parser.add_argument('--output-file-name', metavar='output-file-name', type=str, default='random-patches.npz',
                        help='Output file name.')
    parser.add_argument('--num-processes', metavar='num-processes', type=int, help='Number of processes to use.')

    args = parser.parse_args()

    # Load config yaml.
    with open(args.config_file_name, 'r') as f:
        config = yaml.load(f)

    # Get config.
    large = config.get('large', False)
    seed = config.get('seed', 42)
    test_fraction = config.get('test-fraction', 0.2)
    patches_per_scan = config.get('patches-per-scan', 100)
    num_negatives = config.get('negatives', 30000)

    # Get size.
    size = Util.parse_3d_point(config['size'])

    print('Using patch size: {}x{}x{}.'.format(size.z, size.y, size.x))

    # Get scan ids.
    positive_scan_ids = []
    negative_scan_ids = []
    negative_from_positive_scan_ids = []
    num_nodules = 0

    for data_set_config in config['data-sets']:
        # Get data-set.
        name = data_set_config['name']
        data_set = Util.load_data_set(name)

        # Add ids.
        if data_set_config.get('positives', True):
            ids = [(name, id, True) for id in data_set.get_positive_scan_ids()]

            positive_scan_ids += ids

            num_nodules += sum([len(data_set.get_scan(id).get_included_nodules()) for _, id, _ in ids])

        if data_set_config.get('negatives', True):
            negative_scan_ids += [(name, id, False) for id in data_set.get_negative_scan_ids()]

        if data_set_config.get('negatives-from-positives', False):
            negative_from_positive_scan_ids += [(name, id, False) for id in data_set.get_positive_scan_ids()]

    # Shuffle scans.
    random_state = np.random.RandomState(seed)

    random_state.shuffle(positive_scan_ids)
    random_state.shuffle(negative_scan_ids)
    random_state.shuffle(negative_from_positive_scan_ids)

    # Determine number of negative scans.
    negative_scan_num = int(np.ceil(num_negatives / patches_per_scan))

    # Select scans.
    if negative_scan_num > len(negative_scan_ids):
        # Select from positive scans as well.
        difference = negative_scan_num - len(negative_scan_ids)

        negative_scan_ids += negative_from_positive_scan_ids[0:difference]
    else:
        negative_scan_ids = negative_scan_ids[0:negative_scan_num]

    # Get scan ids.
    scan_ids = positive_scan_ids + negative_scan_ids

    print('Using {} scans ({} positive, {} negative) with {} nodules.'.format(
        len(scan_ids), len(positive_scan_ids), len(negative_scan_ids), num_nodules))

    # Distribute it.
    Util.distribute_cpu_work(scan_ids, worker_process_main, accumulator_process_main,
                             num_processes=args.num_processes,
                             worker_extra_args=(size, patches_per_scan, seed, large),
                             accumulator_extra_args=(args.output_file_name, test_fraction, seed))


if __name__ == '__main__':
    main()
