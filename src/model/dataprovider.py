# Copyright (C) 2017  Aidence B.V.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA

class DataProvider:
    """
    Provides input and output data.
    """

    def get_mini_batch_input_shape(self):
        """
        Returns the mini-batch input shape of this data provider.
        """

        raise NotImplementedError()

    def get_mini_batch_output_shape(self):
        """
        Returns the mini-batch output shape of this data provider.
        Do not implement if data provider does not have outputs.
        """

        raise NotImplementedError()

    def get_mini_batch(self, size):
        """
        Returns a mini-batch of given size. Returns a tuple (inputs, outputs, weights).

        :param size: Size of mini-batch.
        :return:
        """

        raise NotImplementedError()

    def get_everything(self, size):
        """
        Yields all data in mini-batches of given size. Yields (inputs, outputs, weights) tuples.

        :param size: Maximum size of yielded mini-batches.
        :return:
        """

        raise NotImplementedError()
