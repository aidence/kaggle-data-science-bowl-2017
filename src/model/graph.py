# Copyright (C) 2017  Aidence B.V.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA

import numpy as np

from .model import Model
from .nodes import *

import re
import logging
import collections


class Graph(Model):
    """
    Graph model.
    """

    #
    # Public methods.
    #

    def __init__(self, context, input_shape, output_shape, parameters={}):
        # Check shapes.
        if len(input_shape) not in (2, 4, 5):
            raise Exception('Input mini-batches dimensionality must be 2 (batch-size, features), ' +
                            '4 (batch-size, height, width, channels) or ' +
                            '5 (batch-size, depth, height, width, channels).')

        if len(output_shape) not in (2, 4, 5):
            raise Exception('Output mini-batches dimensionality must be 2 (batch-size, classes), ' +
                            '4 (batch-size, height, width, classes) or ' +
                            '5 (batch-size, depth, height, width, classes).')

        # Set members.
        self.used_names = None
        self.main_used_names = None
        self.num_labels = None
        self.input_node = None
        self.output_node = None
        self.nodes = collections.defaultdict(list)
        self.sorted_nodes = None

        # Initialize.
        super().__init__(context, input_shape, output_shape, parameters)

    def get_output_node(self):
        return self.output_node

    # Initializes with all-you-need-is-good-init initialization by Dmytro Mishkin et al.
    # See http://arxiv.org/pdf/1511.06422v7.pdf
    def init_with_data_provider(self, data_provider, verbose=True):
        if verbose:
            logging.info('Initializing model.')

        # Get parameters.
        use_first_output = self.get_parameter('init-use-first-output', False)
        max_iterations = self.get_parameter('init-max-iterations', 3)
        batch_size = self.get_parameter('init-batch-size', 16)
        wanted_std = self.get_parameter('init-wanted-std-dev', 1.0)
        tolerance = self.get_parameter('init-tolerance', 0.05)

        # Walk through nodes.
        for node in self.sorted_nodes:
            # Check for weights.
            if not node.has_weights():
                continue

            if isinstance(node, Output):
                # TODO: As outputs are likely to be probabilities we do not want use them.
                # TODO: Maybe get logits instead of output?
                continue

            # Default to outputs of node itself.
            output_node = node

            if not use_first_output:
                # Get first output that cannot be changed by other weights.
                children = node.get_children()
                if len(children) == 1:
                    current_node = children[0]

                    while True:
                        if current_node.has_weights() or isinstance(current_node, Output):
                            # Node has weights of its own or node is an output node, stop.
                            break

                        parents = current_node.get_parents()
                        if len(parents) > 1:
                            # Node has more than one parent, stop.
                            break

                        # Use node.
                        output_node = current_node

                        # Get children, and check for splits.
                        children = current_node.get_children()

                        if len(children) != 1:
                            # Either node splits here, or we have reached an end (loss or output).
                            break

                        current_node = children[0]

            if verbose:
                logging.info('Setting gain of {} using output of {}.'.format(node.get_name(), output_node.get_name()))

            # Get outputs of nodes in all towers.
            node_outputs = [node.get_output() for node in self.nodes[output_node.get_name()]]

            # Get variables.
            weight_variables = node.get_weights()
            gamma_variables = node.get_gammas()

            # Set gain to make output have wanted standard deviation.
            output_std = None
            for i in range(max_iterations):
                # Get mini-batch.
                inputs, outputs, weights = data_provider.get_mini_batch(batch_size)

                # Get outputs and variance.
                feed_dict = {self.inputs: inputs, self.output_weights: weights, self.training: True}
                output_values = self.session.run(node_outputs, feed_dict=feed_dict)

                # Concatenate outputs and take mean over everything except batch.
                output_values = np.concatenate(output_values, axis=0)
                output_values = output_values.reshape((batch_size, -1))
                output_values = np.mean(output_values, axis=1)

                # Take mean of weights over everything except batch.
                weights = np.mean(weights, axis=tuple(range(1, weights.ndim)))

                # Calculate weighted statistics.
                output_mean = np.average(output_values, weights=weights)
                output_var = np.average((output_values - output_mean) ** 2, weights=weights)
                output_std = np.sqrt(output_var)

                if np.abs(output_std - wanted_std) < tolerance:
                    break

                # Assign new scaled weights. Use gamma when available.
                scale = wanted_std / output_std

                for weight_var, gamma_var in zip(weight_variables, gamma_variables):
                    if gamma_var is not None:
                        self.session.run(gamma_var.assign(gamma_var * scale))
                    else:
                        self.session.run(weight_var.assign(weight_var * scale))

            if verbose:
                logging.info('Final standard deviation of {} is {}.'.format(node.get_name(), output_std))

    #
    # Protected methods that can/should be implemented by subclass.
    #

    def add_graph(self, input):
        raise NotImplementedError()

    #
    # Private methods.
    #

    def build_model(self, tower_num, inputs, true_outputs, output_weights, input_shape, output_shape):
        # Check for main tower.
        if self.in_main_tower:
            # Keep track of used names.
            self.used_names = set()

            # Get number of labels.
            self.num_labels = output_shape[-1]

            # Create input node.
            self.input_node = Input(self, inputs)

            # Add nodes.
            self.output_node = self.add_graph(self.input_node)

            # Process graph.
            self.process_graph(self.input_node, self.output_node)

            # Set used names in main tower to check others.
            self.main_used_names = self.used_names

            # Get output, loss and statistics.
            output = self.output_node.get_output()
            loss = self.output_node.get_loss(true_outputs, output_weights)
            statistics = self.output_node.get_statistics(true_outputs, output_weights)

            return output, loss, statistics
        else:
            # Other towers.
            self.used_names = set()

            input_node = Input(self, inputs)

            output_node = self.add_graph(input_node)

            # Process graph.
            self.process_graph(input_node, output_node)

            # Get output, loss and statistics.
            output = output_node.get_output()
            loss = output_node.get_loss(true_outputs, output_weights)
            statistics = output_node.get_statistics(true_outputs, output_weights)

            return output, loss, statistics

    def merge_statistics(self, statistics, batch_size, output_weight_sum):
        merged_statistics = collections.OrderedDict()
        for name, values in statistics.items():
            merged_value = self.output_node.merge_statistic(name, values, batch_size, output_weight_sum, statistics)

            if merged_value is not None:
                merged_statistics[name] = merged_value

        return merged_statistics

    def get_num_labels(self):
        return self.num_labels

    def use_name(self, name, prefix):
        if name is None:
            prefix += '-'
            length = len(prefix)

            postfixes = (used_name[length:] for used_name in self.used_names if used_name.startswith(prefix))
            postfixes = [0] + [int(postfix) for postfix in postfixes if postfix.isdigit()]

            name = prefix + str(max(postfixes) + 1)
        elif name in self.used_names:
            raise Exception('Node name \'{}\' has already been used.'.format(name))

        if not self.in_main_tower and name not in self.main_used_names:
            raise Exception('Adding extra node named \'{}\', but not in main tower.'.format(name))

        self.used_names.add(name)

        return name

    def process_graph(self, input_node, output_node):
        # Check last node.
        if not isinstance(output_node, Output):
            raise Exception('Output of graph must be an output node.')

        # Check for unused parts and get topological sorted nodes.
        # Do a non-recursive depth-first search to get topological ordering.
        stack = [('visit', input_node)]
        nodes = []
        seen_nodes = {input_node}

        while stack:
            action, node = stack.pop()

            if action == 'append':
                nodes.append(node)
            else:
                children = node.get_children()

                if not children and node != output_node:
                    # TODO: May be used in some kind of loss.

                    raise Exception('Node \'{}\' is unused.'.format(node.get_name()))

                stack.append(('append', node))

                for child in reversed(children):
                    if child not in seen_nodes:
                        stack.append(('visit', child))
                        seen_nodes.add(node)

        nodes.reverse()

        # Store if in main tower.
        if self.in_main_tower:
            self.sorted_nodes = nodes

        # Store nodes of all towers by name.
        for node in nodes:
            self.nodes[node.get_name()].append(node)
