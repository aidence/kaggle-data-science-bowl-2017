# Copyright (C) 2017  Aidence B.V.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA

import tensorflow as tf

import os
import logging

from .adamaxoptimizer import AdamaxOptimizer
from .datafetcher import DataFetcher


class Trainer:
    """
    Model trainer.
    """

    def __init__(self, model, train_data_provider, test_data_provider=None, summaries_dir=None, models_dir=None,
                 parameters={}):
        # Store members.
        self.model = model
        self.context = model.get_context()
        self.graph = self.context.get_graph()
        self.session = self.context.get_session()
        self.train_data_provider = train_data_provider
        self.test_data_provider = test_data_provider
        self.summaries_dir = summaries_dir
        self.models_dir = models_dir
        self.parameters = parameters

        self.inputs = model.get_inputs()
        self.outputs = model.get_outputs()
        self.output_weights = model.get_output_weigths()
        self.training = model.get_training()
        self.summary = model.get_summary()

        # Check shapes.
        input_shape = tuple(train_data_provider.get_mini_batch_input_shape())
        output_shape = tuple(train_data_provider.get_mini_batch_output_shape())

        if input_shape != tuple(model.get_input_shape()):
            raise Exception('Input of model and train data providers do not match.')

        if output_shape != tuple(model.get_output_shape()):
            raise Exception('Output of model and train data providers do not match.')

        if self.test_data_provider is not None and \
                input_shape != tuple(self.test_data_provider.get_mini_batch_input_shape()):
            raise Exception('Input of train and test data providers do not match.')

        if self.test_data_provider is not None and \
                output_shape != tuple(self.test_data_provider.get_mini_batch_output_shape()):
            raise Exception('Output of train and test data providers do not match.')

        # Create output directory.
        if summaries_dir is not None:
            os.makedirs(summaries_dir, exist_ok=True)

        if models_dir is not None:
            os.makedirs(models_dir, exist_ok=True)

        # Add trainer operation.
        with self.graph.as_default():
            # Extra trainer.
            with tf.variable_scope('trainer'):
                with tf.name_scope('trainer'):
                    optimizer = parameters.get('optimizer', 'adam')

                    if optimizer == 'sgd':
                        optimizer = tf.train.GradientDescentOptimizer(parameters['learning-rate'])
                    elif optimizer == 'momentum':
                        optimizer = tf.train.MomentumOptimizer(parameters.get('learning-rate', 0.01),
                                                               parameters.get('momentum', 0.9))
                    elif optimizer == 'nesterov':
                        optimizer = tf.train.MomentumOptimizer(parameters.get('learning-rate', 0.01),
                                                               parameters.get('momentum', 0.9), use_nesterov=True)
                    elif optimizer == 'adagrad':
                        optimizer = tf.train.AdagradOptimizer(parameters['learning-rate'])
                    elif optimizer == 'adadelta':
                        optimizer = tf.train.AdadeltaOptimizer(parameters.get('learning-rate', 1e-4),
                                                               parameters.get('rho', 0.95))
                    elif optimizer == 'adamax':
                        optimizer = AdamaxOptimizer(parameters.get('learning-rate', 1e-4))
                    elif optimizer == 'rmsprop':
                        optimizer = tf.train.RMSPropOptimizer(parameters['learning-rate'],
                                                              parameters.get('decay', 0.95))
                    elif optimizer == 'adam':
                        optimizer = tf.train.AdamOptimizer(parameters.get('learning-rate', 1e-4))
                    else:
                        raise Exception('Unknown optimizer.')

                    gradients_and_variables = model.get_gradients_and_variables()

                    self.training_step = optimizer.apply_gradients(gradients_and_variables)

            # Extra initialization.
            with tf.name_scope('trainer-initialization'):
                trainer_variables = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope='trainer')

                self.init = tf.variables_initializer(trainer_variables)

        # Create writer.
        if self.summary is None:
            self.summaries_dir = None

        if self.summaries_dir is not None:
            self.train_summary_writer = tf.summary.FileWriter(self.summaries_dir + '/train', self.graph, flush_secs=10)
            self.test_summary_writer = tf.summary.FileWriter(self.summaries_dir + '/test', flush_secs=10)
        else:
            self.summary_writer = None

        # Run extra init.
        self.session.run(self.init)

    def train(self, parameters={}):
        # Merge parameters.
        all_parameters = self.parameters.copy()
        all_parameters.update(parameters)

        # Get parameters.
        num_steps = all_parameters.get('num-steps', 1000000)
        train_batch_size = all_parameters.get('train-batch-size', 100)
        test_batch_size = all_parameters.get('test-batch-size', 200)
        queue_size = all_parameters.get('queue-size', 100)
        test_interval = all_parameters.get('test-interval', 100)
        save_interval = all_parameters.get('save-interval', 100)
        summary_interval = all_parameters.get('summary-interval', 10)
        log_interval = all_parameters.get('log-interval', 10)
        log_captures = all_parameters.get('log-captures', ['data-loss'])
        asynchronous = all_parameters.get('asynchronous-data-fetching', True)

        step_start = self.model.get_metadata('training-step', 0) + 1

        # Create fetcher.
        if self.test_data_provider is None:
            data_fetcher = DataFetcher(
                providers=dict(train=self.train_data_provider),
                batch_sizes=dict(train=train_batch_size),
                frequencies=dict(train=1),
                queue_size=queue_size,
                asynchronous=asynchronous
            )
        else:
            data_fetcher = DataFetcher(
                providers=dict(train=self.train_data_provider, test=self.test_data_provider),
                batch_sizes=dict(train=train_batch_size, test=test_batch_size),
                frequencies=dict(train=test_interval, test=1),
                queue_size=queue_size,
                asynchronous=asynchronous
            )

        try:
            # Get captures.
            capture_names = log_captures
            capture_variables = [self.model.get_capture(name) for name in capture_names]

            for step in range(step_start, step_start + num_steps):
                # Train.
                inputs, outputs, weights = data_fetcher.get_mini_batch('train')

                feed_dict = {self.inputs: inputs, self.outputs: outputs,
                             self.output_weights: weights, self.training: True}

                fetches = [self.training_step] + capture_variables

                if self.summaries_dir is not None and (step % summary_interval) == 0:
                    fetches.append(self.summary)

                    _, *capture_values, summary = self.session.run(fetches, feed_dict=feed_dict)

                    self.train_summary_writer.add_summary(summary, global_step=step)
                else:
                    _, *capture_values = self.session.run(fetches, feed_dict=feed_dict)

                # Add captures.
                log_parts = []
                if (step % log_interval) == 0:
                    for name, value in zip(capture_names, capture_values):
                        log_parts.append('{}: {:.4f}'.format(name, value))

                # Enforce constraints.
                self.model.enforce_constraints()

                # Set new step.
                self.model.set_metadata('training-step', step)

                # Test.
                if (step % test_interval) == 0 and self.test_data_provider is not None:
                    inputs, outputs, weights = data_fetcher.get_mini_batch('test')

                    feed_dict = {self.inputs: inputs, self.outputs: outputs, self.output_weights: weights}

                    fetches = capture_variables.copy()

                    if self.summaries_dir is not None:
                        fetches.append(self.summary)

                        *capture_values, summary = self.session.run(fetches, feed_dict=feed_dict)

                        self.test_summary_writer.add_summary(summary, global_step=step)
                    else:
                        capture_values = self.session.run(fetches, feed_dict=feed_dict)

                    # Add captures.
                    for name, value in zip(capture_names, capture_values):
                        log_parts.append('test-{}: {:.4f}'.format(name, value))

                # Log.
                if log_parts:
                    logging.info('Step {}, {}.'.format(step, ', '.join(log_parts)))

                # Save.
                if self.models_dir is not None and (step % save_interval) == 0:
                    self.model.save(self.models_dir, step)
        finally:
            # Stop fetcher.
            data_fetcher.stop()
