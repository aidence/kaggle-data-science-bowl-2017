# Copyright (C) 2017  Aidence B.V.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA

import numpy as np

import tensorflow as tf


class Node:
    """
    A graph node.
    """
    def __init__(self, name, parents, graph=None):
        # Determine graph.
        if graph is None:
            if not parents:
                raise Exception('Could not determine graph.')

            graph = parents[0].get_graph()

        # Set members.
        self.graph = graph
        self.parents = parents
        self.children = []
        self.weights = []
        self.gammas = []
        self.biases = []

        # Use name.
        self.name = self.graph.use_name(name, self.get_name_prefix())

        # Build node.
        with tf.name_scope(self.name):
            self.output = self.build()

        # Determine shape.
        self.shape = self.output.get_shape().as_list()

        # Check it.
        if len(self.shape) not in (2, 4, 5):
            raise Exception('Node output shape dimensionality must be 2, 4 or 5.')

        if self.shape[-1] is None:
            raise Exception('Node output channels must be defined.')

        # Add children to parents.
        for parent in self.parents:
            parent.add_child(self)

        # Add capture.
        self.graph.add_capture(self.name + '/output', self.output)

    def get_name(self):
        return self.name

    def get_graph(self):
        return self.graph

    def get_parents(self):
        return self.parents

    def get_children(self):
        return self.children

    def get_shape(self):
        return self.shape

    def get_dynamic_shape(self):
        """
        Mixes shape with dynamic shape.
        """
        if None not in self.shape:
            return self.shape

        dynamic_shape = tf.shape(self.output)

        return [dynamic_shape[i] if dim_size is None else dim_size for i, dim_size in enumerate(self.shape)]

    def get_num_dimensions(self):
        return len(self.shape)

    def get_num_channels(self):
        return self.shape[-1]

    def get_output(self):
        return self.output

    def has_weights(self):
        return bool(self.weights)

    def get_weights(self):
        return self.weights

    def get_gammas(self):
        return self.gammas

    def has_biases(self):
        return bool(self.biases)

    def get_biases(self):
        return self.biases

    def add_child(self, child):
        self.children.append(child)

    def create_weigths(self, shape, num_inputs, sub_name=None):
        if sub_name is not None:
            name = self.name + '/' + sub_name
        else:
            name = self.name

        var, gamma, value = self.graph.create_weights(name, shape, num_inputs)

        self.weights.append(var)
        self.gammas.append(gamma)

        return value

    def create_biases(self, shape, sub_name=None):
        if sub_name is not None:
            name = self.name + '/' + sub_name
        else:
            name = self.name

        var, value = self.graph.create_biases(name, shape)

        self.biases.append(var)

        return value

    def get_name_prefix(self):
        """
        Returns unique prefix of node for naming.
        Subclass should implement this method.

        :return:
        """
        raise NotImplementedError()

    def get_attributes(self):
        """
        Returns list of key-value pairs of attributes of node.
        Subclass should implement this method.

        :return:
        """
        return []

    def build(self):
        """
        Subclass should build node and return output.

        :return:
        """
        raise NotImplementedError()


class Input(Node):
    """
    The input node.
    """
    def __init__(self, graph, inputs):
        self.inputs = inputs

        super().__init__('input', [], graph)

    def get_name_prefix(self):
        return 'input'

    def build(self):
        return self.inputs


class Output(Node):
    """
    An output node.
    """
    def get_statistics(self, true_outputs, output_weights):
        """
        Returns a list of (name, tensor) statistic tuples of output node.
        """
        raise NotImplementedError()

    def merge_statistic(self, name, values, batch_size, output_weight_sum, all_statistics):
        """
        Merges statistic from multiple output nodes.
        """
        raise NotImplementedError()

    def get_loss(self, true_outputs, output_weights):
        raise NotImplementedError()


class Sigmoid(Output):
    """
    Implements a sigmoid node.
    """
    def __init__(self, input, dense=False, negative_weight=None, name=None):
        self.input = input
        self.dense = dense
        self.negative_weight = negative_weight
        self.logits = None
        self.labels = None

        super().__init__(name, [input])

    def get_name_prefix(self):
        return 'sigmoid'

    def get_attributes(self):
        return [('dense', self.dense), ('negative-weight', self.negative_weight)]

    def build(self):
        # Check.
        if self.dense:
            if self.input.get_num_dimensions() != 2:
                raise Exception('Input dimensionality should be 2 for dense sigmoid.')

        if self.graph.get_num_labels() != 1:
            raise Exception('Number of output labels must be 1.')

        # Add a dense sigmoid.
        num_channels = self.input.get_num_channels()
        if self.dense:
            weights = self.create_weigths((num_channels, 1), num_channels)
            biases = self.create_biases((1,))

            self.logits = tf.matmul(self.input.get_output(), weights) + biases
        else:
            # Check.
            if num_channels != 1:
                raise Exception('Number of channels must be 1 if not dense.')

            self.logits = self.input.get_output()

        # Subtract negative weight.
        if self.negative_weight is not None:
            self.logits -= np.log(self.negative_weight)

        # Apply sigmoid activation.
        output = tf.nn.sigmoid(self.logits)

        # Calculate labels.
        self.labels = (self.logits > 0)

        # Add captures.
        self.graph.add_capture(self.name + '/logits', self.logits)
        self.graph.add_capture(self.name + '/probabilities', output)
        self.graph.add_capture(self.name + '/labels', self.labels)

        return output

    def get_statistics(self, true_outputs, output_weights):
        # Calculate statistics.
        with tf.name_scope('statistics'):
            # Get whether maximum prediction is correct.
            true_labels = (true_outputs > 0.5)
            false_labels = tf.logical_not(true_labels)

            positives = tf.to_float(true_labels)
            negatives = tf.to_float(false_labels)

            true_positives = tf.to_float(tf.logical_and(self.labels, true_labels))
            false_positives = tf.to_float(tf.logical_and(self.labels, false_labels))

            # Calculate biased statistics.
            positives_value = tf.reduce_sum(positives)
            negatives_value = tf.reduce_sum(negatives)

            true_positives_value = tf.reduce_sum(true_positives)
            false_positives_value = tf.reduce_sum(false_positives)

            statistics = [
                ('positives', positives_value),
                ('negatives', negatives_value),
                ('sensitivity', true_positives_value),
                ('fp-rate', false_positives_value)
            ]

            # Optionally add unbiased statistics.
            if self.graph.get_parameter('add-unbiased-statistics', False):
                # Get weighted sum.
                positives_value = tf.reduce_sum(output_weights * positives)
                negatives_value = tf.reduce_sum(output_weights * negatives)

                true_positives_value = tf.reduce_sum(output_weights * true_positives)
                false_positives_value = tf.reduce_sum(output_weights * false_positives)

                statistics += [
                    ('unbiased-positives', positives_value),
                    ('unbiased-negatives', negatives_value),
                    ('unbiased-sensitivity', true_positives_value),
                    ('unbiased-fp-rate', false_positives_value)
                ]

            return statistics

    def merge_statistic(self, name, values, batch_size, output_weight_sum, all_statistics):
        if name in ('positives', 'negatives', 'unbiased-positives', 'unbiased-negatives'):
            # Do not report these.
            return None
        elif name == 'sensitivity' or name == 'fp-rate':
            if name == 'sensitivity':
                denominator = tf.reduce_sum(all_statistics['positives'])
            else:
                denominator = tf.reduce_sum(all_statistics['negatives'])

            return tf.reduce_sum(values) / denominator
        elif name == 'unbiased-sensitivity' or name == 'unbiased-fp-rate':
            if name == 'unbiased-sensitivity':
                denominator = tf.reduce_sum(all_statistics['unbiased-positives'])
            else:
                denominator = tf.reduce_sum(all_statistics['unbiased-negatives'])

            return tf.reduce_sum(values) / denominator
        else:
            raise Exception('Unknown statistic.')

    def get_loss(self, true_outputs, output_weights):
        # Cross-entropy loss.
        with tf.name_scope('cross-entropy'):
            cross_entropies = tf.nn.sigmoid_cross_entropy_with_logits(logits=self.logits, labels=true_outputs)

            # Get weighted sum.
            cross_entropy = tf.reduce_sum(output_weights * cross_entropies)

        return cross_entropy


class Convolution(Node):
    """
    Convolves over spatial dimensions.
    """
    def __init__(self, input, num_channels, filter_size, name=None, stride=None, dilate=1,
                 same_padding=False, bias=True, visualize=False):
        self.input = input
        self.num_channels = num_channels
        self.filter_size = filter_size
        self.stride = stride
        self.visualize = visualize
        self.same_padding = same_padding
        self.bias = bias
        self.dilate = dilate

        super().__init__(name, [input])

    def get_name_prefix(self):
        return 'convolution'

    def get_attributes(self):
        return [
            ('num-channels', self.num_channels),
            ('filter-size', self.filter_size),
            ('stride', self.stride),
            ('dilate', self.dilate),
            ('same-padding', self.same_padding),
            ('bias', self.bias),
            ('visualize', self.visualize),
        ]

    def build(self):
        # Check.
        if len(self.filter_size) == 2:
            # 2D convolution.
            if self.input.get_num_dimensions() != 4:
                raise Exception('Input dimensionality of a 2D convolution should be 4.')
        else:
            # 3D convolution.
            if self.input.get_num_dimensions() != 5:
                raise Exception('Input dimensionality of a 3D convolution should be 5.')

        if self.dilate != 1:
            if self.input.get_num_dimensions() != 4:
                raise Exception('Dilation only supported for 2D convolutions.')
            elif self.stride is not None:
                raise Exception('Dilation can not be combined with stride.')

        # Convolve.
        input_channels = self.input.get_num_channels()

        num_inputs = np.prod(self.filter_size) * input_channels

        weights = self.create_weigths(self.filter_size + (input_channels, self.num_channels), num_inputs)

        # TODO: Summary only on main tower and for 2d and on cpu.

        #if self.visualize:
        #    tf.summary.image(self.name + '/filters', tf.transpose(weights, (3, 0, 1, 2)), max_images=self.num_channels)

        strides = (1,) * (len(self.filter_size) + 2) if self.stride is None else (1,) + self.stride + (1,)
        padding = 'SAME' if self.same_padding else 'VALID'

        if self.dilate != 1:
            def func(value, filters, strides, padding):
                # Apply atrous (dilated) convolution.
                output = tf.nn.atrous_conv2d(value, filters, self.dilate, padding)

                # Set output shape as tensorflow does not do it.
                batch_size, height, width, num_channels = self.input.get_shape()
                if self.same_padding:
                    out_height = height
                    out_width = width
                else:
                    filter_height, filter_width = self.filter_size

                    kernel_width = (filter_width - 1) * self.dilate + 1
                    kernel_height = (filter_height - 1) * self.dilate + 1

                    out_width = None if width is None else width - kernel_width + 1
                    out_height = None if height is None else height - kernel_height + 1

                output.set_shape((batch_size, out_height, out_width, self.num_channels))

                return output
        else:
            func = tf.nn.conv2d if len(self.filter_size) == 2 else tf.nn.conv3d

        output = func(self.input.get_output(), weights, strides=strides, padding=padding)

        if self.bias:
            biases = self.create_biases((self.num_channels,))

            output = tf.nn.bias_add(output, biases)

        return output


class Crop(Node):
    """
    Crops spatial dimensions.
    """
    def __init__(self, input, width, name=None):
        self.input = input
        self.width = width

        super().__init__(name, [input])

    def get_name_prefix(self):
        return 'crop'

    def get_attributes(self):
        return [('width', self.width)]

    def build(self):
        # Get croppings.
        if isinstance(self.width, int):
            croppings = [self.width] * (self.input.get_num_dimensions() - 2)
        else:
            # Check length.
            if len(self.width) != self.input.get_num_dimensions() - 2:
                raise Exception('Cropping width list length does not match input dimensions.')

            croppings = self.width

        croppings = [(cropping, cropping) if isinstance(cropping, int) else (cropping[0], cropping[1])
                     for cropping in croppings]

        croppings = [(0, 0)] + croppings + [(0, 0)]

        dynamic_shape = self.input.get_dynamic_shape()

        begin = [cropping[0] for cropping in croppings]
        size = [-1 if cropping[1] == 0 else dynamic_shape[i] - cropping[0] - cropping[1]
                for i, cropping in enumerate(croppings)]

        # Apply cropping.
        output = tf.slice(self.input.get_output(), begin, size)

        # Set output shape as tensorflow does not do it.
        shape = self.input.get_shape()

        new_shape = [None if dim_size is None else dim_size - cropping[0] - cropping[1]
                     for dim_size, cropping in zip(shape, croppings)]

        output.set_shape(new_shape)

        return output


class Merge(Node):
    """
    Merges given inputs.

    Functions supported: add, mul, min, max.
    """
    def __init__(self, inputs, function='add', name=None):
        self.inputs = inputs
        self.function = function

        super().__init__(name, inputs)

    def get_name_prefix(self):
        return 'merge'

    def get_attributes(self):
        return [('function', self.function)]

    def build(self):
        # Check.
        if not self.inputs:
            raise Exception('No inputs given to merge.')

        # Check whether dimensions match.
        num_dimensions = self.inputs[0].get_num_dimensions()
        num_channels = self.inputs[0].get_num_channels()

        for i in range(1, len(self.inputs)):
            if self.inputs[i].get_num_dimensions() != num_dimensions:
                raise Exception('Input dimensions are not compatible.')

            if self.inputs[i].get_num_channels() != num_channels:
                raise Exception('Input shapes are not compatible.')

            # TODO: Check shapes better if defined.

        # Merge.
        output = self.inputs[0].get_output()
        for i in range(1, len(self.inputs)):
            input_output = self.inputs[i].get_output()

            if self.function == 'add':
                output += input_output
            elif self.function == 'mul':
                output *= input_output
            elif self.function == 'min':
                output = tf.minimum(output, input_output)
            elif self.function == 'max':
                output = tf.maximum(output, input_output)
            else:
                raise Exception('Unsupported merge function.')

        return output


class Activation(Node):
    """
    Applies an activation.
    """
    def __init__(self, input, activation, name=None):
        self.input = input
        self.activation = activation

        super().__init__(name, [input])

    def get_name_prefix(self):
        return 'activation'

    def get_attributes(self):
        return [('activation', self.activation)]

    def build(self):
        # Apply activation.
        func = self.graph.get_activation_function(self.activation)

        output = func(self.input.get_output())

        return output
