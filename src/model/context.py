# Copyright (C) 2017  Aidence B.V.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA

import tensorflow as tf

from pycuda import driver

import os

class Context:
    """
    Model context.
    """
    def __init__(self, num_cores=None, use_gpus=None):
        # Determine GPUs to use.
        if use_gpus is None:
            # Use all GPUs.
            gpu_nums = list(range(Context.get_total_num_gpus()))
        elif isinstance(use_gpus, str):
            # Comma separated string of GPU numbers (one-based) to use.
            gpu_nums = list(map(lambda x: int(x) - 1, use_gpus.split(',')))
        elif isinstance(use_gpus, list):
            # List of GPU numbers (one-based) to use.
            gpu_nums = list(map(lambda x: int(x) - 1, use_gpus))
        else:
            # Single GPU number to use.
            gpu_nums = [int(use_gpus) - 1]

        # Set GPUs to use.
        os.environ['CUDA_VISIBLE_DEVICES'] = ','.join(map(str, gpu_nums))

        # Our GPU numbers start at 0.
        self.gpu_nums = list(range(len(gpu_nums)))

        # Set members.
        self.num_cores = num_cores

        # Create graph.
        self.graph = tf.Graph()

        # Create session.
        if num_cores is not None:
            config = tf.ConfigProto(inter_op_parallelism_threads=num_cores,
                                    intra_op_parallelism_threads=num_cores,
                                    allow_soft_placement=True)

            self.session = tf.Session(graph=self.graph, config=config)
        else:
            config = tf.ConfigProto(allow_soft_placement=True)

            self.session = tf.Session(graph=self.graph, config=config)

        # Unset GPUs.
        os.environ['CUDA_VISIBLE_DEVICES'] = ''

    def destroy(self):
        self.session.close()

        self.session = None
        self.graph = None

    def get_gpu_nums(self):
        return self.gpu_nums

    def get_num_gpus(self):
        return len(self.gpu_nums)

    def get_num_cores(self):
        return self.num_cores

    def get_graph(self):
        return self.graph

    def get_session(self):
        return self.session

    @staticmethod
    def get_total_num_gpus():
        # Use CUDA because Tensorflow will immediately initialize.
        driver.init()
        return driver.Device.count()
