# Copyright (C) 2017  Aidence B.V.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA

import numpy as np

import tensorflow as tf

import os
import pickle
import collections


class Model:
    """
    Model base class.
    """

    #
    # Public methods.
    #

    def __init__(self, context, input_shape, output_shape, parameters={}):
        # Members.
        self.context = context
        self.input_shape = tuple(input_shape)
        self.output_shape = tuple(output_shape)
        self.parameters = parameters

        self.metadata = {}
        self.weights = {}
        self.biases = {}
        self.variables = {}
        self.captures = {}
        self.num_parameters = 0

        self.random_state = np.random.RandomState(parameters.get('seed', 42))

        self.graph = None
        self.session = None

        self.inputs = None
        self.outputs = None
        self.output_weights = None
        self.training = None

        self.data_loss = None
        self.regularization_loss = None
        self.loss = None

        self.statistics = None
        self.gradients_and_variables = None
        self.summary = None
        self.init_variables = None

        self.in_main_tower = None

        # Initialize.
        self.init()

    def get_context(self):
        return self.context

    def get_input_shape(self):
        return self.input_shape

    def get_output_shape(self):
        return self.output_shape

    def get_inputs(self):
        return self.inputs

    def get_outputs(self):
        return self.outputs

    def get_output_weigths(self):
        return self.output_weights

    def get_training(self):
        return self.training

    def get_loss(self):
        return self.loss

    def get_data_loss(self):
        return self.data_loss

    def get_statistics(self):
        return self.statistics

    def get_summary(self):
        return self.summary

    def get_num_parameters(self):
        return self.num_parameters

    def get_gradients_and_variables(self):
        return self.gradients_and_variables

    # Evaluates given named captures with the inputs given.
    def evaluate(self, captures=[], inputs=None, outputs=None, output_weights=None):
        fetches = [self.get_capture(capture) for capture in captures]
        feed_dict = {}

        if inputs is not None:
            feed_dict[self.inputs] = inputs

        if outputs is not None:
            feed_dict[self.outputs] = outputs

        if output_weights is not None:
            feed_dict[self.output_weights] = output_weights

        return self.session.run(fetches, feed_dict=feed_dict)

    # Sets named metadata on model. Will be saved if model is saved.
    def set_metadata(self, name, value):
        self.metadata[name] = value

    # Gets metadata by name.
    def get_metadata(self, name, default=None):
        return self.metadata.get(name, default)

    # Gets all metadata.
    def get_all_metadata(self):
        return self.metadata.copy()

    # Sets all metadata.
    def set_all_metadata(self, metadata):
        self.metadata = metadata

    # Returns whether capture exists.
    def has_capture(self, name):
        return name in self.captures or name in self.variables

    # Sets variable values.
    def set_variable_values(self, values):
        # Set new variables.
        assignments = []

        for name, var in self.variables.items():
            if name in values:
                assignment = var.assign(values[name])
                assignments.append(assignment)

        self.session.run(assignments)

    # Gets variable values.
    def get_variable_values(self, names=None):
        # Get names and variables.
        if names is None:
            variables = list(self.variables.values())
            names = list(self.variables.keys())
        else:
            variables = [self.variables[name] for name in names]

        # Evaluate variables.
        values = self.session.run(variables)

        return dict(zip(names, values))

    # Saves model.
    def save(self, directory, step):
        # Determine file name.
        file_name = os.path.join(directory, 'model-{}.pkl'.format(step))

        # Evaluate variables.
        values = self.get_variable_values()

        info = {'variables': values, 'input-shape': self.input_shape,
                'output-shape': self.output_shape, 'parameters': self.parameters, 'metadata': self.metadata}

        # Save.
        with open(file_name, 'wb') as f:
            pickle.dump(info, f)

    # Loads a model. Parameters can be given that will override loaded parameters.
    @classmethod
    def load(cls, context, directory, step, input_shape=None, output_shape=None, parameters={}):
        # Load.
        file_name = os.path.join(directory, 'model-{}.pkl'.format(step))

        with open(file_name, 'rb') as f:
            info = pickle.load(f)

        model_parameters = info['parameters']
        model_parameters.update(parameters)

        input_shape = info['input-shape'] if input_shape is None else input_shape
        output_shape = info['output-shape'] if output_shape is None else output_shape

        model = cls(context, input_shape, output_shape, model_parameters)

        # Set new variables.
        model.set_variable_values(info['variables'])

        # Set metadata.
        for name, value in info['metadata'].items():
            model.set_metadata(name, value)

        return model

    #
    # Private methods.
    #

    def init(self):
        # Initialize.
        self.graph = self.context.get_graph()
        self.session = self.context.get_session()
        with self.graph.as_default():
            self.build_graph()

        self.session.run(self.init_variables)

    def build_graph(self):
        # Create placeholders.
        self.inputs = tf.placeholder(tf.float32, self.input_shape, name='inputs')
        self.outputs = tf.placeholder(tf.float32, self.output_shape, name='outputs')
        self.output_weights = tf.placeholder(tf.float32, tuple(self.output_shape[:-1]) + (1,), name='output-weights')
        self.training = tf.placeholder_with_default(False, (), name='training')

        # Normalize output weights so batch size does not matter.
        batch_size = tf.to_float(tf.shape(self.inputs)[0])

        normalized_output_weights = self.output_weights * (1 / batch_size)

        # Create model template.
        # Subsequent calls to this method will reuse variables created with first call.
        model_template = tf.make_template('model', self.build_model)

        # Build model.
        devices = ['/gpu:{}'.format(gpu_num) for gpu_num in self.context.get_gpu_nums()]
        if not devices:
            devices = ['/cpu:0']

        num_devices = len(devices)

        split_inputs = tf.split(self.inputs, num_devices, axis=0)
        split_outputs = tf.split(self.outputs, num_devices, axis=0)
        split_output_weights = tf.split(normalized_output_weights, num_devices, axis=0)

        outputs = []
        losses = []
        output_weight_sums = []
        gradients = []
        statistics = collections.OrderedDict()

        all_variables = None

        for tower_num, device in enumerate(devices):
            self.in_main_tower = (tower_num == 0)

            with tf.device(device):
                # Apply model.
                output, loss, device_statistics = \
                    model_template(tower_num, split_inputs[tower_num], split_outputs[tower_num],
                                   split_output_weights[tower_num], self.input_shape, self.output_shape)

                # Get output weight sum.
                output_weight_sum = tf.reduce_sum(split_output_weights[tower_num])

                # Calculate gradients.
                if all_variables is None:
                    all_variables = tf.trainable_variables()

                gradient = tf.gradients(loss, all_variables)
                gradient = [0 if value is None else value for value in gradient]

                # Add partial outputs.
                outputs.append(output)
                losses.append(loss)
                output_weight_sums.append(output_weight_sum)
                gradients.append(gradient)

                # Add statistics by name.
                for name, statistic in device_statistics:
                    if name in statistics:
                        statistics[name].append(statistic)
                    else:
                        statistics[name] = [statistic]

        # Get output.
        all_output = tf.concat(outputs, axis=0)

        self.captures['output'] = all_output

        # Set data loss (estimate).
        output_weight_sum = tf.add_n(output_weight_sums)

        self.data_loss = tf.add_n(losses) / output_weight_sum

        tf.summary.scalar('data-loss', self.data_loss)

        self.captures['data-loss'] = self.data_loss

        # Add regularization.
        self.regularization_loss = self.build_regularization(self.weights, self.biases)

        tf.summary.scalar('regularization-loss', self.regularization_loss)

        self.captures['regularization-loss'] = self.regularization_loss

        # Add gradients of regularization.
        gradient = tf.gradients(self.regularization_loss, all_variables)
        gradient = [0 if value is None else value for value in gradient]

        gradients.append(gradient)

        # Set loss.
        self.loss = self.data_loss + self.regularization_loss

        tf.summary.scalar('total-loss', self.loss)

        self.captures['loss'] = self.loss

        # Merge statistics.
        self.statistics = self.merge_statistics(statistics, batch_size, output_weight_sum)

        for name, statistic in self.statistics.items():
            tf.summary.scalar(name, statistic)

            self.captures[name] = statistic

        # Sum gradients.
        all_gradients = gradients[0]

        for tower_gradients in gradients[1:]:
            for i, tower_gradient in enumerate(tower_gradients):
                all_gradients[i] += tower_gradient

        gradients_and_variables = list(zip(all_gradients, all_variables))

        # Adjust gradients.
        self.gradients_and_variables = self.adjust_gradients(gradients_and_variables)

        # Summaries.
        self.summary = tf.summary.merge_all()

        # Initialization.
        with tf.name_scope('initialization'):
            self.init_variables = tf.global_variables_initializer()

    #
    # Protected methods that can/should be implemented by subclass.
    #

    def build_model(self, tower_num, inputs, true_outputs, output_weights, input_shape, output_shape):
        """
        Builds the actual model. May be called multiple times for different towers.

        :return (outputs, loss, statistics) tuple.
        """
        raise NotImplementedError()

    def merge_statistics(self, statistics, batch_size, output_weight_sum):
        """
        Merges statistics.
        """
        return collections.OrderedDict()

    def build_regularization(self, weights, biases):
        # L1 regularization.
        l1_regularization_scale = self.parameters.get('l1-regularization', 0.0)

        regularization = 0
        if l1_regularization_scale != 0.0:
            with tf.name_scope('l1-regularization'):
                abs_weights = [tf.reduce_sum(tf.abs(weights)) for weights in weights.values()]

                if abs_weights:
                    regularization = l1_regularization_scale * tf.add_n(abs_weights)

        # L2 regularization.
        l2_regularization_scale = self.parameters.get('l2-regularization', 0.0)

        if l2_regularization_scale != 0.0:
            with tf.name_scope('l2-regularization'):
                squared_weights = [tf.reduce_sum(weights * weights) for weights in weights.values()]

                if squared_weights:
                    regularization += l2_regularization_scale * 0.5 * tf.add_n(squared_weights)

        return regularization

    # Makes sure constraints are met (eg: max-norm on weights). May be implemented in subclass.
    def enforce_constraints(self):
        pass

    # Adjusts gradients (eg: caps them). May be implemented in subclass.
    def adjust_gradients(self, gradients_and_variables):
        return gradients_and_variables

    #
    # Private utility methods.
    #

    def create_weights(self, name, shape, num_inputs):
        # Set names.
        weights_name = name + '/weights'
        gamma_name = name + '/gamma'

        # Get values.
        init_method = self.parameters.get('init-method', 'normal')
        if init_method == 'xavier':
            # Not strictly speaking Xavier, as we do not have the output number.
            values = self.random_state.randn(*shape).astype(np.float32)
            values *= np.sqrt(2.0 / num_inputs)

            default_gain = 1.0
        elif init_method == 'normal':
            # Normal (Gaussian) initialization.
            values = self.random_state.randn(*shape).astype(np.float32)

            default_gain = 0.05
        else:
            raise Exception('Unknown init method.')

        values *= self.parameters.get('init-gain', default_gain)

        # Create trainable variable.
        var = tf.get_variable(weights_name, shape, initializer=tf.constant_initializer(values), trainable=True)

        # Optionally apply weight-normalization.
        gamma_var = None
        value = var

        if self.parameters.get('weight-normalization', False):
            value = tf.nn.l2_normalize(value, list(range(len(shape) - 1)))

            # Optionally apply scaling.
            if self.parameters.get('weight-normalization-gamma', False):
                gamma_var = tf.get_variable(gamma_name, shape[-1], initializer=tf.constant_initializer(1.0), trainable=True)

                value *= gamma_var

        if self.in_main_tower:
            # Create histogram.
            with tf.device('/cpu:0'):
                tf.summary.histogram(weights_name, var)

            # Add statistics.
            self.num_parameters += np.prod(shape)

            if gamma_var is not None:
                self.num_parameters += shape[-1]

            # Add variables.
            self.weights[weights_name] = var
            self.add_variable(weights_name, var)

            if gamma_var is not None:
                self.add_variable(gamma_name, gamma_var)

        return var, gamma_var, value

    def create_biases(self, name, shape):
        # Set name.
        name += '/biases'

        # Get values.
        values = np.zeros(*shape).astype(np.float32)

        # Create variable.
        var = tf.get_variable(name, shape, initializer=tf.constant_initializer(values), trainable=True)

        if self.in_main_tower:
            # Create histogram.
            with tf.device('/cpu:0'):
                tf.summary.histogram(name, var)

            # Add statistics.
            self.num_parameters += np.prod(shape)

            # Add variable.
            self.biases[name] = var
            self.add_variable(name, var)

        return var, var

    def get_activation_function(self, name):
        if name == 'linear':
            return id
        elif name == 'relu':
            return tf.nn.relu
        elif name == 'relu6':
            return tf.nn.relu6
        elif name == 'elu':
            return tf.nn.elu
        elif name == 'celu':
            return lambda x: tf.nn.elu(tf.concat([x, -x], axis=x.get_shape().ndims - 1))
        elif name == 'crelu':
            return lambda x: tf.nn.relu(tf.concat([x, -x], axis=x.get_shape().ndims - 1))
        elif name == 'tanh':
            return tf.nn.tanh
        elif name == 'sigmoid':
            return tf.nn.sigmoid
        else:
            raise Exception('Unknown activation function \'{}\'.'.format(name))

    def get_capture(self, name):
        if name in self.captures:
            return self.captures[name]
        elif name in self.variables:
            return self.variables[name]
        else:
            raise Exception('Could not find capture named: \'{}\'.'.format(name))

    def add_capture(self, name, value):
        if not self.in_main_tower:
            if name not in self.captures:
                raise Exception('Adding extra capture named \'{}\', but not in main tower.'.format(name))

            return

        if name in self.captures:
            raise Exception('A capture named \'{}\' already exists.'.format(name))

        self.captures[name] = value

    def add_variable(self, name, value):
        if not self.in_main_tower:
            if name not in self.variables:
                raise Exception('Adding extra variable named \'{}\', but not in main tower.'.format(name))

            return

        if name in self.variables:
            raise Exception('A variable named \'{}\' already exists.'.format(name))

        self.variables[name] = value

    def get_parameter(self, name, default_value):
        return self.parameters.get(name, default_value)
