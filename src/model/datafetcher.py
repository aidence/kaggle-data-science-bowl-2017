# Copyright (C) 2017  Aidence B.V.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA

import threading
import queue

import numpy as np


class DataFetcher:
    """
    Fetches data asynchronously from multiple data providers.
    """

    def __init__(self, providers, batch_sizes, frequencies=None, queue_size=100, asynchronous=True):
        # Set members.
        self.providers = providers
        self.batch_sizes = batch_sizes
        self.asynchronous = asynchronous

        # Setup asynchronous thread.
        if asynchronous:
            # Create queues.
            self.token_queue = queue.Queue()
            self.batch_queues = {name: queue.Queue() for name in providers.keys()}

            # Put tokens in queue.
            if frequencies is None:
                frequencies = {name: 1 for name in providers.keys()}

            sorted_providers = sorted(providers.items(), key=lambda item: frequencies[item[0]], reverse=True)

            frequency_sum = sum(frequencies.values())
            for name, provider in sorted_providers:
                num_tokens = max(1, int(np.ceil(frequencies[name] / frequency_sum * queue_size)))

                for i in range(num_tokens):
                    self.token_queue.put(name)

            # Start thread.
            self.stop_event = threading.Event()

            self.thread = threading.Thread(target=self.thread_main)
            self.thread.start()

    def stop(self):
        if not self.asynchronous:
            return

        # Stop and join thread.
        self.stop_event.set()

        self.thread.join()

    def get_mini_batch(self, name):
        if not self.asynchronous:
            return self.providers[name].get_mini_batch(self.batch_sizes[name])

        # Get mini-batch.
        mini_batch = self.batch_queues[name].get()

        # Put token back in queue.
        self.token_queue.put(name)

        return mini_batch

    def thread_main(self):
        # Watch event.
        while not self.stop_event.is_set():
            # Get token.
            try:
                token = self.token_queue.get(timeout=1)
            except queue.Empty:
                continue

            # Get data and put it in queue.
            mini_batch = self.providers[token].get_mini_batch(self.batch_sizes[token])

            self.batch_queues[token].put(mini_batch)
