# Copyright (C) 2017  Aidence B.V.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA

import glob
import argparse
import os
import multiprocessing
import pickle
import datetime
from util.util import Util

import numpy as np


def worker_process_main(file_name_queue, candidate_queue, threshold, size_threshold, no_masking):
    import inference

    import queue

    # Walk over files.
    candidate_calculator = inference.CandidateCalculator(threshold, size_threshold)

    data_sets = {}
    while True:
        # Try to get a file name.
        try:
            file_name = file_name_queue.get_nowait()
        except queue.Empty:
            break

        # Read data.
        with open(file_name, 'rb') as f:
            entry = pickle.load(f)

        scan_id = entry['id']
        logits = entry['logits']
        start = entry['start']
        mode = entry.get('mode', 'mask')
        large = entry.get('large', False)

        # Get data-set.
        data_set_name = entry['data-set']
        if data_set_name in data_sets:
            data_set = data_sets[data_set_name]
        else:
            data_set = Util.load_data_set(data_set_name)

            data_sets[data_set_name] = data_set

        # Get scan.
        scan = data_set.get_scan(scan_id)

        if mode != 'nodules':
            mask = scan.get_mask()

            candidates = candidate_calculator.calculate(logits, start, mask=mask, large=large, in_mask=not no_masking,
                                                        world_offset=scan.get_offset(), world_scale=scan.get_scale())
        else:
            # Do not use mask as it may have to be created.
            candidates = candidate_calculator.calculate(logits, start, large=large,
                                                        world_offset=scan.get_offset(), world_scale=scan.get_scale())

        # Check with true positives.
        nodules = scan.get_nodules()
        if not nodules:
            # There are no positives in scan.
            positives = []
            negatives = candidates
        else:
            # For each candidate determine whether it's a positive.
            positives = []
            negatives = []

            for candidate in candidates:
                center = candidate['center']

                found_nodule = None
                for nodule in nodules:
                    if nodule.is_point_inside_bounding_sphere(center):
                        found_nodule = nodule
                        break

                if found_nodule is not None:
                    # Add nodule information.
                    candidate['nodule-metadata'] = found_nodule.get_metadata()
                    candidate['nodule-radius'] = found_nodule.get_radius()
                    candidate['nodule-included'] = found_nodule.is_included()

                    positives.append(candidate)
                else:
                    negatives.append(candidate)

            # Ignore negatives if in nodules mode.
            if mode == 'nodules':
                negatives = []

        # Put them in queue.
        the_data = {
            'type': 'candidates',
            'data-set': data_set_name,
            'id': scan_id,
            'mode': mode,
            'large': large,
            'threshold': threshold,
            'size-threshold': size_threshold,
            'in-mask': not no_masking,
            'candidates': candidates,
            'positives': positives,
            'negatives': negatives,
        }

        candidate_queue.put(the_data)


def write_entries(entries, output_directory, batch_num):
    date = 'UTC-{:%Y-%m-%d@%H:%M:%S}'.format(datetime.datetime.utcnow())

    file_name = os.path.join(output_directory, 'candidates-{}-{}.pkl'.format(date, batch_num))

    with open(file_name, 'wb') as f:
        pickle.dump(entries, f)


def accumulator_process_main(candidate_queue, num_entries, output_directory):
    # Accumulate.
    entries = []
    batch_num = 1
    num_in_batch = 100
    num_written = 0
    num_positives = 0
    num_negatives = 0

    while True:
        # Get an entry.
        entry = candidate_queue.get()

        # Check whether we are done.
        if entry is None:
            break

        entries.append(entry)

        num_positives += len(entry['positives'])
        num_negatives += len(entry['negatives'])

        # Check for write.
        if len(entries) == num_in_batch:
            write_entries(entries, output_directory, batch_num)

            entries = []
            num_written += num_in_batch
            batch_num += 1

            print('{}/{} ({:.1%}), positives: {}, negatives: {}.'.format(
                num_written,
                num_entries,
                num_written / num_entries,
                num_positives,
                num_negatives))

    # Write remaining entries.
    if entries:
        write_entries(entries, output_directory, batch_num)

        num_written += len(entries)

        print('{}/{} ({:.1%}), positives: {}, negatives: {}.'.format(
            num_written,
            num_entries,
            num_written / num_entries,
            num_positives,
            num_negatives))


def main():
    # Parse command line options.
    parser = argparse.ArgumentParser(description='Write candidates from logits.',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('output_directory', metavar='output-directory', type=str, help='Output directory.')
    parser.add_argument('logit_directories', metavar='logit-directories', type=str, nargs='+',
                        help='Directories where logits are stored.')
    parser.add_argument('--num-processes', metavar='num-processes', type=int, help='Number of processes to use.')
    parser.add_argument('--threshold', metavar='threshold', type=float, help='Logit threshold.')
    parser.add_argument('--size-threshold', metavar='size-threshold', type=float, help='Size threshold.')
    parser.add_argument('--no-masking', action='store_true', help='Do not mask.')

    args = parser.parse_args()

    # Create directory.
    os.makedirs(args.output_directory, exist_ok=True)

    # Get pickle files.
    file_names = []

    for logits_directory in args.logit_directories:
        file_names += glob.glob(os.path.join(logits_directory, 'logits-*.pkl'))

    print('Found {} logit files.'.format(len(file_names)))

    # Distribute it.
    Util.distribute_cpu_work(file_names, worker_process_main, accumulator_process_main,
                             num_processes=args.num_processes,
                             worker_extra_args=(args.threshold, args.size_threshold, args.no_masking),
                             accumulator_extra_args=(args.output_directory,))


if __name__ == '__main__':
    main()
