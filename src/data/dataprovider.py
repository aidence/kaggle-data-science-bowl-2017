# Copyright (C) 2017  Aidence B.V.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA

import model
import numpy as np

import threading


class DataProvider(model.DataProvider):
    """
    Training data provider based on patches.
    """

    def __init__(self, file_name, test, region=1, final_region=1, negative_weight=2, normalize_positives=False, seed=42):
        # Set members.
        self.region = region
        self.final_region = final_region
        self.test = test
        self.negative_weight = negative_weight
        self.normalize_positives = normalize_positives

        # Load data.
        self.data = np.load(file_name)

        prefix = 'test' if test else 'train'

        self.positive_examples = self.data[prefix + '-positive-inputs']
        self.negative_examples = self.data[prefix + '-negative-inputs']

        # Reshape to 5D.
        self.positive_examples = np.expand_dims(self.positive_examples, -1)
        self.negative_examples = np.expand_dims(self.negative_examples, -1)

        self.positive_indices = list(range(len(self.positive_examples)))
        self.negative_indices = list(range(len(self.negative_examples)))

        self.positive_index = 0
        self.negative_index = 0

        self.positive_weights = None
        self.negative_weights = None

        self.positive_outputs = None
        self.negative_outputs = None

        self.negative_weight_factor = None
        self.negative_data_lock = threading.Lock()

        # Create random state.
        self.random_state = np.random.RandomState(seed)

        self.create_outputs()

    def get_mini_batch_input_shape(self):
        return (None,) + self.positive_examples.shape[1:]

    def get_mini_batch_output_shape(self):
        return None, 1, self.region, self.region, 1

    def get_mini_batch(self, size):
        # Get amounts.
        positive_num = size // 2
        negative_num = size - positive_num

        # Get indices.
        positive_indices = self.get_indices(positive_num, positive=True)
        negative_indices = self.get_indices(negative_num, positive=False)

        # Get data.
        positive_examples = self.positive_examples[positive_indices]
        positive_outputs = self.positive_outputs[positive_indices]
        positive_weights = self.positive_weights[positive_indices]

        negative_outputs = self.negative_outputs[negative_indices]

        with self.negative_data_lock:
            negative_examples = self.negative_examples[negative_indices]
            negative_weights = self.negative_weights[negative_indices]

        inputs = np.concatenate((positive_examples, negative_examples), axis=0)
        outputs = np.concatenate((positive_outputs, negative_outputs), axis=0)
        weights = np.concatenate((positive_weights, negative_weights), axis=0)

        inputs = (inputs - np.float32(127.5)) * np.float32(1 / 127.5)

        # Shuffle it so statistics of splits are not biased.
        shuffle_indices = self.random_state.permutation(size)

        inputs = inputs[shuffle_indices]
        outputs = outputs[shuffle_indices]
        weights = weights[shuffle_indices]

        return inputs, outputs, weights

    def get_everything(self, size):
        raise NotImplementedError()

    def create_outputs(self):
        # Set negative weights. Weight is repeated for every output pixel.
        prefix = 'test' if self.test else 'train'

        self.negative_weights = self.data[prefix + '-negative-weights']
        self.negative_weights = self.negative_weights.repeat(self.region * self.region)
        self.negative_weights = self.negative_weights.reshape((-1, 1, self.region, self.region, 1))

        # Make them be 1 per pixel on average.
        self.negative_weights *= 1 / np.mean(self.negative_weights.astype(np.float64))

        # Create positive weights.
        self.positive_weights = np.ones((len(self.positive_examples), 1, self.region, self.region, 1), dtype=np.float32)

        # Create output arrays.
        self.positive_outputs = np.zeros(self.positive_weights.shape, dtype=np.float32)
        self.negative_outputs = np.zeros(self.negative_weights.shape, dtype=np.float32)

        # Set nodules as positive and count positive weight.
        total_positive_weight = 0
        for i, nodule_dimensions in enumerate(self.data[prefix + '-positive-dimensions']):
            # Dimensions are stored as z-y-x.
            start_x = max(0, (self.region - nodule_dimensions[2]) // 2)
            start_y = max(0, (self.region - nodule_dimensions[1]) // 2)

            end_x = start_x + nodule_dimensions[2]
            end_y = start_y + nodule_dimensions[1]

            self.positive_outputs[i,:,start_y:end_y,start_x:end_x,:] = 1

            non_zero_count = np.count_nonzero(self.positive_outputs[i])

            total_positive_weight += non_zero_count

            if self.normalize_positives:
                # Normalize positive area.
                self.positive_weights[i][self.positive_outputs[i] != 0] *= 1 / non_zero_count

        # Get average weights per pixel.
        average_positive_weight = total_positive_weight / len(self.positive_examples)
        average_negative_weight = 2 * self.final_region * self.final_region - average_positive_weight

        if self.normalize_positives:
            # Weight has already been normalized.
            average_positive_weight = 1

        # Determine weight factors.
        positive_weight_factor = 2 / (self.negative_weight + 1)
        negative_weight_factor = 2 * self.negative_weight / (self.negative_weight + 1)

        positive_weight_factor *= 1 / average_positive_weight
        negative_weight_factor *= 1 / average_negative_weight

        # Correct weights.
        self.positive_weights[self.positive_outputs != 0] *= positive_weight_factor
        self.positive_weights[self.positive_outputs == 0] *= negative_weight_factor
        self.negative_weights *= negative_weight_factor

        # Save factor.
        self.negative_weight_factor = negative_weight_factor

    def get_indices(self, num, positive):
        # Get indices array.
        all_indices = self.positive_indices if positive else self.negative_indices

        # Draw indices.
        if positive:
            indices = all_indices[self.positive_index:self.positive_index + num]
            self.positive_index += num
        else:
            indices = all_indices[self.negative_index:self.negative_index + num]
            self.negative_index += num

        while len(indices) < num:
            # Not enough, reshuffle indices.
            self.random_state.shuffle(all_indices)

            # Add more examples.
            num_left = num - len(indices)

            indices += all_indices[0:num_left]

            # Reset index.
            if positive:
                self.positive_index = num_left
            else:
                self.negative_index = num_left

        return indices
