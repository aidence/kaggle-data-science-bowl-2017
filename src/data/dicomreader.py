# Copyright (C) 2017  Aidence B.V.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA

import os
import pickle
import warnings

import dicom
import dicom.errors
import numpy as np
import scipy
import scipy.ndimage
import scipy.interpolate
import scipy.ndimage._nd_image
import SimpleITK as sitk

from util.point import Point3D


class DicomRejectedException(Exception):
    pass


class Dicom:
    def __init__(self,
                 offset, scale, x_basis, y_basis, size,
                 original_offset, original_scale, original_x_basis, original_y_basis, original_size, original_z_values,
                 instance_numbers, slice_thickness, convolution_kernel,
                 accession_number, patient_id, study_uid, series_uid, source,
                 data=None):
        self.offset = offset
        self.scale = scale
        self.x_basis = x_basis
        self.y_basis = y_basis
        self.size = size

        self.original_offset = original_offset
        self.original_scale = original_scale
        self.original_x_basis = original_x_basis
        self.original_y_basis = original_y_basis
        self.original_size = original_size
        self.original_z_values = original_z_values

        self.instance_numbers = instance_numbers
        self.slice_thickness = slice_thickness
        self.convolution_kernel = convolution_kernel

        self.accession_number = accession_number
        self.patient_id = patient_id
        self.study_uid = study_uid
        self.series_uid = series_uid
        self.source = source

        self.data = data

    def get_metadata(self):
        metadata = {
            'offset': self.offset,
            'scale': self.scale,
            'x-basis': self.x_basis,
            'y-basis': self.y_basis,
            'size': self.size,

            'original-offset': self.original_offset,
            'original-scale': self.original_scale,
            'original-x-basis': self.original_x_basis,
            'original-y-basis': self.original_y_basis,
            'original-size': self.original_size,
            'original-z-values': self.original_z_values,

            'instance-numbers': self.instance_numbers,
            'slice-thickness': self.slice_thickness,
            'convolution-kernel': self.convolution_kernel,

            'accession-number': self.accession_number,
            'patient-id': self.patient_id,
            'study-uid': self.study_uid,
            'series-uid': self.series_uid,
            'source': self.source,
        }

        return metadata

    def get_data(self):
        return self.data

    def get_data_as_uint8(self, start_unit=-1000, end_unit=300):
        if self.data is None:
            raise Exception('No data available.')

        return np.clip((self.data - start_unit) / (end_unit - start_unit) * 256, 0, 255).astype(np.uint8)

    def get_data_as_int16(self):
        if self.data is None:
            raise Exception('No data available.')

        return np.clip(np.round(self.data), np.iinfo(np.int16).min, np.iinfo(np.int16).max).astype(np.int16)

    def save_data_as_uint8(self, file_name):
        data = self.get_data_as_uint8(-1000, 300)

        np.savez_compressed(file_name, data=data)

    def save_data_as_int16(self, file_name):
        data = self.get_data_as_int16()

        np.savez_compressed(file_name, data=data)

    def save_metadata(self, file_name):
        with open(file_name, 'wb') as f:
            pickle.dump(self.get_metadata(), f)


class DicomReader:
    required_attributes = [
        'InstanceNumber',
        #'AccessionNumber',
        #'ImageType',
        #'ConvolutionKernel',
        'ImagePositionPatient',
        'ImageOrientationPatient',
        'PixelSpacing',
        'Rows',
        'Columns',
        'RescaleSlope',
        'RescaleIntercept',
        #'SliceThickness',
    ]

    def __init__(self):
        pass

    def read_series(self, directory, read_data=True, minimum_depth=200, maximum_z_spacing=3, maximum_xy_spacing=1,
                    output_scale=Point3D(2.5, 0.512, 0.512), check_extension=False, source=None):
        accession_number = None
        convolution_kernel = None
        images = []
        x_basis = None
        y_basis = None
        xy_offset = None
        xy_scale = None
        height = None
        width = None

        slice_file_names = []
        for slice in os.listdir(directory):
            # Check extension.
            if check_extension and os.path.splitext(slice)[1] != '.dcm':
                continue

            # Read image.
            slice_file_name = os.path.join(directory, slice)

            try:
                image = dicom.read_file(slice_file_name, stop_before_pixels=True)
            except dicom.errors.InvalidDicomError:
                raise DicomRejectedException('Not a DICOM image: {}.'.format(slice_file_name))

            # Check attributes.
            for name in DicomReader.required_attributes:
                if name not in image:
                    raise DicomRejectedException('Attribute not found: {}.'.format(name))

            # Skip localizer images.
            if 'ImageType' in image and 'LOCALIZER' in image.ImageType:
                continue

            # Check accession number.
            if 'AccessionNumber' in image:
                image_accession_number = str(image.AccessionNumber).strip()
                if accession_number is None:
                    accession_number = image_accession_number
                elif accession_number != image_accession_number:
                    raise DicomRejectedException(
                        'Accession number does not match: {} vs {}.'.format(image_accession_number, accession_number))

            # Check kernel.
            if 'ConvolutionKernel' in image:
                if convolution_kernel is None:
                    convolution_kernel = image.ConvolutionKernel
                elif convolution_kernel != image.ConvolutionKernel:
                    raise DicomRejectedException(
                        'Convolution kernel does not match: {} vs {}.'.format(convolution_kernel, image.ConvolutionKernel))

            # Check image offset.
            image_offset = list(map(float, image.ImagePositionPatient[:2]))
            if xy_offset is None:
                xy_offset = image_offset
            elif not np.all(np.isclose(xy_offset, image_offset, atol=1e-4)):
                raise DicomRejectedException('Image offset does not match: {} vs {}.'.format(image_offset, xy_offset))

            # Check image basis.
            image_basis = list(map(float, image.ImageOrientationPatient))

            if x_basis is None:
                x_basis = image_basis[:3]
                y_basis = image_basis[3:]

                if not np.all(np.isclose(np.abs(x_basis), [1, 0, 0], atol=1e-5)):
                    raise DicomRejectedException('X basis not supported: {}.'.format(x_basis))

                if not np.all(np.isclose(np.abs(y_basis), [0, 1, 0], atol=1e-5)):
                    raise DicomRejectedException('Y basis not supported: {}.'.format(y_basis))
            else:
                if not np.all(np.isclose(image_basis[:3], x_basis, atol=1e-5)):
                    raise DicomRejectedException('X Basis does not match: {} vs {}.'.format(x_basis, image_basis[:3]))

                if not np.all(np.isclose(image_basis[3:], y_basis, atol=1e-5)):
                    raise DicomRejectedException('Y Basis does not match: {} vs {}.'.format(y_basis, image_basis[3:]))

            # Check image scale.
            image_xy_scale = list(map(float, image.PixelSpacing))
            if xy_scale is None:
                xy_scale = image_xy_scale

                if xy_scale[0] > maximum_xy_spacing:
                    raise DicomRejectedException('X spacing too large: {} > {}.'.format(xy_scale[0], maximum_xy_spacing))
                elif xy_scale[1] > maximum_xy_spacing:
                    raise DicomRejectedException('Y spacing too large: {} > {}.'.format(xy_scale[1], maximum_xy_spacing))
            elif not np.all(np.isclose(image_xy_scale, xy_scale, atol=1e-5)):
                raise DicomRejectedException('XY spacing does not match: {} vs {}.'.format(xy_scale, image_xy_scale))

            # Check image size.
            image_height = image.Rows
            image_width = image.Columns

            if height is None:
                height = image_height
                width = image_width
            elif image_height != height or image_width != width:
                raise DicomRejectedException('Image dimensions do not match ({}, {}) vs ({}, {}).'.format(
                    width, height, image_width, image_height))

            # Append file name and image.
            images.append((slice_file_name, image))

        # Check length. We need at least two to determine z spacing.
        if len(images) < 2:
            raise DicomRejectedException('Not enough slices: {}.'.format(len(images)))

        # Sort images by z.
        images.sort(key=lambda info: float(info[1].ImagePositionPatient[2]))

        # Get z scale.
        z_values = np.array([float(image.ImagePositionPatient[2]) for _, image in images], dtype=np.float32)

        z_differences = z_values[1:] - z_values[:-1]

        z_scale_median = np.median(z_differences)
        z_scale_mean = np.mean(z_differences)

        # Check it.
        uniform_z_spacing = np.all(np.isclose(z_differences, z_scale_median, atol=1e-2))

        if z_scale_median > maximum_z_spacing:
            raise DicomRejectedException('Z spacing too large: {} > {}.'.format(z_scale_median, maximum_z_spacing))

        z_length = z_values[-1] - z_values[0]
        if z_length < minimum_depth:
            raise DicomRejectedException('Scan has not enough depth: {} < {}.'.format(z_length, minimum_depth))

        # Determine mirror.
        mirror_x = (x_basis[0] < 0)
        mirror_y = (y_basis[1] < 0)

        # Get scale, offset and size.
        x_scale = xy_scale[0]
        y_scale = xy_scale[1]
        z_scale = z_scale_mean

        x_offset = xy_offset[0]
        y_offset = xy_offset[1]
        z_offset = float(images[0][1].ImagePositionPatient[2])

        depth = len(images)

        original_scale = Point3D(z_scale, y_scale, x_scale)
        original_offset = Point3D(z_offset, y_offset, x_offset)
        original_size = Point3D(depth, height, width)

        # Shift offset when mirroring.
        if mirror_x:
            x_offset -= (width - 1) * x_scale

        if mirror_y:
            y_offset -= (height - 1) * y_scale

        output_offset = Point3D(z_offset, y_offset, x_offset)

        # Get basis.
        original_x_basis = Point3D.from_xyz_list(x_basis)
        original_y_basis = Point3D.from_xyz_list(y_basis)

        output_x_basis = Point3D(0, 0, 1)
        output_y_basis = Point3D(0, 1, 0)

        # Get output size.
        if output_scale is not None:
            output_size = Point3D(
                int(round(z_length / output_scale.z + 1)),
                int(round((height - 1) * original_scale.y / output_scale.y + 1)),
                int(round((width - 1) * original_scale.x / output_scale.x + 1))
            )
        else:
            output_scale = original_scale
            output_size = original_size

        # Get instance numbers, z values and slice thickness.
        instance_numbers = np.array([int(image.InstanceNumber) for _, image in images])
        original_z_values = np.array([float(image.ImagePositionPatient[2]) for _, image in images], dtype=np.float32)
        slice_thickness = float(images[0][1].SliceThickness) if 'SliceThickness' in images[0][1] else None

        # Get identification info.
        patient_id = str(images[0][1].PatientID).strip() if 'PatientID' in images[0][1] else None
        study_uid = str(images[0][1].StudyInstanceUID).strip() if 'StudyInstanceUID' in images[0][1] else None
        series_uid = str(images[0][1].SeriesInstanceUID).strip() if 'SeriesInstanceUID' in images[0][1] else None

        # Optionally read data.
        if read_data:
            # Read data.
            try:
                data = np.empty((depth, height, width), dtype=np.float32)
                for i, (file_name, image) in enumerate(images):
                    # Read data.
                    image_data = sitk.ReadImage(file_name)

                    data[i] = sitk.GetArrayFromImage(image_data)[0]
            except (TypeError, ValueError):
                # TODO: I don't know what exception will be thrown on error.
                raise DicomRejectedException('Dicom data corrupted or not available.')

            # Mirror data.
            if mirror_x and mirror_y:
                data = data[:, ::-1, ::-1]
            elif mirror_x:
                data = data[:, :, ::-1]
            elif mirror_y:
                data = data[:, ::-1, :]

            # Scale data.
            data = DicomReader.scale_data(data, original_scale, output_scale, z_values, output_size, uniform_z_spacing)
        else:
            data = None

        return Dicom(output_offset, output_scale, output_x_basis, output_y_basis, output_size,
                     original_offset, original_scale, original_x_basis, original_y_basis, original_size, original_z_values,
                     instance_numbers, slice_thickness, convolution_kernel,
                     accession_number, patient_id, study_uid, series_uid, source,
                     data)

    @staticmethod
    def scale_data(data, original_scale, output_scale, z_values, output_size, uniform_z_spacing):
        # Check for quick case.
        if uniform_z_spacing:
            # Determine scaling matrix.
            matrix = (output_scale.z / original_scale.z, output_scale.y / original_scale.y, output_scale.x / original_scale.x)

            # Determine output shape.
            output_shape = (output_size.z, output_size.y, output_size.x)

            # Due to a bug (or unwanted behavior) in zoom function scaling factor is determined by array shape ratio,
            # not by given zoom factor. So we cannot do:
            #
            # return scipy.ndimage.interpolation.zoom(data, .., order=1, mode='nearest')
            #
            # But must instead use scipy.ndimage.affine_transform(..).
            #
            with warnings.catch_warnings():
                warnings.simplefilter('ignore', UserWarning)

                return scipy.ndimage.interpolation.affine_transform(data, matrix, output_shape=output_shape, order=1, mode='nearest')
        else:
            # Construct original locations.
            z = z_values - z_values[0]
            y = np.arange(data.shape[1]) * original_scale.y
            x = np.arange(data.shape[2]) * original_scale.x

            # Differences are not allowed to be exactly zero, so move zero differences a tiny bit.
            for i in range(1, len(z)):
                if z[i] <= z[i - 1]:
                    z[i] = np.nextafter(z[i - 1], np.float32(np.inf))

            # Build coordinates array.
            new_z = (np.arange(output_size.z) * output_scale.z).astype(np.float32)
            new_y = (np.arange(output_size.y) * output_scale.y).astype(np.float32)
            new_x = (np.arange(output_size.x) * output_scale.x).astype(np.float32)

            coordinates = np.moveaxis(np.meshgrid(new_z, new_y, new_x, indexing='ij', copy=False), 0, -1)

            # Evaluate.
            interpolator = scipy.interpolate.RegularGridInterpolator((z, y, x), data, bounds_error=False, fill_value=None)

            return interpolator(coordinates)
