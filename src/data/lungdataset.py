# Copyright (C) 2017  Aidence B.V.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA

from util.point import Point3D
from .lungmask import LungMaskCache

import numpy as np

import os
import pickle


class AbstractLungNodule:
    def get_start_index(self):
        """
        Returns start index of nodule in pixel space.
        """

        raise NotImplementedError()

    def get_end_index(self):
        """
        Returns end index (exclusive) of nodule in pixel space.
        """

        raise NotImplementedError()

    def get_index_size(self):
        """
        Returns size of nodule in pixel space.
        """

        return Point3D.from_zyx_list(
            self.get_end_index().get_as_zyx_array() - self.get_start_index().get_as_zyx_array())

    def get_start(self, in_world_space=True):
        """
        Returns start of nodule in pixel/millimeters world space.
        """

        raise NotImplementedError()

    def get_end(self, in_world_space=True):
        """
        Returns end of nodule in pixel/millimeters world space.
        """

        raise NotImplementedError()

    def get_size(self, in_world_space=True):
        """
        Returns size of nodule in pixel/millimeters world space.
        """

        return Point3D.from_zyx_list(
            self.get_end(in_world_space).get_as_zyx_array() - self.get_start(in_world_space).get_as_zyx_array())

    def get_center(self, in_world_space=True):
        """
        Returns center of nodule in pixel/millimeters world space.
        """

        raise NotImplementedError()

    def get_radius(self):
        """
        Returns radius of nodule in millimeters world space.
        """

        raise NotImplementedError()

    def is_included(self):
        return NotImplementedError()

    def is_excluded(self):
        return not self.is_included()

    def get_metadata(self):
        return NotImplementedError()

    def is_point_inside_bounding_box(self, point):
        """
        Checks whether point in millimeters world space is inside bounding box of nodule.
        """

        start = self.get_start()
        end = self.get_end()

        return start.z <= point.z <= end.z and \
               start.y <= point.y <= end.y and \
               start.x <= point.x <= end.x

    def is_point_inside_bounding_sphere(self, point):
        """
        Checks whether point in millimeters world space is inside bounding sphere of nodule.
        """

        center = self.get_center()
        radius = self.get_radius()

        return np.sum(np.square(center.get_as_zyx_array() - point.get_as_zyx_array())) < radius * radius

    def get_volume(self, in_world_space=True):
        """
        Returns volume of nodule in px^3/mm^3.
        """

        size = self.get_size(in_world_space)

        return size.z * size.y * size.x

    def get_distance(self, point):
        """
        Returns L2 distance in millimeters from nodule center to point in millimeters world space.
        """

        return np.linalg.norm(point.get_as_zyx_array() - self.get_center().get_as_zyx_array())


class AbstractLungScan:
    def get_volume(self):
        raise NotImplementedError()

    def get_nodules(self):
        """
        Returns list of lung nodules.
        """

        raise NotImplementedError()

    def get_mask(self):
        """
        Returns lung mask.
        """

        raise NotImplementedError()

    def get_offset(self):
        """
        Returns offset of scan in mm.
        """

        raise NotImplementedError()

    def get_scale(self):
        """
        Returns scale of scan in mm.
        """

        raise NotImplementedError()

    def get_included_nodules(self):
        return [nodule for nodule in self.get_nodules() if nodule.is_included()]

    def get_excluded_nodules(self):
        return [nodule for nodule in self.get_nodules() if nodule.is_excluded()]


class AbstractLungDataSet:
    """
    A data set consisting of lung scans.
    """

    def get_positive_scan_ids(self):
        """
        Returns a list of positive scan ids.
        """

        raise NotImplementedError()

    def get_negative_scan_ids(self):
        """
        Returns a list of negative scan ids.
        """

        raise NotImplementedError()

    def get_all_scan_ids(self):
        """
        Returns a list of all scan ids.
        """

        raise NotImplementedError()

    def get_scan(self, id):
        """
        Retrieves a scan by id.
        """

        raise NotImplementedError()


class LungNodule(AbstractLungNodule):
    default_size = 5.5

    def __init__(self, center, offset, scale, radius=None, size=None, included=True, metadata=None):
        # Determine size and radius.
        no_depth = size is None or size.z is None or size.z <= 0
        no_height = size is None or size.y is None or size.y <= 0
        no_width = size is None or size.x is None or size.x <= 0
        no_radius = radius is None or radius <= 0

        if no_depth and no_height and no_width:
            # We have nothing, fill in with radius or default size.
            default_size = LungNodule.default_size if no_radius else radius * 2

            size = Point3D(default_size, default_size, default_size)
        elif no_depth or no_height or no_width:
            # Fill in missing value with mean of existing sizes.
            existing_sizes = list(filter(lambda s: s is not None and s > 0, size.get_as_zyx_tuple()))

            mean_size = np.mean(existing_sizes)

            depth = mean_size if no_depth else size.z
            height = mean_size if no_height else size.y
            width = mean_size if no_width else size.x

            size = Point3D(depth, height, width)

        if no_radius:
            # Approximate radius by taking half the mean of dimensions.
            radius = 0.5 * np.mean(size.get_as_zyx_tuple())

        # Set members.
        self.world_center = center
        self.radius = radius
        self.included = included
        self.metadata = metadata if metadata is not None else {}

        # Determine world start and end.
        self.world_start = Point3D(
            center.z - size.z * 0.5,
            center.y - size.y * 0.5,
            center.x - size.x * 0.5
        )

        self.world_end = Point3D(
            center.z + size.z * 0.5,
            center.y + size.y * 0.5,
            center.x + size.x * 0.5
        )

        # Determine pixel space start, end and center.
        self.start = Point3D.from_zyx_list(
            (self.world_start.get_as_zyx_array() - offset.get_as_zyx_array()) / scale.get_as_zyx_array())

        self.end = Point3D.from_zyx_list(
            (self.world_end.get_as_zyx_array() - offset.get_as_zyx_array()) / scale.get_as_zyx_array())

        self.center = Point3D.from_zyx_list(
            (self.world_center.get_as_zyx_array() - offset.get_as_zyx_array()) / scale.get_as_zyx_array())

        # Determine start and end indices.
        self.start_index = Point3D(
            int(round(self.start.z + 0.5)),
            int(round(self.start.y + 0.5)),
            int(round(self.start.x + 0.5))
        )

        self.end_index = Point3D(
            self.start_index.z + int(round(size.z / scale.z)),
            self.start_index.y + int(round(size.y / scale.y)),
            self.start_index.x + int(round(size.x / scale.x))
        )

    def get_start_index(self):
        return self.start_index

    def get_end_index(self):
        return self.end_index

    def get_center(self, in_world_space=True):
        return self.world_center if in_world_space else self.center

    def get_start(self, in_world_space=True):
        return self.world_start if in_world_space else self.start

    def get_end(self, in_world_space=True):
        return self.world_end if in_world_space else self.end

    def get_radius(self):
        return self.radius

    def is_included(self):
        return self.included

    def get_metadata(self):
        return self.metadata


class LungScan(AbstractLungScan):
    def __init__(self, data_set, id, metadata):
        self.data_set = data_set
        self.id = id
        self.metadata = metadata
        self.nodules = None
        self.volume = None
        self.mask = None

    def get_volume(self):
        if self.volume is None:
            self.volume = self.data_set.load_volume(self.id)

        return self.volume

    def get_mask(self):
        if self.mask is None:
            self.mask = self.data_set.load_mask(self.id, self.get_volume(), self.get_scale())

        return self.mask

    def get_metadata(self):
        return self.metadata

    def get_nodules(self):
        if self.nodules is None:
            # Create nodules.
            def nodule_from_info(info, included):
                return LungNodule(info['center'], self.metadata['offset'], self.metadata['scale'],
                                  size=info.get('size', None), radius=info.get('radius', None),
                                  included=included, metadata=info)

            self.nodules = list(map(lambda info: nodule_from_info(info, True), self.metadata['included-annotations'])) + \
                           list(map(lambda info: nodule_from_info(info, False), self.metadata['excluded-annotations']))

        return self.nodules

    def get_offset(self):
        return self.metadata['offset']

    def get_scale(self):
        return self.metadata['scale']

    def get_uid(self):
        return self.metadata['series-uid']


class LungDataSet(AbstractLungDataSet):
    def __init__(self, data_directory):
        # Set members.
        self.data_directory = data_directory

        # Read info.
        with open(os.path.join(self.data_directory, 'dataset.pkl'), 'rb') as f:
            self.metadata = pickle.load(f)['scans']

        # Get scan ids.
        positive_ids = set()
        negative_ids = set()
        for id, scan_info in self.metadata.items():
            if scan_info['included-annotations'] or scan_info['excluded-annotations']:
                positive_ids.add(id)
            else:
                negative_ids.add(id)

        # Set sorted ids.
        self.negative_ids = sorted(negative_ids)
        self.positive_ids = sorted(positive_ids)

        # Create mask cache.
        self.mask_cache = LungMaskCache(os.path.join(self.data_directory, 'mask'))

    def get_positive_scan_ids(self):
        return self.positive_ids

    def get_negative_scan_ids(self):
        return self.negative_ids

    def get_all_scan_ids(self):
        return self.positive_ids + self.negative_ids

    def get_scan(self, id):
        return LungScan(self, id, self.metadata[id])

    def load_volume(self, id):
        if 'directory' in self.metadata[id]:
            # Custom directory structure.
            file_name = os.path.join(self.data_directory, 'data',
                                     self.metadata[id]['directory'], self.metadata[id]['file-name'])
        else:
            # Use just file name.
            file_name = os.path.join(self.data_directory, 'data', self.metadata[id]['file-name'])

        return np.load(file_name)['data']

    def load_mask(self, id, volume, scale):
        mask = self.mask_cache.get(id, volume=volume, scale=scale)

        # Check whether volume matches mask.
        if mask.get_mask().shape != volume.shape:
            self.mask_cache.invalidate(id)

            mask = self.mask_cache.get(id)

        return mask
