# Copyright (C) 2017  Aidence B.V.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA

import cv2
import numpy as np
from scipy import ndimage as nd

from util.cache import Cache
from util.point import Point3D


class LungMask:
    def __init__(self, volume=None, scale=None, mask=None, start=None, end=None):
        if volume is None and scale is None:
            self.mask = mask
            self.start = start
            self.end = end
        else:
            self.mask = LungMask.calculate_mask(volume, scale)
            self.start, self.end = LungMask.calculate_bounding_box(self.mask)

    def is_lung(self, point):
        index = np.round(point.get_as_zyx_array()).astype(np.uint)

        return self.mask[index[0], index[1], index[2]]

    def get_mask(self):
        return self.mask

    def get_start(self):
        """
        Returns bounding box start.
        """
        return self.start

    def get_end(self):
        """
        Returns bounding box end (exclusive).
        """
        return self.end

    def get_size(self):
        """
        Returns bounding box size.
        """
        return Point3D.from_zyx_list(self.end.get_as_zyx_array() - self.start.get_as_zyx_array())

    @staticmethod
    def load(file_name):
        content = np.load(file_name)

        min_z, min_y, min_x, max_z, max_y, max_x = content['bb']

        start = Point3D(min_z, min_y, min_x)
        end = Point3D(max_z + 1, max_y + 1, max_x + 1)

        mask = content['mask']

        if mask.dtype != np.bool:
            mask = (mask > 0)

        return LungMask(mask=mask, start=start, end=end)

    def save(self, file_name):
        bounding_box = (self.start.z, self.start.y, self.start.x, self.end.z - 1, self.end.y - 1, self.end.x - 1)

        np.savez_compressed(file_name, mask=self.mask, bb=bounding_box)

    @staticmethod
    def calculate_mask(volume, scale):
        # Blur volume.
        volume = LungMask.blur_volume(volume)

        # Threshold it.
        mask = LungMask.threshold_volume(volume)

        # Fill background.
        mask = LungMask.fill_background(mask)

        # Expand mask.
        mask = LungMask.expand_mask(mask, scale)

        # Remove small artifacts.
        mask = LungMask.remove_artifacts(mask)

        # Close convexity defects.
        mask = LungMask.close_convexity_defects(mask, scale)

        # Fill holes.
        mask = LungMask.fill_holes(mask)

        # Make boolean.
        mask = (mask > 0)

        return mask

    @staticmethod
    def blur_volume(volume):
        # Copy volume.
        volume = volume.copy()

        # Blur slices with median blur.
        for z in range(volume.shape[0]):
            volume[z] = cv2.medianBlur(volume[z], 11)

        return volume

    @staticmethod
    def threshold_volume(volume):
        # Map as follows:
        # [0, t] = 255
        # [t + 1, 255] = 0
        #
        # Threshold at approximately -543 HU.

        threshold = 90

        volume[volume <= threshold] = 0
        volume[volume > threshold] = 255

        volume = 255 - volume

        return volume

    @staticmethod
    def fill_background(mask):
        # Create an inverted circle mask.
        inverted_circle_mask = np.zeros(mask.shape[1:], dtype=np.uint8)
        cv2.circle(inverted_circle_mask, (mask.shape[1] // 2, mask.shape[2] // 2), mask.shape[1] // 2, 255, -1)
        inverted_circle_mask = 255 - inverted_circle_mask

        for z in range(mask.shape[0]):
            z_mask = mask[z]

            # Turn everything outside circle on.
            z_mask |= inverted_circle_mask

            # turn background black, fill from the corners
            cv2.floodFill(z_mask, None, (0, 0), 0)
            cv2.floodFill(z_mask, None, (0, z_mask.shape[0] - 1), 0)
            cv2.floodFill(z_mask, None, (z_mask.shape[1] - 1, 0), 0)
            cv2.floodFill(z_mask, None, (z_mask.shape[1] - 1, z_mask.shape[0] - 1), 0)

            mask[z] = z_mask

        return mask

    @staticmethod
    def expand_mask(mask, scale):
        # Determine first kernel size (7.68 mm x 7.68 mm).
        first_kernel_x_size = int(np.round(15 * 0.512 / scale.x))
        first_kernel_y_size = int(np.round(15 * 0.512 / scale.y))

        # Determine second kernel size (1.536 mm x 7.5 mm).
        second_kernel_x_size = int(np.round(3 * 0.512 / scale.x))
        second_kernel_y_size = int(np.round(3 * 0.512 / scale.y))
        second_kernel_z_size = int(np.round(3 * 2.5 / scale.z))

        # Determine max size (7.86 cm^2).
        max_size = 3000 * 0.512 * 0.512 / (scale.x * scale.y)

        # Dilate XY planes.
        kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (first_kernel_x_size, first_kernel_y_size))
        for z in range(mask.shape[0]):
            z_mask = mask[z]

            # Expand the mask area a bit so that we get the lung borders as well.
            z_mask = cv2.dilate(z_mask, kernel, iterations=1)

            # Anything other than the first label is an inclusion, get rid of it.
            labels, num_labels = nd.label(z_mask)

            for n in range(1, num_labels + 1):
                # Label can't be too large.
                if np.sum(labels == n) < max_size:
                    z_mask[labels == n] = 0

            mask[z] = z_mask

        # Dilate XZ planes.
        kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (second_kernel_z_size, second_kernel_x_size))
        for y in range(mask.shape[1]):
            mask[:, y, :] = cv2.dilate(mask[:, y, :], kernel, iterations=1)

        # Dilate YZ planes.
        kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (second_kernel_z_size, second_kernel_y_size))
        for x in range(mask.shape[2]):
            mask[:, :, x] = cv2.dilate(mask[:, :, x], kernel, iterations=1)

        return mask

    @staticmethod
    def remove_artifacts(mask):
        # Create labels on 3D mask, there should be only 1.
        labels, num_labels = nd.label(mask)

        # Anything other than the first label is an artifact, remove.
        if num_labels > 1:
            sizes_and_labels = list(zip(np.bincount(labels.flatten())[1:], list(range(1, num_labels + 1))))
            sizes_and_labels.sort(key=lambda x: x[0], reverse=True)

            for i in range(1, len(sizes_and_labels)):
                if sizes_and_labels[i][0] < 0.5 * sizes_and_labels[0][0]:
                    # Too small, must be an artifact.
                    # Only one lung segment should be larger than this.
                    mask[labels == sizes_and_labels[i][1]] = 0

        return mask

    @staticmethod
    def close_convexity_defects(mask, scale):
        # TODO: Check whether this has any effect at all with such a large minimum depth.

        # Determine minimum depth (51.2 cm) !?
        min_depth = 1000 * 0.512 / scale.x

        # Determine maximum distance (40.96 mm).
        max_distance = 80 * 0.512 / scale.x

        # Find any convexity defects and close them if they are small enough.
        for z in range(mask.shape[0]):
            _, contours, _ = cv2.findContours(mask[z].copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

            for contour in contours:
                convex_hull = cv2.convexHull(contour, returnPoints=False)
                defects = cv2.convexityDefects(contour, convex_hull)

                if defects is not None:
                    for i in range(defects.shape[0]):
                        start_index, end_index, _, depth = defects[i, 0]

                        start = tuple(contour[start_index][0])
                        end = tuple(contour[end_index][0])

                        # Start and end indices must not be too close, and the defect depth must be large enough.
                        if end_index - start_index > 10 and depth > min_depth:
                            distance = np.sqrt((start[0] - end[0]) ** 2 + (start[1] - end[1]) ** 2)

                            # The defect points must be this close together.
                            if distance < max_distance:
                                # Draw a line to close off the convexity defect.
                                cv2.line(mask[z], start, end, 255, 1)

        return mask

    @staticmethod
    def fill_holes(mask):
        # Fill holes, existing ones and closings we just introduced to fill convexity defects.

        # Make mask binary.
        mask[mask > 0] = 1

        for z in range(mask.shape[0]):
            m = nd.binary_fill_holes(mask[z])

            mask[z] = m.astype(np.uint8) * 255

        return mask

    @staticmethod
    def calculate_bounding_box(mask):
        z, y, x = np.where(mask)

        start = Point3D(np.min(z), np.min(y), np.min(x))
        end = Point3D(np.max(z) + 1, np.max(y) + 1, np.max(x) + 1)

        return start, end


class LungMaskCache(Cache):
    def __init__(self, directory):
        super().__init__(directory, LungMask, 'npz')
