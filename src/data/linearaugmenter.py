# Copyright (C) 2017  Aidence B.V.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA

import model

import numpy as np
import cv2


class LinearAugmenter(model.DataProvider):
    """
    Augments data provider with linear transformations.
    """

    def __init__(self, data_provider, input_width, input_height, output_width, output_height,
                 scale_min=0.9, scale_max=1.11111, noise_std_dev=0.0, just_positives=False, seed=42):
        self.data_provider = data_provider

        self.input_width = input_width
        self.input_height = input_height

        self.output_width = output_width
        self.output_height = output_height

        self.log_scale_min = np.log(scale_min)
        self.log_scale_max = np.log(scale_max)

        self.noise_std_dev = noise_std_dev

        self.just_positives = just_positives

        # Create random state.
        self.random_state = np.random.RandomState(seed)

    def get_mini_batch_input_shape(self):
        batch_size, depth, _, _, num_channels = self.data_provider.get_mini_batch_input_shape()

        return batch_size, depth, self.input_height, self.input_width, num_channels

    def get_mini_batch_output_shape(self):
        batch_size, depth, _, _, num_channels = self.data_provider.get_mini_batch_output_shape()

        return batch_size, depth, self.output_height, self.output_width, num_channels

    def get_mini_batch(self, size):
        inputs, outputs, weights = self.data_provider.get_mini_batch(size)

        return self.augment(inputs, outputs, weights)

    def get_everything(self, size):
        raise NotImplementedError()

    def augment(self, inputs, outputs, weights):
        # Get sizes.
        batch_size, input_depth, input_height, input_width, input_channels = inputs.shape
        _, output_depth, output_height, output_width, output_channels = outputs.shape

        # Do not rotate/scale if output is singular.
        output_tranform = (output_height != 1 or output_width != 1)

        # Get offsets/shapes of warped data.
        input_x_offset = (input_width - self.input_width) // 2
        input_y_offset = (input_height - self.input_height) // 2

        output_x_offset = (output_width - self.output_width) // 2
        output_y_offset = (output_height - self.output_height) // 2

        input_shape = (self.input_width + input_x_offset, self.input_height + input_y_offset)
        output_shape = (self.output_width + output_x_offset, self.output_height + output_y_offset)

        # Create new arrays.
        new_inputs = np.empty((batch_size, input_depth, self.input_height, self.input_width, input_channels),
                              dtype=np.float32)
        new_outputs = np.empty((batch_size, output_depth, self.output_height, self.output_width, output_channels),
                               dtype=np.float32)
        new_weights = np.empty((batch_size, output_depth, self.output_height, self.output_width, 1), dtype=np.float32)

        # Transform.
        for i in range(batch_size):
            # Determine whether to skip.
            skip = False
            if self.just_positives:
                if output_channels == 1:
                    if not np.any(outputs[i] > 0.5):
                        skip = True
                elif not np.any(np.argmax(outputs[i], axis=3) != 0):
                    skip = True

            if skip:
                new_inputs[i] = inputs[i, :, input_y_offset:input_shape[1], input_x_offset:input_shape[0], :]
                new_outputs[i] = outputs[i, :, output_y_offset:output_shape[1], output_x_offset:output_shape[0], :]
                new_weights[i] = weights[i, :, output_y_offset:output_shape[1], output_x_offset:output_shape[0], :]

                continue

            # Determine rotation and scale.
            rotation = self.random_state.rand() * 360
            log_scale = self.random_state.rand() * (self.log_scale_max - self.log_scale_min) + self.log_scale_min
            scale = np.exp(log_scale)

            # Create matrices.
            input_matrix = cv2.getRotationMatrix2D((input_width * 0.5, input_height * 0.5), rotation, scale)
            output_matrix = cv2.getRotationMatrix2D((output_width * 0.5, output_height * 0.5), rotation, scale)

            # Rotate inputs.
            for j in range(input_depth):
                for k in range(input_channels):
                    warped = cv2.warpAffine(inputs[i, j, :, :, k], input_matrix, input_shape, flags=cv2.INTER_CUBIC)

                    new_inputs[i, j, :, :, k] = warped[input_y_offset:, input_x_offset:]

            # Rotate outputs and weights.
            if output_tranform:
                for j in range(output_depth):
                    warped = cv2.warpAffine(weights[i, j, :, :, 0], output_matrix, output_shape, flags=cv2.INTER_LINEAR)

                    new_weights[i, j, :, :, 0] = warped[output_y_offset:, output_x_offset:]

                    for k in range(output_channels):
                        warped = cv2.warpAffine(outputs[i, j, :, :, k], output_matrix, output_shape, flags=cv2.INTER_LINEAR)

                        new_outputs[i, j, :, :, k] = warped[output_y_offset:, output_x_offset:]
            else:
                # Just copy single value.
                new_weights[i] = weights[i]
                new_outputs[i] = outputs[i]

            # Generate mirror group Z_2 over x.
            if self.random_state.rand() > 0.5:
                new_inputs[i] = new_inputs[i, :, :, ::-1, :]
                new_outputs[i] = new_outputs[i, :, :, ::-1, :]
                new_weights[i] = new_weights[i, :, :, ::-1, :]

            # Generate mirror group Z_2 over y.
            if self.random_state.rand() > 0.5:
                new_inputs[i] = new_inputs[i, :, ::-1, :, :]
                new_outputs[i] = new_outputs[i, :, ::-1, :, :]
                new_weights[i] = new_weights[i, :, ::-1, :, :]

            # Generate mirror group Z_2 over z.
            if self.random_state.rand() > 0.5:
                new_inputs[i] = new_inputs[i, ::-1, :, :, :]
                new_outputs[i] = new_outputs[i, ::-1, :, :, :]
                new_weights[i] = new_weights[i, ::-1, :, :, :]

        # Add Gaussian noise.
        if self.noise_std_dev != 0.0:
            new_inputs += self.random_state.randn(*new_inputs.shape) * self.noise_std_dev

        return new_inputs, new_outputs, new_weights
