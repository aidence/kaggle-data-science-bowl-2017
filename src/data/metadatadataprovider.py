# Copyright (C) 2017  Aidence B.V.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA

import model
import numpy as np

import gzip
import pickle

import collections
import logging


class MetadataDataProvider(model.DataProvider):
    """
    Training data provider based on patches.
    """

    def __init__(self, file_name, test, name, seed=42):
        # Set members.
        self.test = test

        # Load data.
        with gzip.open(file_name, 'rb') as f:
            self.data = pickle.load(f)

        prefix = 'test' if test else 'train'

        self.inputs = self.data[prefix + '-positive-inputs']

        # Reshape to 5D.
        self.inputs = np.expand_dims(self.inputs, -1)

        # Create random state.
        self.random_state = np.random.RandomState(seed)

        # Create outputs.
        self.outputs, indices, self.num_labels = self.create_outputs(name)

        # Sample inputs.
        self.inputs = self.inputs[indices]

        # Set indices and index.
        self.indices = list(range(len(self.inputs)))
        self.index = 0

        # Create weights.
        self.positive_weights = np.ones((len(self.inputs), 1, 1, 1, 1), dtype=np.float32)

    def get_mini_batch_input_shape(self):
        return (None,) + self.inputs.shape[1:]

    def get_mini_batch_output_shape(self):
        return None, 1, 1, 1, self.num_labels

    def get_mini_batch(self, size):
        # Get indices.
        positive_indices = self.get_indices(size)

        # Get data.
        inputs = self.inputs[positive_indices]
        outputs = self.outputs[positive_indices]
        weights = self.positive_weights[positive_indices]

        inputs = (inputs - np.float32(127.5)) * np.float32(1 / 127.5)

        # Shuffle it so statistics of splits are not biased.
        shuffle_indices = self.random_state.permutation(size)

        inputs = inputs[shuffle_indices]
        outputs = outputs[shuffle_indices]
        weights = weights[shuffle_indices]

        return inputs, outputs, weights

    def get_everything(self, size):
        raise NotImplementedError()

    def create_outputs(self, name):
        # Determine prefix.
        prefix = 'test' if self.test else 'train'

        # Get metadata values.
        all_values = [metadata.get(name, None) for metadata in self.data[prefix + '-positive-metadata']]

        # Get indices.
        indices = [i for i, values in enumerate(all_values) if values is not None]
        if not indices:
            raise Exception('No values found.')

        # Check name.
        min_value = 1
        max_value = 5
        num_labels = 1
        if name == 'type':
            # Continuous: 'ground-glass', 'mixed', 'soft-tissue'.
            raise NotImplementedError()
        elif name == 'subtlety':
            # Continuous: 1 - Extremely Subtle -- 5 - Obvious.
            pass
        elif name == 'structure':
            # Discrete: 1 - Soft Tissue (Default), 2 - Fluid, 3 - Fat, 4 - Air.

            # Completely useless now as distribution of 97% of LIDC is:
            # 1: 1143
            # 2: 2
            # 3: 0
            # 4: 2

            raise NotImplementedError()
        elif name == 'calcification':
            # Discrete: 1 - Popcorn, 2 - Laminated, 3 - Solid, 4 - Non-Central, 5 - Central, 6 - Absent.

            # Distribution of 97% of LIDC is:
            # 1: 0
            # 2: 2
            # 3: 105
            # 4: 14
            # 5: 15
            # 6: 1011
            #
            # So select only 3 and 6.

            # TODO: Remove bias towards lower numbers.

            # Get medians and map values to {0, 1}.
            medians = [int(np.sort(all_values[i])[len(all_values[i]) // 2]) for i in indices]
            all_values = [1.0 if median == 3 else 0.0 for median in medians]

            min_value = 0
            max_value = 1

            # Select indices that have median 3 or 6.
            indices = [index for median, index in zip(medians, indices) if median in (3, 6)]
        elif name == 'sphericity':
            # Continuous: 1 - Linear -- 5 - Round.
            pass
        elif name == 'margin':
            # Continuous: 1 - Poorly -- 5 - Sharp.
            pass
        elif name == 'lobulation':
            # Continuous: 1 - None -- 5 - Marked.
            pass
        elif name == 'spiculation':
            # Continuous: 1 - None -- 5 - Marked.
            pass
        elif name == 'texture':
            # Continuous: 1 - Non-Solid/Ground Glass Opacity -- 5 - Solid.
            pass
        elif name == 'malignancy':
            # Continuous: 1 - Highly Unlikely for Cancer -- 5 - Highly Suspicious for Cancer.
            pass
        elif name == 'radius':
            min_value = 0
            max_value = 15
        else:
            raise NotImplementedError()

        if num_labels != 1:
            # TODO: Remove bias towards lower numbers.
            # TODO: Not actually used anymore.

            # Discrete: get median values (minus one).
            medians = [int(np.median(all_values[i])) - 1 for i in indices]

            # Show amounts.
            counts = collections.Counter(medians).most_common()

            total = sum([count[1] for count in counts])

            logging.info('Metadata value count:')
            for value, count in counts:
                logging.info('{}: {} ({:%}).'.format(value + 1, count, count / total))

            # Create one-hot encoding.
            outputs = np.zeros((len(medians), 1, 1, 1, num_labels), dtype=np.float32)
            outputs[np.arange(len(medians)), 0, 0, 0, medians] = 1
        else:
            # Continous: rescale and clip.
            outputs = [np.clip((np.mean(all_values[i]) - min_value) / (max_value - min_value), 0, 1) for i in indices]
            outputs = np.array(outputs, dtype=np.float32).reshape((len(outputs), 1, 1, 1, 1))

        return outputs, indices, num_labels

    def get_indices(self, num):
        # Get indices array.
        all_indices = self.indices

        # Draw indices.
        indices = all_indices[self.index:self.index + num]
        self.index += num

        while len(indices) < num:
            # Not enough, reshuffle indices.
            self.random_state.shuffle(all_indices)

            # Add more examples.
            num_left = num - len(indices)

            indices += all_indices[0:num_left]

            # Reset index.
            self.index = num_left

        return indices
