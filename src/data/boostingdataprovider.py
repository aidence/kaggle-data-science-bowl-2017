# Copyright (C) 2017  Aidence B.V.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA

import model
import numpy as np

import gzip
import pickle


class BoostingDataProvider(model.DataProvider):
    """
    Training data provider based on patches.
    """

    def __init__(self, file_name, test, negative_weight=2, use_types=False, seed=42):
        # Set members.
        self.test = test
        self.negative_weight = negative_weight

        # Load data.
        with gzip.open(file_name, 'rb') as f:
            self.data = pickle.load(f)

        prefix = 'test' if test else 'train'

        self.positive_inputs = self.data[prefix + '-positive-inputs']
        self.negative_inputs = self.data[prefix + '-negative-inputs']

        # Reshape to 5D.
        self.positive_inputs = np.expand_dims(self.positive_inputs, -1)
        self.negative_inputs = np.expand_dims(self.negative_inputs, -1)

        self.positive_indices = list(range(len(self.positive_inputs)))
        self.negative_indices = list(range(len(self.negative_inputs)))

        self.positive_index = 0
        self.negative_index = 0

        # Create output arrays.
        if use_types:
            raise NotImplementedError()
        else:
            self.num_labels = 1

            self.positive_outputs = np.ones((len(self.positive_inputs), 1, 1, 1, 1), dtype=np.float32)
            self.negative_outputs = np.zeros((len(self.negative_inputs), 1, 1, 1, 1), dtype=np.float32)

        # Create weights.
        self.positive_weights = np.ones((len(self.positive_inputs), 1, 1, 1, 1), dtype=np.float32)
        self.negative_weights = np.ones((len(self.negative_inputs), 1, 1, 1, 1), dtype=np.float32)

        # Correct weights.
        self.positive_weights *= 2 / (self.negative_weight + 1)
        self.negative_weights *= 2 * self.negative_weight / (self.negative_weight + 1)

        # Create random state.
        self.random_state = np.random.RandomState(seed)

    def get_mini_batch_input_shape(self):
        return (None,) + self.positive_inputs.shape[1:]

    def get_mini_batch_output_shape(self):
        return None, 1, 1, 1, self.num_labels

    def get_mini_batch(self, size):
        # Get amounts.
        positive_num = size // 2
        negative_num = size - positive_num

        # Get indices.
        positive_indices = self.get_indices(positive_num, positive=True)
        negative_indices = self.get_indices(negative_num, positive=False)

        # Get data.
        positive_inputs = self.positive_inputs[positive_indices]
        positive_outputs = self.positive_outputs[positive_indices]
        positive_weights = self.positive_weights[positive_indices]

        negative_inputs = self.negative_inputs[negative_indices]
        negative_outputs = self.negative_outputs[negative_indices]
        negative_weights = self.negative_weights[negative_indices]

        inputs = np.concatenate((positive_inputs, negative_inputs), axis=0)
        outputs = np.concatenate((positive_outputs, negative_outputs), axis=0)
        weights = np.concatenate((positive_weights, negative_weights), axis=0)

        inputs = (inputs - np.float32(127.5)) * np.float32(1 / 127.5)

        # Shuffle it so statistics of splits are not biased.
        shuffle_indices = self.random_state.permutation(size)

        inputs = inputs[shuffle_indices]
        outputs = outputs[shuffle_indices]
        weights = weights[shuffle_indices]

        return inputs, outputs, weights

    def get_everything(self, size):
        raise NotImplementedError()

    def get_indices(self, num, positive):
        # Get indices array.
        all_indices = self.positive_indices if positive else self.negative_indices

        # Draw indices.
        if positive:
            indices = all_indices[self.positive_index:self.positive_index + num]
            self.positive_index += num
        else:
            indices = all_indices[self.negative_index:self.negative_index + num]
            self.negative_index += num

        while len(indices) < num:
            # Not enough, reshuffle indices.
            self.random_state.shuffle(all_indices)

            # Add more examples.
            num_left = num - len(indices)

            indices += all_indices[0:num_left]

            # Reset index.
            if positive:
                self.positive_index = num_left
            else:
                self.negative_index = num_left

        return indices
