# Copyright (C) 2017  Aidence B.V.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA

import model


class CenterCrop(model.DataProvider):
    """
    Creates a center crop of data.
    """

    def __init__(self, data_provider, input_width=None, input_height=None, input_depth=None,
                 output_width=None, output_height=None, output_depth=None):
        self.data_provider = data_provider

        self.input_width = input_width
        self.input_height = input_height
        self.input_depth = input_depth

        self.output_width = output_width
        self.output_height = output_height
        self.output_depth = output_depth

    def get_mini_batch_input_shape(self):
        batch_size, depth, height, width, num_channels = self.data_provider.get_mini_batch_input_shape()

        depth = self.input_depth if self.input_depth is not None else depth
        height = self.input_height if self.input_height is not None else height
        width = self.input_width if self.input_width is not None else width

        return batch_size, depth, height, width, num_channels

    def get_mini_batch_output_shape(self):
        batch_size, depth, height, width, num_channels = self.data_provider.get_mini_batch_output_shape()

        depth = self.output_depth if self.output_depth is not None else depth
        height = self.output_height if self.output_height is not None else height
        width = self.output_width if self.output_width is not None else width

        return batch_size, depth, height, width, num_channels

    def get_mini_batch(self, size):
        inputs, outputs, weights = self.data_provider.get_mini_batch(size)

        return self.crop(inputs, outputs, weights)

    def get_everything(self, size):
        for inputs, outputs, weights in self.data_provider.get_everything(size):
            yield self.crop(inputs, outputs, weights)

    def crop(self, inputs, outputs, weights):
        # Get sizes.
        _, input_depth, input_height, input_width, _ = inputs.shape
        _, output_depth, output_height, output_width, _ = outputs.shape

        # Get offsets.
        input_x_start = (input_width - self.input_width) // 2 if self.input_width is not None else 0
        input_y_start = (input_height - self.input_height) // 2 if self.input_height is not None else 0
        input_z_start = (input_depth - self.input_depth) // 2 if self.input_depth is not None else 0

        input_x_end = input_x_start + self.input_width if self.input_width is not None else None
        input_y_end = input_y_start + self.input_height if self.input_height is not None else None
        input_z_end = input_z_start + self.input_depth if self.input_depth is not None else None

        output_x_start = (output_width - self.output_width) // 2 if self.output_width is not None else 0
        output_y_start = (output_height - self.output_height) // 2 if self.output_height is not None else 0
        output_z_start = (output_depth - self.output_depth) // 2 if self.output_depth is not None else 0

        output_x_end = output_x_start + self.output_width if self.output_width is not None else None
        output_y_end = output_y_start + self.output_height if self.output_height is not None else None
        output_z_end = output_z_start + self.output_depth if self.output_depth is not None else None

        # Crop.
        inputs = inputs[:, input_z_start:input_z_end, input_y_start:input_y_end, input_x_start:input_x_end, :]
        outputs = outputs[:, output_z_start:output_z_end, output_y_start:output_y_end, output_x_start:output_x_end, :]
        weights = weights[:, output_z_start:output_z_end, output_y_start:output_y_end, output_x_start:output_x_end, :]

        return inputs, outputs, weights
