# Copyright (C) 2017  Aidence B.V.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA

import os
import csv
import argparse
import pickle

from util.util import Util

import numpy as np


def read_label_csv(file_name):
    labels = {}
    with open(file_name, 'r') as f:
        reader = csv.reader(f)

        next(reader)

        for row in reader:
            id, cancer = row

            cancer = bool(int(cancer))

            labels[id] = cancer

    return labels


def get_matching_series(data_directory, labels):
    result = []

    for series in os.listdir(data_directory):
        series_directory = os.path.join(data_directory, series)
        if not os.path.isdir(series_directory):
            continue

        result.append((series, series_directory, labels.get(series, None)))

    return result


def worker_process_main(series_queue, scan_queue, output_data_directory, force, scale, full_precision):
    import data

    import queue
    import traceback

    # Process series.
    dicom_reader = data.DicomReader()

    while True:
        # Try to get a series.
        try:
            id, series_directory, label = series_queue.get_nowait()
        except queue.Empty:
            break

        try:
            # Determine output file names.
            data_file_name = os.path.join(output_data_directory, '{}.npz'.format(id))
            metadata_file_name = os.path.join(output_data_directory, '{}-metadata.pkl'.format(id))

            # Skip if they exists.
            if not force and os.path.exists(data_file_name) and os.path.exists(metadata_file_name):
                # Get scan info.
                with open(metadata_file_name, 'rb') as f:
                    metadata = pickle.load(f)

                # Check scale.
                if scale.get_as_zyx_tuple() != metadata['scale'].get_as_zyx_tuple():
                    raise Exception('Found existing scan with different scale: {}.'.format(id))

                # Put them in queue.
                scan_queue.put((id, metadata, label))

                continue

            # Read dicom.
            try:
                dicom = dicom_reader.read_series(series_directory, minimum_depth=0, maximum_z_spacing=np.inf,
                                                 maximum_xy_spacing=np.inf, output_scale=scale, check_extension=True,
                                                 source='kaggle')
            except data.DicomRejectedException as e:
                print('Scan {} was rejected. {}'.format(id, str(e)))
                continue

            # Save data.
            if full_precision:
                dicom.save_data_as_int16(data_file_name)
            else:
                dicom.save_data_as_uint8(data_file_name)

            dicom.save_metadata(metadata_file_name)

            # Put them in queue.
            scan_queue.put((id, dicom.get_metadata(), label))
        except KeyboardInterrupt:
            raise
        except:
            traceback.print_exc()

            print('Processing scan {} failed.'.format(id))
            continue


def accumulator_process_main(scan_queue, num_entries, output_directory, annotations):
    scans = {}
    while True:
        # Get an entry.
        scan = scan_queue.get()

        # Check whether we are done.
        if scan is None:
            break

        # Get scan info.
        id, metadata, label = scan

        # Skip annotations if test scan.
        if label is None:
            included_annotations = []
        else:
            def convert_annotation(annotation):
                texture = annotation.get('type', None)

                if texture == 'solid':
                    texture = [5]
                elif texture == 'partsolid':
                    texture = [3]
                elif texture == 'groundglass':
                    texture = [1]
                elif texture is None:
                    texture = None
                else:
                    raise Exception('Unknown annotation type.')

                return {'radius': annotation['diameter'] * 0.5, 'center': annotation['center'], 'texture': texture}

            included_annotations = [convert_annotation(annotation)
                                    for annotation in annotations.get(id, []) if annotation.get('is_nodule', True)]

        # Get annotations.
        metadata['included-annotations'] = included_annotations
        metadata['excluded-annotations'] = []
        metadata['label'] = label
        metadata['file-name'] = '{}.npz'.format(id)

        # Set scan data.
        scans[id] = metadata

        # Show progress.
        print('{}/{} ({:.1%}).'.format(len(scans), num_entries, len(scans) / num_entries))

    # Write dataset.
    dataset = {'type': 'dataset', 'scans': scans}

    with open(os.path.join(output_directory, 'dataset.pkl'), 'wb') as f:
        pickle.dump(dataset, f)


def main():
    # Parse command line options.
    parser = argparse.ArgumentParser(description='Write Kaggle data-set.',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('annotations_file_name', metavar='annotations-file-name', type=str, help='Annotation file name.')
    parser.add_argument('data_directory', metavar='data-directory', type=str, help='Data directory.')
    parser.add_argument('label_file_name', metavar='label-file-name', type=str, help='Label file name.')
    parser.add_argument('output_directory', metavar='output-directory', type=str, help='Output directory.')
    parser.add_argument('--scale', metavar='scale', type=str, default='2.5x0.512x0.512', help='Output scale.')
    parser.add_argument('--force', action='store_true', default=False, help='Overwrite existing entries.')
    parser.add_argument('--full-precision', action='store_true', default=False,
                        help='Store full int16 precision. Otherwise data is saved as uint8.')
    parser.add_argument('--num-processes', metavar='num-processes', type=int, help='Number of processes to use.')

    args = parser.parse_args()

    # Get scale.
    scale = Util.parse_3d_point(args.scale, dtype=float)

    print('Using scale: {}x{}x{}.'.format(scale.z, scale.y, scale.x))

    # Read annotations.
    with open(args.annotations_file_name, 'rb') as f:
        annotations = pickle.load(f)

    # Read labels.
    labels = read_label_csv(os.path.join(args.label_file_name))

    print('Found {} labels.'.format(len(labels)))

    # Create output directory.
    output_data_directory = os.path.join(args.output_directory, 'data')
    os.makedirs(output_data_directory, exist_ok=True)

    # Get matching series.
    series = get_matching_series(args.data_directory, labels)

    print('Found {} matching series.'.format(len(series)))

    # Distribute it.
    Util.distribute_cpu_work(series, worker_process_main, accumulator_process_main,
                             num_processes=args.num_processes,
                             worker_extra_args=(output_data_directory, args.force, scale, args.full_precision),
                             accumulator_extra_args=(args.output_directory, annotations))


if __name__ == '__main__':
    main()
