# Copyright (C) 2017  Aidence B.V.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA

import argparse
import yaml
import os
import shutil
import datetime
import logging
import traceback

import model
from util.util import Util


def setup_logger(log_file_name):
    logger = logging.getLogger('')
    logger.setLevel(logging.INFO)

    file_handler = logging.FileHandler(log_file_name)
    file_handler.setLevel(logging.INFO)

    stream_handler = logging.StreamHandler()
    stream_handler.setLevel(logging.INFO)

    formatter = logging.Formatter('%(asctime)s %(levelname)s: %(message)s')
    file_handler.setFormatter(formatter)
    stream_handler.setFormatter(formatter)

    logger.addHandler(file_handler)
    logger.addHandler(stream_handler)


def create_data_providers(config, metadata):
    # Create train and test data providers.
    extra_parameters = dict(metadata=metadata)

    train_data_provider = Util.process_config_value(config['train-data-provider'], extra_parameters)

    if 'test-data-provider' in config:
        test_data_provider = Util.process_config_value(config['test-data-provider'], extra_parameters)
    else:
        test_data_provider = None

    return train_data_provider, test_data_provider


def load_model(config, num_cores, use_gpus, output_directory, step):
    model_config = config['model'].copy()

    model_config['directory'] = output_directory
    model_config['step'] = step

    the_model = Util.load_model(model_config, num_cores, use_gpus)

    metadata = the_model.get_all_metadata()

    return the_model, metadata


def create_model(config, num_cores, use_gpus, train_data_provider, metadata):
    # Create model.
    input_shape = train_data_provider.get_mini_batch_input_shape()
    output_shape = train_data_provider.get_mini_batch_output_shape()

    the_model = Util.create_model(config['model'], input_shape, output_shape, num_cores, use_gpus)

    # Set metadata.
    the_model.set_all_metadata(metadata)

    # Optionally init it.
    if isinstance(the_model, model.Graph) and config.get('init-with-data-provider', True):
        the_model.init_with_data_provider(train_data_provider)

        logging.info('Initialized model.')

    return the_model


def create_trainer(config, the_model, train_data_provider, test_data_provider, output_directory):
    # Get trainer parameters.
    trainer = config['trainer']

    trainer_parameters = {}
    for name, value in trainer.items():
        trainer_parameters[Util.process_config_name(name, underscore=False)] = Util.process_config_value(value)

    # Create trainer.
    summaries_dir = os.path.join(output_directory, 'summaries')
    models_dir = os.path.join(output_directory, 'models')

    return model.Trainer(the_model, train_data_provider, test_data_provider, summaries_dir=summaries_dir,
                         models_dir=models_dir, parameters=trainer_parameters)


def main():
    # Parse command line options.
    parser = argparse.ArgumentParser(description='Train a model.',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('config_file_name', metavar='config-file-name', type=str, help='Configuration file name.')
    parser.add_argument('output_directory', metavar='output-directory', type=str,
                        help='Directory to store training output.')
    parser.add_argument('--step', metavar='step', type=int, help='Step to start at.')
    parser.add_argument('--gpus', metavar='gpus', type=str, help='GPU numbers to use (comma separated).')
    parser.add_argument('--num-cores', metavar='num-cores', type=int, help='Number of CPU cores to use.')

    args = parser.parse_args()

    # Load config yaml.
    with open(args.config_file_name, 'r') as f:
        config = yaml.load(f)

    try:
        # Create output directory.
        if args.step is None:
            if os.path.isdir(args.output_directory):
                raise Exception('Output path already exists. Specify step to train further.')

            os.makedirs(args.output_directory)

        # Copy config.
        step_and_date = 'UTC-{:%Y-%m-%d@%H:%M:%S}-step-{}'.format(datetime.datetime.utcnow(),
                                                                  '0' if args.step is None else args.step, )

        config_file_name = os.path.join(args.output_directory, 'config-{}.yml'.format(step_and_date))

        shutil.copy(args.config_file_name, config_file_name)

        # Setup logging.
        log_file_name = os.path.join(args.output_directory, 'log-{}.log'.format(step_and_date))

        setup_logger(log_file_name)

        # Log device usage.
        logging.info('Using {} cores and {} gpu ids.'.format(
            'all' if args.num_cores is None else args.num_cores, 'all' if args.gpus is None else args.gpus))

        # Load model if applicable.
        if args.step is not None:
            # Load model.
            the_model, metadata = load_model(config, args.num_cores, args.gpus, args.output_directory, args.step)

            logging.info('Loaded model with step {}.'.format(args.step))
        else:
            # Create model later on when we have train and test data.
            metadata = {}

        # Create data providers.
        train_data_provider, test_data_provider = create_data_providers(config, metadata)

        # Create model if applicable.
        if args.step is None:
            the_model = create_model(config, args.num_cores, args.gpus, train_data_provider, metadata)

            logging.info('Created model from class {}.{}.'.format(the_model.__class__.__module__, the_model.__class__.__name__))

        # Show statistics.
        logging.info('Model has {:,} parameters.'.format(the_model.get_num_parameters()))

        # Load trainer and train.
        trainer = create_trainer(config, the_model, train_data_provider, test_data_provider, args.output_directory)

        trainer.train()
    except KeyboardInterrupt:
        logging.info('Graceful exit.')
    except Exception:
        exception = traceback.format_exc()

        logging.error(exception)


if __name__ == '__main__':
    main()
