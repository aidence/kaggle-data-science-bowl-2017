# Copyright (C) 2017  Aidence B.V.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA

import os
import csv
import collections
import argparse
import pickle
import glob

import dicom

from util.point import Point3D
from util.util import Util

import numpy as np

import scipy
import scipy.ndimage

import xml.etree.ElementTree as ET


def read_annotations_csv(file_name):
    info = collections.defaultdict(list)
    amount = 0

    with open(file_name, 'r') as f:
        reader = csv.reader(f)

        next(reader)

        for row in reader:
            uid, x, y, z, diameter = row

            center = Point3D(float(z), float(y), float(x))

            diameter = float(diameter)

            if diameter < 0:
                diameter = 10

            info[uid].append(dict(center=center, radius=diameter * 0.5))
            amount += 1

    return info, amount


def read_uids_csv(file_name):
    uids = set()
    with open(file_name, 'r') as f:
        reader = csv.reader(f)

        for row in reader:
            uid = row[0]

            uids.add(uid)

    return uids


def get_matching_series(data_directory, uids):
    result = []
    for patient in os.listdir(data_directory):
        patient_directory = os.path.join(data_directory, patient)
        if not os.path.isdir(patient_directory):
            continue

        for study in os.listdir(patient_directory):
            study_directory = os.path.join(patient_directory, study)
            if not os.path.isdir(study_directory):
                continue

            for series in os.listdir(study_directory):
                series_directory = os.path.join(study_directory, series)
                if not os.path.isdir(series_directory):
                    continue

                if series in uids:
                    # Read slices until we find a non-localizer slice.
                    found = False
                    for file_name in glob.glob(os.path.join(series_directory, '*.dcm')):
                        image = dicom.read_file(file_name, stop_before_pixels=True)

                        if 'LOCALIZER' not in image.ImageType:
                            found = True
                            break

                    if not found:
                        raise Exception('No suitable slice found.')

                    info = {
                        'x-scale': float(image.PixelSpacing[0]),
                        'y-scale': float(image.PixelSpacing[1]),

                        'x-offset': float(image.ImagePositionPatient[0]),
                        'y-offset': float(image.ImagePositionPatient[1]),

                        'x-factor': 1 if float(image.ImageOrientationPatient[0]) > 0 else -1,
                        'y-factor': 1 if float(image.ImageOrientationPatient[4]) > 0 else -1,
                    }

                    result.append((patient, series, series_directory, info))

    return result


def read_annotations_xml(file_name, info):
    namespace = {'nih': 'http://www.nih.gov'}

    root = ET.parse(file_name).getroot()

    if 'LidcReadMessage' not in root.tag:
        return

    scale = (info['y-scale'] * info['y-factor'], info['x-scale'] * info['x-factor'])
    offset = (info['y-offset'], info['x-offset'])

    annotations = []
    for reading_session_node in root.iterfind('nih:readingSession', namespace):
        for unblinded_read_nodule_node in reading_session_node.iterfind('nih:unblindedReadNodule', namespace):
            # Find characteristics node.
            ch_node = unblinded_read_nodule_node.find('nih:characteristics', namespace)
            if ch_node is None:
                # This is nodule < 3mm.
                continue

            # Find all coordinates per slice.
            xs_per_z = collections.defaultdict(list)
            ys_per_z = collections.defaultdict(list)
            for roi_node in unblinded_read_nodule_node.iterfind('nih:roi', namespace):
                # Find all coordinates.
                z = float(roi_node.find('nih:imageZposition', namespace).text)

                for edge_map_node in roi_node.iterfind('nih:edgeMap', namespace):
                    x = int(edge_map_node.find('nih:xCoord', namespace).text)
                    y = int(edge_map_node.find('nih:yCoord', namespace).text)

                    xs_per_z[z].append(x)
                    ys_per_z[z].append(y)

            # Process per slice.
            center = np.zeros(3, dtype=np.float32)
            total_count = 0
            for z in xs_per_z.keys():
                xs = xs_per_z[z]
                ys = ys_per_z[z]

                min_x = np.min(xs)
                max_x = np.max(xs)

                min_y = np.min(ys)
                max_y = np.max(ys)

                inclusion_array = np.zeros((max_y - min_y + 1, max_x - min_x + 1), dtype=np.bool)
                exclusion_array = np.zeros((max_y - min_y + 1, max_x - min_x + 1), dtype=np.bool)

                for roi_node in unblinded_read_nodule_node.iterfind('nih:roi', namespace):
                    # Apply coordinates.
                    if float(roi_node.find('nih:imageZposition', namespace).text) != z:
                        continue

                    xs = []
                    ys = []
                    for edge_map_node in roi_node.iterfind('nih:edgeMap', namespace):
                        x = int(edge_map_node.find('nih:xCoord', namespace).text)
                        y = int(edge_map_node.find('nih:yCoord', namespace).text)

                        xs.append(x)
                        ys.append(y)

                    xs = np.array(xs)
                    ys = np.array(ys)

                    # Create array of filled area.
                    array = np.zeros((max_y - min_y + 1, max_x - min_x + 1), dtype=np.bool)

                    array[ys - min_y, xs - min_x] = True

                    array = scipy.ndimage.morphology.binary_fill_holes(array)

                    array[ys - min_y, xs - min_x] = False

                    # Include or exclude.
                    inclusion_text = roi_node.find('nih:inclusion', namespace).text
                    if inclusion_text == 'TRUE':
                        inclusion_array[array] = True
                    else:
                        exclusion_array[array] = True

                # Exclude exclusion.
                inclusion_array[exclusion_array] = False

                # Calculate center of mass.
                slice_center = scipy.ndimage.measurements.center_of_mass(inclusion_array, inclusion_array, 1)

                slice_center = (np.array(slice_center, dtype=np.float32) + (min_y, min_x)) * scale + offset

                slice_count = np.count_nonzero(inclusion_array)

                if slice_count:
                    center += np.array((z, slice_center[0], slice_center[1]), dtype=np.float32) * slice_count
                    total_count += slice_count

            if not total_count:
                continue

            center /= total_count

            info = {
                'center': Point3D.from_zyx_list(center),
                'subtlety': int(ch_node.find('nih:subtlety', namespace).text) if ch_node.find('nih:subtlety', namespace) is not None else None,
                'structure': int(ch_node.find('nih:internalStructure', namespace).text) if ch_node.find('nih:internalStructure', namespace) is not None else None,
                'calcification': int(ch_node.find('nih:calcification', namespace).text) if ch_node.find('nih:calcification', namespace) is not None else None,
                'sphericity': int(ch_node.find('nih:sphericity', namespace).text) if ch_node.find('nih:sphericity', namespace) is not None else None,
                'margin': int(ch_node.find('nih:margin', namespace).text) if ch_node.find('nih:margin', namespace) is not None else None,
                'lobulation': int(ch_node.find('nih:lobulation', namespace).text) if ch_node.find('nih:lobulation', namespace) is not None else None,
                'spiculation': int(ch_node.find('nih:spiculation', namespace).text) if ch_node.find('nih:spiculation', namespace) is not None else None,
                'texture': int(ch_node.find('nih:texture', namespace).text) if ch_node.find('nih:texture', namespace) is not None else None,
                'malignancy': int(ch_node.find('nih:malignancy', namespace).text) if ch_node.find('nih:malignancy', namespace) is not None else None,
            }

            annotations.append(info)

    return annotations


def add_annotation_information(included, series):
    names = ['subtlety', 'structure', 'calcification', 'sphericity', 'margin',
             'lobulation', 'spiculation', 'texture', 'malignancy']

    for patient, series, series_directory, info in series:
        csv_annotations = included.get(series, [])
        if not csv_annotations:
            continue

        # Read annotation information.
        xml_annotations = None
        for file_name in glob.glob(os.path.join(series_directory, '*.xml')):
            xml_annotations = read_annotations_xml(file_name, info)

            if xml_annotations is not None:
                break

        if xml_annotations is None:
            raise Exception('No XML annotation information read.')

        for annotation in csv_annotations:
            center = annotation['center'].get_as_zyx_tuple()
            radius = annotation['radius']

            # Find matching XML annotations.
            matching_xml_annotations = []

            for xml_annotation in xml_annotations:
                distance = np.linalg.norm(xml_annotation['center'].get_as_zyx_array() - center)
                if distance < radius:
                    matching_xml_annotations.append(xml_annotation)

            for name in names:
                values = [annotation[name] for annotation in matching_xml_annotations if annotation[name] is not None]
                if not values:
                    # No estimates.
                    annotation[name] = None
                    continue

                annotation[name] = np.sort(values)


def worker_process_main(series_queue, scan_queue, output_data_directory, force, scale, full_precision):
    import data

    import queue
    import traceback

    # Process series.
    dicom_reader = data.DicomReader()

    while True:
        # Try to get a series.
        try:
            patient, uid, series_directory, _ = series_queue.get_nowait()
        except queue.Empty:
            break

        try:
            # Determine output file names.
            data_file_name = os.path.join(output_data_directory, '{}.npz'.format(uid))
            metadata_file_name = os.path.join(output_data_directory, '{}-metadata.pkl'.format(uid))

            # Skip if they exists.
            if not force and os.path.exists(data_file_name) and os.path.exists(metadata_file_name):
                # Get scan info.
                with open(metadata_file_name, 'rb') as f:
                    metadata = pickle.load(f)

                # Check scale.
                if scale.get_as_zyx_tuple() != metadata['scale'].get_as_zyx_tuple():
                    raise Exception('Found existing scan with different scale: {}.'.format(patient))

                # Put them in queue.
                scan_queue.put((patient, uid, metadata))

                continue

            # Read dicom.
            try:
                dicom = dicom_reader.read_series(series_directory, minimum_depth=0, maximum_z_spacing=np.inf,
                                                 maximum_xy_spacing=np.inf, output_scale=scale, check_extension=True,
                                                 source='lidc')
            except data.DicomRejectedException as e:
                print('Scan {}/{} was rejected. {}'.format(patient, uid, str(e)))
                continue

            # Save data.
            if full_precision:
                dicom.save_data_as_int16(data_file_name)
            else:
                dicom.save_data_as_uint8(data_file_name)

            dicom.save_metadata(metadata_file_name)

            # Put them in queue.
            scan_queue.put((patient, uid, dicom.get_metadata()))
        except KeyboardInterrupt:
            raise
        except:
            traceback.print_exc()

            print('Processing scan {}/{} failed.'.format(patient, uid))
            continue


def accumulator_process_main(scan_queue, num_entries, included, excluded, output_directory):
    scans = {}
    while True:
        # Get an entry.
        scan = scan_queue.get()

        # Check whether we are done.
        if scan is None:
            break

        # Get scan info.
        patient, uid, metadata = scan

        # Get annotations.
        included_annotations = included.get(uid, [])
        excluded_annotations = excluded.get(uid, [])

        metadata['included-annotations'] = included_annotations
        metadata['excluded-annotations'] = excluded_annotations
        metadata['file-name'] = '{}.npz'.format(uid)

        # Set scan data.
        scans[uid] = metadata

        # Show progress.
        print('{}/{} ({:.1%}).'.format(len(scans), num_entries, len(scans) / num_entries))

    # Write dataset.
    dataset = {'type': 'dataset', 'scans': scans}

    with open(os.path.join(output_directory, 'dataset.pkl'), 'wb') as f:
        pickle.dump(dataset, f)


def main():
    # Parse command line options.
    parser = argparse.ArgumentParser(description='Write LIDC data-set.',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('included_file_name', metavar='included-file-name', type=str, help='Included annotations.')
    parser.add_argument('excluded_file_name', metavar='excluded-file-name', type=str, help='Excluded annotations.')
    parser.add_argument('uids_file_name', metavar='uids-file-name', type=str, help='Scan uids.')
    parser.add_argument('data_directory', metavar='data-directory', type=str, help='Data directory.')
    parser.add_argument('output_directory', metavar='output-directory', type=str, help='Output directory.')
    parser.add_argument('--scale', metavar='scale', type=str, default='2.5x0.512x0.512', help='Output scale.')
    parser.add_argument('--force', action='store_true', default=False, help='Overwrite existing entries.')
    parser.add_argument('--full-precision', action='store_true', default=False,
                        help='Store full int16 precision. Otherwise data is saved as uint8.')
    parser.add_argument('--num-processes', metavar='num-processes', type=int, help='Number of processes to use.')

    args = parser.parse_args()

    # Get scale.
    scale = Util.parse_3d_point(args.scale, dtype=float)

    print('Using scale: {}x{}x{}.'.format(scale.z, scale.y, scale.x))

    # Read annotation files.
    included, included_num = read_annotations_csv(args.included_file_name)
    excluded, excluded_num = read_annotations_csv(args.excluded_file_name)

    print('Found {} annotations, {} included and {} excluded.'.format(
        included_num + excluded_num, included_num, excluded_num))

    # Read uids.
    uids = read_uids_csv(args.uids_file_name)

    print('Found {} uids.'.format(len(uids)))

    # Create output directory.
    output_data_directory = os.path.join(args.output_directory, 'data')
    os.makedirs(output_data_directory, exist_ok=True)

    # Get matching series.
    series = get_matching_series(args.data_directory, uids)

    print('Found {} matching series.'.format(len(series)))

    # Add nodule information.
    add_annotation_information(included, series)

    # Distribute it.
    Util.distribute_cpu_work(series, worker_process_main, accumulator_process_main,
                             num_processes=args.num_processes,
                             worker_extra_args=(output_data_directory, args.force, scale, args.full_precision),
                             accumulator_extra_args=(included, excluded, args.output_directory))


if __name__ == '__main__':
    main()
