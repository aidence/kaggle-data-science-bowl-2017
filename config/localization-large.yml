# Copyright (C) 2017  Aidence B.V.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA

init-with-data-provider: true

train-data-provider:
    class: 'data.LinearAugmenter'
    seed: 42
    noise-std-dev: 0.05
    input-width: 128
    input-height: 128
    output-width: 58
    output-height: 58
    data-provider:
        class: 'data.DataProvider'
        file-name: 'patches/lidc-kaggle-random-patches-large.npz'
        test: false
        negative-weight: 2
        region: 92
        final-region: 58
        normalize-positives: true

test-data-provider:
    class: 'data.CenterCrop'
    input-width: 128
    input-height: 128
    data-provider:
        class: 'data.DataProvider'
        file-name: 'patches/lidc-kaggle-random-patches-large.npz'
        test: true
        negative-weight: 2
        region: 58
        normalize-positives: true

model:
    class: 'network.ResnetNetwork'
    add-unbiased-statistics: true
    weight-normalization: true
    weight-normalization-gamma: true
    negative-weight: 2
    seed: 42
    extra-residual-network: false
    first-activation: false
    global-pool: false
    num-filters: 8
    noise: false
    init-wanted-std-dev: 0.1

trainer:
    num-steps: 70000

    # Batch sizes.
    train-batch-size: 32
    test-batch-size: 64

    test-interval: 50
    save-interval: 100
    log-interval: 1
    summary-interval: 10

    # Optimizer.
    optimizer: 'adamax' # Can be: sgd/momentum/nesterov/adagrad/adadelta/adamax/rmspro/adam
    learning-rate: 3.0e-3

    # Optimizer parameters: momentum, rho, decay if applicable.

    log-captures:
        - 'data-loss'
        - 'unbiased-sensitivity'
        - 'unbiased-fp-rate'
        - 'fp-rate'
